/*getHomePagePrice();

setInterval(function(){     
	getHomePagePrice();
}, 5000);*/

function getHomePagePrice()
{

  $.get(base_url+'/bitunio/getHomePagePrice', function(data){
	//$('.pageloadingBG, .pageloading').css('display', '');

	var rec = data.record;
	var html = "";
	
	if(rec.length>0)
    {
		  for(var j=0;j<rec.length;j++){
			  var i=1;
			  //console.log("Coin : " + rec[j].currency_name);
			  var cname = rec[j].currency_name;//Currency FullName
			  var curr = rec[j].from_currency+"/BTC";//Currency Name
			  var img = rec[j].from_currency;
			  var lastprice = rec[j].last_price;//Last Price
			  var percent = rec[j].daily_pct;//Daily Percentage
			  var per = parseFloat(percent).toFixed(2);
			  percent = per + "%";
			   
			  var range = rec[j].range_24hrs;//Range
			  
			  var pr = range.split("-");
			  var low = pr[0];//Low Price
			  var high = pr[1];//High Price
			  
			  var openprice = rec[j].open_price;//Open Price
			  
			  console.log("Per : " + per + " : " + (per>0));
			  
			  if (per > 0) {
			         var dailyChangeIconClass = 'fa-caret-up';
			         var dailyChangeColor = 'green';//'#009900';
			     } else {
			         var dailyChangeIconClass = 'fa-caret-down';
			         var dailyChangeColor = 'red';//'#d9534f';
			     }
			  var iconimg ="";
			  if(img=="ETH"){
				  iconimg = '<img src='+ base_url + 'assets/img/eth.png class="img-responsive">';
			  }
			  if(img=="XRP"){
				  iconimg = '<img src='+ base_url + 'assets/img/xrp.png class="img-responsive">';
			  }
			  if(img=="XMR"){
				  iconimg = '<img src='+ base_url + 'assets/img/xmr.png class="img-responsive">';
			  }
			  if(img=="ETC"){
				  iconimg = '<img src='+ base_url + 'assets/img/etc.png class="img-responsive">';
			  }
			  if(img=="ZEC"){
				  iconimg = '<img src='+ base_url + 'assets/img/zec.png class="img-responsive">';
			  }
			  if(img=="LTC"){
				  iconimg = '<img src='+ base_url + 'assets/img/ltc.png class="img-responsive">';
			  }
			  if(img=="DASH"){
				  iconimg = '<img src='+ base_url + 'assets/img/dash.png class="img-responsive">';
			  }
			  if(img=="BCH"){
				  iconimg = '<img src='+ base_url + 'assets/img/bch.png class="img-responsive">';
			  }
			  
			  html += "<tr class='row100'>";
			  		
			  		html += '<td class="column100 column1" data-column="column1">' + iconimg + '&nbsp;&nbsp;' + curr + '</td>';
			  		
			  		html += '<td class="column100 column2" data-column="column2">' + cname + '</td>';
			  		
			  		html += '<td class="column100 column3" data-column="column3">' + lastprice + '</td>';
			  		
			  		html += '<td class="column100 column4" data-column="column4">&nbsp;<i class="fa '+dailyChangeIconClass+'"></i>&nbsp;<span class="'+dailyChangeColor+'">' + percent + '</span></td>';
			  		
			  		html += '<td class="column100 column5" data-column="column5">' + low + '</td>';
			  		
			  		html += '<td class="column100 column6" data-column="column6">' + high + '</td>';
			  		
			  		html += '<td class="column100 column7" data-column="column7">' + openprice + '</td>';			  		
			  html += "</tr>"	
			
	      }//End of For
		  
		  
		 // console.log("Data : " + html);
		  $("#prices").html(html);//By Mujeeb
    }//End of If
   

  },'json');

}//End of Function

