	/*	orderBookOrders = {
			buy: [],
			sell: []
		};
		var isPrecisionValid = function(precision) {
			return precision <= 6 && precision >= 6 - 2;
		};

		var getLocalPrecision = function() {
			var localPrecision = boWebsocketClient.getLocalStoragePrecision("BTC-AED", 6);
			if (!isPrecisionValid(localPrecision)) {
				localPrecision = 6;
				boWebsocketClient.setLocalStoragePrecision("BTC-AED", localPrecision);
			}
			return localPrecision;
		};
		var orderBookBidsTableSelector = '.order-book div.bids .data';
		var orderBookAsksTableSelector = '.order-book div.asks .data';
		var orderBookPrecision = getLocalPrecision();
		var isOrderBookPrecisionUpdated = false;
		var lastTradesData = null;

		var getOrderBookIcons = function(price, orders) {
			for(var order in orders) {
				var orderPrice = orders[order];
				if(round(orderPrice, orderPrice.getPricePrecisionForDigits(orderBookPrecision, false)) === price) {
					return '<i class="fa fa-dot-circle-o" title="Your limit order"></i>';
				}
			}
			return '&nbsp;';
		};
		var updateOrderBookOrders = function() {
			$(orderBookBidsTableSelector + ' .row').each(function() {
				var price = parseFloat($(this).attr('data-price'));
				$(this).children('.icon').html(getOrderBookIcons(price, orderBookOrders.buy));
			});
			$(orderBookAsksTableSelector + ' .row').each(function() {
				var price = parseFloat($(this).attr('data-price'));
				$(this).children('.icon').html(getOrderBookIcons(price, orderBookOrders.sell));
			});
		};
		var replaceOrderBookOrders = function(data) {
			orderBookOrders.buy = [];
			orderBookOrders.sell = [];
			for (var i = 0; i < data.length; i++) {
				var order = data[i];
				if (order.status === 1 && order.type === "limit" && order.cryptocurrency === "BTC") {
					var price = parseFloat(order.price);
					if (price !== NaN) {
						if (order.side === 'buy') {
							orderBookOrders.buy.push(price);
						} else {
							orderBookOrders.sell.push(price);
						}
					}
				}
			}
			updateOrderBookOrders();
		};
		var updatePrecisionText = function() {
			var text = 'Med';
			if (orderBookPrecision >= 6) {
				text = 'Max';
			} else if (orderBookPrecision <= 6 - 2) {
				text = 'Min';
			}
			$('.order-book .precision .precision-value').text(text);
		};
		var fillOrderBook = function (data) {
			var updateRow = function(row, rowPrice, rowCumulative, rowAmount, rowCount) {
				row.css('display', rowPrice === null ? 'none' : '');
				if(rowPrice !== null) {
					var width = round(Math.min(100, rowCumulative * 0.5), 2);
					var priceDecimalPoints = rowPrice.getPricePrecisionForDigits(orderBookPrecision);
					row.attr('data-price', round(rowPrice, priceDecimalPoints));
					row.children('.depth-bar').css('width', width + '%');
					row.children('.count').text(rowCount);
					row.children('.amount').text(formatNumber(rowAmount, 2));
					row.children('.cumulative').text(formatNumber(rowCumulative, 2));
					row.find('.price').text(formatNumber(rowPrice, priceDecimalPoints));
				}
			};
			if (data.precision !== orderBookPrecision) {
				orderBookPrecision = data.precision;
				updatePrecisionText();
				boWebsocketClient.setLocalStoragePrecision("BTC-AED", data.precision);
				if (lastTradesData !== null) {
					fillTrades(lastTradesData);
				}
			}
			if (data.bids.length > 0) {
				var bidsTable = $(orderBookBidsTableSelector);
				for(var i = 0; i < 25; i++) {
					var row = bidsTable.find('.row-' + i);
					if(i < data.bids.length) {
						var bid = data.bids[i];
						updateRow(row, bid.price, bid.cumulative, bid.amount, bid.count);
					} else {
						updateRow(row, null);
					}
				}
			}
			
			if (data.asks.length > 0) {
				var asksTable = $(orderBookAsksTableSelector);
				for(var i = 0; i < 25; i++) {
					var row = asksTable.find('.row-' + i);
					if(i < data.asks.length) {
						var ask = data.asks[i];
						updateRow(row, ask.price, ask.cumulative, ask.amount, ask.count);
					} else {
						updateRow(row, null);
					}
				}
			}
			updateOrderBookOrders();
		};
		
		var fillTrades = function(data) {
			if (data.length > 0) {
				lastTradesData = data;
				var tradesTable = $('.order-book div.trades .data');
				tradesTable.html('');
				for(var i = 0; i < data.length; i++) {
					var trade = data[i];
					tradesTable.append('<div class="row"><div class="'+trade.type+'">&nbsp;</div><div class="col-xs-4">'+formatDate(trade.date)+'</div><div class="col-xs-4">'+parseFloat(trade.price).exchangePrice(orderBookPrecision)+'</div><div class="col-xs-4">'+trade.amount.toFixed(2)+'</div></div>');
				}
			}
		};

		var formatDate = function(date) {
			date = new Date(date);
			hours = date.getHours();
			minutes = date.getMinutes();
			seconds = date.getSeconds();
			return (hours < 10 ? '0' : '') + hours + (minutes < 10 ? ':0' : ':') + minutes + (seconds < 10 ? ':0' : ':') + seconds;
		};
		
		var setOrderBookPrecision = function(precision) {
			if (isOrderBookPrecisionUpdated || !isPrecisionValid(precision) || orderBookPrecision === precision) {
				return;
			}
			isOrderBookPrecisionUpdated = true;
			var elModal = $('#order-book-modal').addClass('loading');
			boWebsocketClient.subscribe("order-book", "unsubscribe");
			setTimeout(function() {
				boWebsocketClient.subscribe("order-book", "subscribe", { precision: precision, pair: "BTC-AED" });
				isOrderBookPrecisionUpdated = false;
				elModal.removeClass('loading');
			}, 500);
		};

		var updateOrderBookPrecision = function(precisionDelta) {
			var precision = orderBookPrecision + precisionDelta;
			setOrderBookPrecision(precision);
		};

		updatePrecisionText();
		$(function() {
			updateOrderBookOrders();
			boWebsocketClient.addListener("order-book", "subscribe", fillOrderBook, { precision: orderBookPrecision, pair: "BTC-AED" });
			boWebsocketClient.addListener("trades", "subscribe", fillTrades, { pair: "BTC-AED" });
			boWebsocketClient.addListener("user-orders", "subscribe", replaceOrderBookOrders);
		});
	*/