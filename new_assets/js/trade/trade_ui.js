

$( document ).ready(function() {
	var url = window.location.pathname;
	$('#tradeSummaryCurrency').val(url.substr(url.lastIndexOf('/') + 1) === "trade" ? "ETH_BTC" : url.substr(url.lastIndexOf('/') + 1));
	
	$("#success-alert").hide();
});

$('#tradeSummaryCurrency').change(function() {
	// set the window's location property to the value of the option the user has selected
	//window.location = "https://localhost/bitio/bitunio/trade/" + $(this).val();
	//var url = window.location.pathname;
	window.location = base_url +"trade/" + $(this).val();
});


//Function by Mujeeb: To Show order status on notification
function showAlert(message, atype)
{
	 var sclass;
	 if(atype=="success")//Set success notification
	 {
		 sclass = "alert alert-success";
	 }
	 else if(atype=="info")//Set info notification
	 {
		 sclass = "alert alert-info";
	 }
	 else if(atype=="warning")//Set warning notification
	 {
		 sclass = "alert alert-warning";
	 }
	 else if(atype=="danger")//Set danger notification
	 {
		 sclass = "alert alert-danger";
	 }
	 $("#success-alert").removeClass(); //Remove previous all classes
	 
	 //$('#alert').css('display', 'block');//Alert Box CSS
 	 $("#error").html(message); //Set order status to notification	 
 	 $("#success-alert").addClass(sclass);//Set type of alert to notification
 	 
 	 //Show and hide order notification after 5 seconds
     $("#success-alert").fadeTo(5000, 500).slideUp(500, function(){
    	 $("#error").html("");
    	 $("#success-alert").slideUp(500);
     });   
 
}//End of Function




//  OrderBook Integration

var orderBookBidsTableSelector = '.order-book div.bids .data';
var orderBookAsksTableSelector = '.order-book div.asks .data';

var fillOrderBook = function (data) {
 //console.log(data);
 var updateRow = function(row, rowPrice, rowCumulative, rowAmount, rowCount) {
  row.css('display', rowPrice === null ? 'none' : '');
  if(rowPrice !== null) {
   //var width =  Math.round(Math.min(100, rowCumulative * 0.5), 2);
   var width = parseFloat(Math.min(100, rowCumulative * 0.5)).toFixed(2);
   var priceDecimalPoints = 8; //rowPrice.getPricePrecisionForDigits(orderBookPrecision);
   
   var dataprice = parseFloat(rowPrice).toFixed(priceDecimalPoints);// Math.round(rowPrice, priceDecimalPoints);
   //console.log("rowPrice : " + dataprice);
   
   row.attr('data-price', dataprice);
   row.children('.depth-bar').css('width', width + '%');
   row.children('.count').text(rowCount);
   row.children('.amount').text(rowAmount);
   row.children('.cumulative').text(rowCumulative);
   row.find('.price').text(rowPrice);
  }
 };
 /*if (data.precision !== orderBookPrecision) 
 {
	  orderBookPrecision = data.precision;
	  updatePrecisionText();
	  if (lastTradesData !== null) {
	   fillTrades(lastTradesData);
	  }
 }*/
 if (data.bids.length > 0) {
  var bidsTable = $(orderBookBidsTableSelector);
  for(var i = 0; i < 25; i++) {
   var row = bidsTable.find('.row-' + i);
   if(i < data.bids.length) {
    var bid = data.bids[i];
    updateRow(row, bid.price, bid.cummulative, bid.amount, bid.count);
   } else {
    updateRow(row, null);
   }
  }
 }

 if (data.asks.length > 0) {
  var asksTable = $(orderBookAsksTableSelector);
  for(var i = 0; i < 25; i++) {
   var row = asksTable.find('.row-' + i);
   if(i < data.asks.length) {
    var ask = data.asks[i];
    updateRow(row, ask.price, ask.cummulative, ask.amount, ask.count);
   } else {
    updateRow(row, null);
   }
  }
 }
 //updateOrderBookOrders();
 updateOrderBook();
};

var fillTrades = function(data) {
 //console.log(data);
	 if (data.length > 0) 
	 {	 
		  lastTradesData = data;
		  var tradesTable = $('.order-book div.trades .data');
		  tradesTable.html('');
		  for(var i = 0; i < data.length; i++) 
		  {
			   var trade = data[i];
			 //  tradesTable.append('<div class="row"><div class="'+trade.type+'">&nbsp;</div><div class="col-xs-4">'+formatDate(trade.date)+'</div><div class="col-xs-4">'+parseFloat(trade.price)+'</div><div class="col-xs-4">'+trade.amount.toFixed(2)+'</div></div>');
			   tradesTable.append('<div class="row"><div class="'+trade.type+'">&nbsp;</div><div class="col-xs-4">'+trade.date+'</div><div class="col-xs-4">'+parseFloat(trade.price)+'</div><div class="col-xs-4">'+parseFloat(trade.amount).toFixed(2)+'</div></div>');
		  }
	 }
};

var formatDate = function(date) {
 date = new Date(date);
 hours = date.getHours();
 minutes = date.getMinutes();
 seconds = date.getSeconds();
 return (hours < 10 ? '0' : '') + hours + (minutes < 10 ? ':0' : ':') + minutes + (seconds < 10 ? ':0' : ':') + seconds;
};


//By Mujeeb--------------
function addZero(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}//End of Function

function loadTradeBook()
{
	var t = new Date() 
	var diff = 0;
	
	var trades = new Array();
	
	for(var i=0;i<25;i++)
	{
		//var dt = new Date(t);
		var h = addZero(t.getHours());
		var m = addZero(t.getMinutes());
		var s = addZero(t.getSeconds());
		//$("#time").append(h + ":" + m+ ":" + s+"</br>");
		var tm = h +":"+ m + ":" + s;
		//console.log("D : " + tm);
		var buysell = getRandomDynamic(0,1);
		var tamt = parseFloat(getRandomDynamicTradeAmt());
		
		var loadedTrades = {
			date : tm,
			price: buysell==0?instant_buy_price:instant_sell_price, //0.123123,
			amount: tamt,//0.123123,
			type: buysell==0?'buy':'sell' ///Dynamic Number Generation
		};
		trades.push(loadedTrades);
		var inc = getRandomDynamic(0,3);
		
		diff = parseInt(diff)+parseInt(inc); //Dynamic Number Generation
		//console.log("Inc + diff" + inc + " :  "+ diff);
		t.setSeconds(t.getSeconds()-diff);				
		
	}
	
	return trades;
}//End of Function

var updateTradeBook = function () {
 var trades = new Array();
 for(var i=0;i<25;i++) {
  var loadedTades = {
   date: Date(),
   price: 0.123123,
   amount: 0.123213,
   type: 'sell'
  };
  trades.push(loadedTades);
 }

 return trades;
};

function updateOrderBookLive(buy, sell){
	var asks = new Array();
		if(buy.length>0)
	    {      
			
	      var sell_title = '';
	      for(var j=0;j<buy.length;j++){
	    	  
	    	  	var loadedAsks = {
	    			   price: parseFloat(buy[j].Price).toFixed(8),
	    			   amount: parseFloat(buy[j].Amount).toFixed(2),
	    			   cummulative: parseFloat(buy[j].Total).toFixed(8),
	    			   count: buy[j].CountTotal//'-'//getRandomCount()
	    			  };    			
	    			  asks.push(loadedAsks);      
	      }     
	    }
	    else
	    {
	    		var loadedAsks = {
	 			   price: parseFloat('0.0').toFixed(8),
	 			   amount: parseFloat('0.0').toFixed(2),
	 			   cummulative: parseFloat('0.0').toFixed(8),
	 			   count: '-'//getRandomCount()
	 			  }; 			
	 			  asks.push(loadedAsks);
	    }
	
		var bids = new Array();	
		if(sell.length>0)
	    { 
			for(var i=0;i<sell.length;i++){
		  	  
			  	var loadedBids = {
					   price: parseFloat(sell[i].Price).toFixed(8),
					   amount: parseFloat(sell[i].Amount).toFixed(2),
					   cummulative: parseFloat(sell[i].Total).toFixed(8),
					   count: sell[i].CountTotal//'-'//getRandomCount()
					  };    			
					  bids.push(loadedBids);      
		    }     
				
		}
		else
		{
				var loadedBids = {
					   price: parseFloat('0.0').toFixed(8),
					   amount: parseFloat('0.0').toFixed(2),
					   cummulative: parseFloat('0.0').toFixed(8),
					   count: '-'//getRandomCount()
					  }; 			
					bids.push(loadedBids);
		}
	
	var data = {asks: asks, bids: bids, precision: 8};
	
	fillOrderBook(data);
}//End of Function

function updateTradeBookLive(tradesdata)
{
	var trades = new Array();
	
	var t = new Date() 
	var diff = 0;
	
	//var trades = new Array();
	
	
		//var dt = new Date(t);
		var h = addZero(t.getHours());
		var m = addZero(t.getMinutes());
		var s = addZero(t.getSeconds());
		//$("#time").append(h + ":" + m+ ":" + s+"</br>");
		var tm = h +":"+ m + ":" + s;
		//console.log("D : " + tm);
		var buysell = getRandomDynamic(0,1);
		var tamt = parseFloat(getRandomDynamicTradeAmt());
		if(tradesdata.length>0)
		{
			for(var i=0;i<tradesdata.length;i++){
			  	var times = tradesdata[i].orderTime;
			  	var loadedTrades = {
			  		   date: times,
					   price: parseFloat(tradesdata[i].Price).toFixed(8),
					   amount: parseFloat(tradesdata[i].Amount).toFixed(2),
					   type: tradesdata[i].Type	=='Buy'?'buy':'sell'				   
					  };    			
			  	trades.push(loadedTrades);      
		    }    
		}
		else
		{
				var loadedTrades = {
					   date:'-',
					   price: parseFloat('0.00').toFixed(8),
					   amount: parseFloat('0.00').toFixed(2),
					   type: 'sell'		   
					  };    			
				trades.push(loadedTrades);
		}
		
		fillTrades(trades);
	//return trades;
}//End of Function


var updateOrderBook = function () {

 var asks = new Array();

 var askCummulative = 0;
 var bidCummulative = 0;
	 for(var i=0;i<25;i++)
	 {
		  var rn = parseFloat(getRandom());
		  var amount = parseFloat(getRandomAskAmount());
		  askCummulative = (parseFloat(askCummulative) + parseFloat(amount)).toFixed(2);
		
		  var loadedAsks = {
		   price: parseFloat(rn - 0.000004).toFixed(8),
		   amount: amount,
		   cummulative: askCummulative,
		   count: getRandomCount()
		  };
		
		  asks.push(loadedAsks);
	 }

		 var bids = new Array();
		
		 for(var i=0;i<25;i++)
		 {
		  var rn = parseFloat(getRandom());
		  var amount = parseFloat(getRandomAskAmount());
		  bidCummulative = (parseFloat(bidCummulative) + parseFloat(amount)).toFixed(2);
		  
		  var loadedBids = {
		   price: parseFloat(0.000004+rn).toFixed(8),
		   amount: amount,
		   cummulative: bidCummulative,
		   count: getRandomCount()
		  };
		
		  bids.push(loadedBids);
		 }

		 return {asks: asks, bids: bids, precision: 8};
};

function getRandom() {
	// var min = 0.026635;
	// var max = 0.026644;
	 var min = instant_buy_price-0.000010;
	 var max = instant_buy_price;
	  highlightedNumber =(Math.random() * (max - min) + min).toFixed(8);
	 return highlightedNumber;
};

function getRandomCount() {
 var min = 1,
  max = 5,
  countNumber =(Math.random() * (max - min) + min).toFixed(0);
 return countNumber;
};



function getRandomAskAmount() {
 var min = 10.99,
  max = 9264.99,
  askAmount =(Math.random() * (max - min) + min).toFixed(2);
 return askAmount;
};

function getRandomDynamic(min, max) {
	var min = min,
	  max = max,
	  countNumber =(Math.random() * (max - min) + min).toFixed(0);
	 return countNumber;
};

function getRandomDynamicTradeAmt()
{
	var min = 0.10, 
	max = 32.67,
	countNumber = (Math.random() * (max - min) + min).toFixed(6);
	//countNumber = 0.123456;
	return countNumber;
}//End of Function


$(function() {
 /*setInterval(function() {
  //var data = updateOrderBook();
  var data = updateOrderBookLive();

  fillOrderBook(data);
 }, 1000);

 setInterval(function() {
 // var trades = updateTradeBook();
  var trades = loadTradeBook();
  fillTrades(trades);
 }, 3000);*/
});

function updateActiveOrders(active_orders,pair1, pair2)
{
	var pair_active_orders;
	var fir_cur_dec=2,sec_cur_dec=8;
	
	var totalorders = active_orders.length;
	actualizeOpenOrdersCount(totalorders);
	
	if(active_orders.length>0)
	{
		//alert("M : " + active_orders.length);
        var act_tot = '',act_fee = '';
        
       // var totalorders = active_orders.lenght;
        pair_active_orders = '<tr><th>Date</th><th class="hidden-xs">Type</th><th class="hidden-xs">Amount('+pair1+')</th><th>Price</th><th>Pair</th><th class="hidden-xs"><span title="" data-original-title="This is amount after the fee is applied.">Total(BTC)</span></th><th class="hidden-xs">Status</th></tr>';
        for(var ka=0;ka<active_orders.length;ka++)
        {
          act_fee = parseFloat(active_orders[ka].fee);
          var act_fee_per = parseFloat(active_orders[ka].fee_per);
          var cal_fee = parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].amount)*(act_fee_per/100);
          act_tot = (active_orders[ka].Type=='Buy')?(parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].Amount)+parseFloat(active_orders[ka].Fee)):(parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].Amount)-parseFloat(active_orders[ka].Fee));
          //pair_active_orders += '<tr><th>Date</th><th class="hidden-xs">Type</th><th class="hidden-xs">Amount('+pair1+')</th><th>Price</th><th>Pair</th><th class="hidden-xs"><span title="" data-original-title="This is amount after the fee is applied.">Total(BTC)</span></th><th class="hidden-xs">Status</th></tr>';
          pair_active_orders += '<tr style="color:#000;font-size:10px;font-weight:normal;"><td>'+active_orders[ka].datetime+'</td><td><span class="'+((active_orders[ka].Type=='Buy')?'green':'red')+'">'+active_orders[ka].Type+'</span></td><td class="sort">'+parseFloat(active_orders[ka].amount).toFixed(fir_cur_dec)+'</td><td>'+parseFloat(active_orders[ka].Price).toFixed(sec_cur_dec)+'</td><td>'+pair1+"-"+pair2+'</td><td>'+act_tot.toFixed(sec_cur_dec)+'</td><td><a href="javascript:void(0);" onclick="close_active_order(\''+active_orders[ka].trade_id+'\',this)">Cancel</a></td>';
        }     
     }
	 else
     {
        pair_active_orders += '<tr><th>You have no order yet</th></tr>';
     } 
	
	//$('#'+selected+'_history_active_orders tbody').html(pair_active_orders);//.sort('pair_active_orders','desc');
	
	$("#orders_active").html(pair_active_orders);
	
}//End of active Orders


function actualizeOpenOrdersCount(totalorders) {
	//alert("T : " + totalorders);
	$('.open-orders h3 span').text('(' + totalorders + ')');
}

	
	$('.open-orders h3').click(function(e) {
		$('.open-orders div.order-list').toggle();
		equalheight('.equalheight .blocks');
		var arrowEl = $(this).children('i');
		if (arrowEl.length) {
			if (arrowEl.hasClass('fa-chevron-down')) {
				arrowEl.removeClass('fa-chevron-down').addClass('fa-chevron-up');
			} else {
				arrowEl.removeClass('fa-chevron-up').addClass('fa-chevron-down');
			}
		}
	});




//End of Order Book


