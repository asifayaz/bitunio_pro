
if(secondCurrency == "INR" ||  secondCurrency == "AED")
{
	  var sec_cur_dec = 2;
	  var fir_cur_dec = 8;
}	  
else 
{
	  var sec_cur_dec = 8;
	  var fir_cur_dec = 8;
}

var balances='';


//Modified By Mujeeb
//$("#buy_instant_btn").click(function() {
function marketBuy(){
			
	//var amnt = $("#buy_instant_amnt").val();
	var amnt = $("#frm-buyAndSell-form-amount").val();// By Mujeeb
	var instantbuyprice = $("#bprice").val();// By Mujeeb
	
	var fc = $("#fc").val();
	var oamt = amnt;
	
	var lamt = getMinTradeValue(fc);
	

	if((!isNaN(amnt)) && (amnt != ""))
	{
		//$('.pageloadingBG, .pageloading').css('display', '');
		var f_balance = parseFloat($("#fc_balance").html());
		var s_balance = parseFloat($("#sc_balance").html());
		//var tot       = parseFloat(amnt) * parseFloat(instant_buy_price);	
		var tot       = parseFloat(amnt) * parseFloat(instantbuyprice);	//By Mujeeb
		var fee       = parseFloat(tot) * parseFloat(comm)/100;
		var total     = parseFloat(tot) + parseFloat(fee);

			
		
			if(s_balance < total)
			{
				//$("#buy_instant_error").show();
				//$("#buy_instant_error").html("<center>Insufficient Balance</center>");			
				showAlert("Your balance is not sufficient for this order!", "danger"); //By Mujeeb				
				return false;
			}
			if(tot <= 0)
			{
				//$("#buy_instant_error").show();
				//$("#buy_instant_error").html("<center>Incorrect Amount</center>");
				showAlert("Order amount should be greater than 0!", "danger"); //By Mujeeb
				return false;
			}
			if(oamt < lamt)
			{
				//$("#buy_instant_error").show();
				//$("#buy_instant_error").html("<center>Please Enter Above 0.05 </center>");
				showAlert("Amount should be greater than "+lamt + " " + fc , "danger"); //By Mujeeb
				return false;
			}
			
			//return;
			
			//var dataform=$('#buy_instant').serialize();
			var dataform=$('#frm-buyAndSell-form').serialize();
			$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createbuyorder',
					 beforeSend:function(){
						 	//$("#buy_instant_error").hide();
						 	//$('.pageloadingBG, .pageloading').css('display', 'block');
						 	$('.pageloadingBG, .pageloading').show();
					 },
                     success:function(output) 
                     {
					 	//$('.pageloadingBG, .pageloading').css('display', 'none');
					 	$('.pageloadingBG, .pageloading').hide();
					
                        var doutput = output.trim();
						
						if(doutput == "true")//If Order Placed Success
                        {
							//$("#stat_suc").html("Success");
							//$("#msg_suc").html("Your Order Has been Placed");
							//$('#success-modal').modal('show');	
							showAlert("Your Order has been placed successfully", "success"); //By Mujeeb
						}
						else if(doutput == "balance")//If Low Balance
						{
							//$("#stat").html("Error");
							//$("#msg").html("Insufficient Balance");
							//$('#error-modal').modal('show');	
							$('.pageloadingBG, .pageloading').css('display', 'none');
							showAlert("Server Your balance is not sufficient for this order!", "danger"); //By Mujeeb
						}
						else if(doutput == "empty_value")
						{
							//$("#stat").html("Error");
							//$("#msg").html("Please Enter Valid Values");
							//$('#error-modal').modal('show');	
							showAlert("Please enter valid amount for this order", "danger"); //By Mujeeb
						}
						else if(doutput == "stop_limit"){
							showAlert("Placing order 'stop limit' comming soon !", "danger"); //By Mujeeb
						}
						else
                        {						
							//$("#buy_l_subtot").html("0.000");
							//$('#buy_instant')[0].reset();
							//$("#stat_suc").html("Success");
							//$("#msg_suc").html("Your Order Has been Placed");
							//$('#success-modal').modal('show');
							$("#totalamt").html("~0.00"); //Where 8 is for bitcoin's by Mujeeb
							$('#frm-buyAndSell-form')[0].reset();
							showAlert("Your Order has been placed successfully : ", "success"); //By Mujeeb
							mapping(doutput);
						}
					}
			});
			
			
		}//End of If
		else 
		{
				showAlert("Please enter valid numeric values", "success"); //By Mujeeb
				//$("#buy_instant_error").show();
				//$("#buy_instant_error").html("<center>Please Enter Valid Numeric Values</center>");
		}
}//End of Function By Mujeeb	
//}); //End of Instant Sales


$( "#buy_limit_btn" ).click(function() {

	var amnt=$("#buy_limit_amnt").val();
	var price=$("#buy_limit_price").val();
	
	var fc = $("#fc").val();
	var oamt = amnt;
	
	var lamt = getMinTradeValue(fc);
	
	if((!isNaN(price)) && (price != "") && (!isNaN(amnt)) && (amnt != ""))
	{
		var f_balance=parseFloat($("#fc_balance").html());
		var s_balance=parseFloat($("#sc_balance").html());
		var tot = parseFloat(amnt) * parseFloat(price);			
		var fee=parseFloat(tot) * parseFloat(comm) /100;			
		var total=parseFloat(tot) + parseFloat(fee);


// if(parseFloat(price) < parseFloat(instant_price))
// {
// $("#buy_limit_error").show();
// $("#buy_limit_error").html("<center>Please Enter Above Current rate</center>");
// return false;
// }
// else

	if(s_balance < total)
	{
		$("#buy_limit_error").show();
		$("#buy_limit_error").html("<center>Insufficient Balance</center>");
		return false;
	}
	if(tot <= 0)
	{
		$("#buy_limit_error").show();
		$("#buy_limit_error").html("<center>Incorrect Amount</center>");
		return false;
	}
	if(oamt < lamt)
	{		
		showAlert("Amount should be greater than "+lamt + " " + fc , "danger"); //By Mujeeb
		return false;
	}
	/*if(amnt < 0.05)
	{
		$("#buy_limit_error").show();
		$("#buy_limit_error").html("<center>please Enter Above 0.05 </center>");
		return false;
	}*/


		var dataform=$('#buy_limit').serialize();
		$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createbuyorder',
					 beforeSend:function(){
					 $("#buy_limit_error").hide();
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					 	$('.pageloadingBG, .pageloading').css('display', 'none');
					
                        var doutput = output.trim();
						
						if(doutput == "true")
                        {
							$("#stat_suc").html("Success");
							$("#msg_suc").html("Your Order Has been Placed");
							$('#success-modal').modal('show');
							
						}
						else if(doutput == "balance")
						{
							$("#stat").html("Error");
							$("#msg").html("Insufficient Balance");
							$('#error-modal').modal('show');						
						}
						else if(doutput == "empty_value")
						{
							$("#stat").html("Error");
							$("#msg").html("Please Enter Valid Numeric Values");
							$('#error-modal').modal('show');						
						}
						else
                        {
							$("#buy_limit_subtot").html("0.000");
							$('#buy_limit')[0].reset();						
							$("#stat_suc").html("Success");
							$("#msg_suc").html("Your Order Has been Placed");
							$('#success-modal').modal('show');
							mapping(doutput);
						}
					}//End of Success
			});//End of Ajax Call
			
			
			}
			else 
			{
				$("#buy_limit_error").show();
				$("#buy_limit_error").html("<center>Please Enter Valid Number Values</center>");
				return false;
			}
});//End of Limit Buy
														
														
$( "#buy_stop_btn" ).click(function() {

var amnt=$("#buy_stop_amnt").val();
var price=$("#buy_stop_price").val();

var fc = $("#fc").val();
var oamt = amnt;

var lamt = getMinTradeValue(fc);

if((!isNaN(price)) && (price != ""))
{
		var f_balance=parseFloat($("#fc_balance").html());
		var s_balance=parseFloat($("#sc_balance").html());
		var tot = parseFloat(amnt) * parseFloat(price);			
		var fee=parseFloat(tot) * parseFloat(comm) /100;			
		var total=parseFloat(tot) + parseFloat(fee);


	if(parseFloat(price) <= parseFloat(instant_buy_price))
	{
		$("#buy_stop_error").show();
		$("#buy_stop_error").html("<center>Please Enter Above Current rate</center>");
		return false;
	}
	else if(s_balance < total)
	{
		$("#buy_stop_error").show();
		$("#buy_stop_error").html("<center>Insufficient Balance</center>");
		return false;
	}
	else if(tot <= 0)
	{
		$("#buy_stop_error").show();
		$("#buy_stop_error").html("<center>Incorrect Amount</center>");
		return false;
	}
	if(oamt < lamt)
	{		
		showAlert("Amount should be greater than "+lamt + " " + fc , "danger"); //By Mujeeb
		return false;
	}
/*	else if(amnt < 0.05)
	{
		$("#buy_stop_error").show();
		$("#buy_stop_error").html("<center>please Enter Above 0.05 </center>");
		return false;
	}*/
//262



	var dataform=$('#buy_stop').serialize();
	$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createbuyorder',
					 beforeSend:function(){
					 $("#buy_stop_error").hide();
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					
					 	$('.pageloadingBG, .pageloading').css('display', 'none');
					
                        var doutput = output.trim();
						//alert("Muj : " + output);
						//return;
						
						if(doutput == "true")
                        {
							$("#stat_suc").html("Success");
							$("#msg_suc").html("Your Order Has been Placed");
							$('#success-modal').modal('show');
						
						}
						else if(doutput == "balance")
						{
							$("#stat").html("Error");
							$("#msg").html("Insufficient Balance");
							$('#error-modal').modal('show');						
						}
						else if(doutput == "empty_value")
						{
							$("#stat").html("Error");
							$("#msg").html("Please Enter Valid Values");
							$('#error-modal').modal('show');						
						}
						else
                        {
							$("#buy_stop_subtot").html("0.000");
							$('#buy_stop')[0].reset();
							$("#stat_suc").html("Success");
							$("#msg_suc").html("Your Order Has been Placed");
							$('#success-modal').modal('show');
							mapping(doutput);
						}
					}
			});
	}//End of If
	else 
	{
				$("#buy_stop_error").html("<center>Please Enter Valid Numeric Values</center>");
	}
});//End of Buy Stop
			
			
			function buy_l_input(amnt)
			{
				if(amnt != "")
				{
					//alert(instant_price);
					var tot = parseFloat(amnt) * parseFloat(instant_buy_price);
					var fee=parseFloat(tot) * parseFloat(comm) /100;			
					var total=parseFloat(tot) + parseFloat(fee);
					$("#buy_l_subtot").html(total.toFixed(sec_cur_dec));
				}
				else 
				{
					$("#buy_l_subtot").html("0.000");
				}
				
			}//End of Function
			
			function buy_limit_input()
			{
			
				var amnt=$("#buy_limit_amnt").val();
				var price=$("#buy_limit_price").val();
				if(amnt != "" && price != "")
				{
				//alert(instant_price);
					var tot = parseFloat(amnt) * parseFloat(price);			
					var fee=parseFloat(tot) * parseFloat(comm) /100;			
					var total=parseFloat(tot) + parseFloat(fee);
					$("#buy_limit_subtot").html(total.toFixed(sec_cur_dec));
				}
				else 
				{
					$("#buy_limit_subtot").html("0.000");
				}
			
			}//End of function
			function buy_stop_input()
			{
			
				var amnt=$("#buy_stop_amnt").val();
				var price=$("#buy_stop_price").val();
				if(amnt != "" && price != "")
				{
					//alert(instant_price);
					var tot = parseFloat(amnt) * parseFloat(price);			
					var fee=parseFloat(tot) * parseFloat(comm) /100;			
					var total=parseFloat(tot) + parseFloat(fee);
					$("#buy_stop_subtot").html(total.toFixed(sec_cur_dec));
				}
				else 
				{
					$("#buy_stop_subtot").html("0.000");
				}
			
			}//End of Function
			
			
			
			//Sell buy copy
			
			
			
//Function Modified By Mujeeb : 30-12-2011			
//$( "#sell_instant_btn" ).click(function() {
function marketSell() {

  var amnt = $("#frm-buyAndSell-form-amount").val();
  var instantsellprice = $("#sprice").val();
  
  var fc = $("#fc").val();
  var oamt = amnt;

  var lamt = getMinTradeValue(fc);
  
  if((!isNaN(amnt)) && (amnt != ""))
  {
		var f_balance = parseFloat($("#fc_balance").html());		
		//var tot       = parseFloat(amnt) * parseFloat(instant_sell_price);
		var tot       = parseFloat(amnt) * parseFloat(instantsellprice);
		var fee       = parseFloat(tot) * parseFloat(comm) /100;			
		var total     = parseFloat(tot) + parseFloat(fee);
		
		if(f_balance < amnt)
		{
			showAlert("Your balance is not sufficient for this order!", "danger"); //By Mujeeb
			return false;
		}
		else if(amnt <= 0)
		{
			showAlert("Please enter valid amount!", "danger"); //By Mujeeb
			return false;
		}
		if(oamt < lamt)
		{		
			showAlert("Amount should be greater than "+lamt + " " + fc , "danger"); //By Mujeeb
			return false;
		}
		/*else if(amnt < 0.05)
		{
			showAlert("Please enter amount above 0.05!", "danger"); //By Mujeeb
			return false;
		}*/
	
	
		var dataform=$('#frm-buyAndSell-form').serialize();
		var url = base_url+'Trade/createsellorder';
		//dataform = "";
	
	$.ajax({
					 url:base_url+'createsellorder',
                     type:'POST',
                     data:dataform,                   
					 beforeSend:function(){					
						 $('.pageloadingBG, .pageloading').show();
					 },
                     success:function(output) 
                     {
					 	$('.pageloadingBG, .pageloading').hide();
                        var doutput = output.trim();
						
                       // alert("Return : " + doutput);
						if(doutput == "true")
                        {
							showAlert("Order has been placed successfully...!", "success"); //By Mujeeb
							
						}
						else if(doutput == "balance")
						{
							showAlert("Your balance is not sufficient for this order!", "danger"); //By Mujeeb
						}
						else if(doutput == "empty_value")
						{
							showAlert("Please enter valid amount!", "danger"); //By Mujeeb							
						}
						else if(doutput == "stop_limit"){
							showAlert("Placing order 'stop limit' comming soon !", "danger"); //By Mujeeb
						}
						else
                        {
							$("#totalamt").html("~0.00"); //Where 8 is for bitcoin's by Mujeeb
							$('#frm-buyAndSell-form')[0].reset();							
							showAlert("Order has been placed successfully...!", "success"); //By Mujeeb
							mapping(doutput);
						}
					}
			});
			
			
	}//End of If
	else 
	{
			showAlert("Please enter valid numeric values", "info"); //By Mujeeb
	}
}//End of FUnction 
//});//End of Instant Sales


//Start of Sell Limit
$( "#sell_limit_btn" ).click(function() {

	var amnt = $("#sell_limit_amnt").val();
	var price= $("#sell_limit_price").val();
	
	var fc = $("#fc").val();
	var oamt = amnt;//$("#amount").val();

	var lamt = getMinTradeValue(fc);

	if((!isNaN(price)) && (price != "") && (!isNaN(amnt)) && (amnt != ""))
	{
		var f_balance = parseFloat($("#fc_balance").html());
		var tot = parseFloat(amnt) * parseFloat(price);			
		var fee=parseFloat(tot) * parseFloat(comm) /100;			
		var total=parseFloat(tot) + parseFloat(fee);

		//alert(amnt); return false;

		// if(parseFloat(price) < parseFloat(instant_price))
		// {
		// $("#sell_limit_error").show();
		// $("#sell_limit_error").html("<center>Please Enter Above Current rate</center>");
		// return false;
		// }
		// else 
		if(f_balance < amnt)
		{
			$("#sell_limit_error").show();
			$("#sell_limit_error").html("<center>Insufficient Balance</center>");
			return false;
		}
		else if(tot <= 0)
		{
			$("#sell_limit_error").show();
			$("#sell_limit_error").html("<center>Incorrect Amount</center>");
			return false;
		}
		if(oamt < lamt)
		{		
			showAlert("Amount should be greater than "+lamt + " " + fc , "danger"); //By Mujeeb
			return false;
		}
		/*else if(amnt < 0.05)
		{
			$("#sell_limit_error").show();
			$("#sell_limit_error").html("<center>please Enter Above 0.05 </center>");
			return false;
		}*/


		var dataform=$('#sell_limit').serialize();
		$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createsellorder',
					 beforeSend:function(){
					 $("#sell_limit_error").hide();
						$('.pageloadingBG, .pageloading').css('display', '');
					 },
                     success:function(output) 
                     {
					 	$('.pageloadingBG, .pageloading').css('display', 'none');
					
                        var doutput = output.trim();
						
						if(doutput == "true")
                        {
							$("#stat_suc").html("Success");
							$("#msg_suc").html("Your Order Has been Placed");
							$('#success-modal').modal('show');						
						}
						else if(doutput == "balance")
						{
							$("#stat").html("Error");
							$("#msg").html("Insufficient Balance");
							$('#error-modal').modal('show');						
						}
						else if(doutput == "empty_value")
						{
							$("#stat").html("Error");
							$("#msg").html("Please Enter Valid Numeric Values");
							$('#error-modal').modal('show');						
						}
						else
                        {		
							$("#sell_limit_subtot").html("0.000");
							$('#sell_limit')[0].reset();
							$("#stat_suc").html("Success");
							$("#msg_suc").html("Your Order Has been Placed");
							$('#success-modal').modal('show');
							mapping(doutput);
						}
					}
			});
			
			
		}//End of Outer If
		else 
		{
				$("#sell_limit_error").show();
				$("#sell_limit_error").html("<center>Please Enter Valid Number Values</center>");
				return false;
		}
}); //End of Sell Limit
														
	
//Start of Stop Sell
$( "#sell_stop_btn" ).click(function() {

	var amnt=$("#sell_stop_amnt").val();
	var price=$("#sell_stop_price").val();
	
	var fc = $("#fc").val();
	var oamt = amnt;//$("#amount").val();

	var lamt = getMinTradeValue(fc);

	if((!isNaN(price)) && (price != ""))
	{
		var f_balance = parseFloat($("#fc_balance").html());
		var tot = parseFloat(amnt) * parseFloat(price);			
		var fee = parseFloat(tot) * parseFloat(comm) /100;			
		var total = parseFloat(tot) + parseFloat(fee);

		if(parseFloat(price) >= parseFloat(instant_sell_price))
		{
			$("#sell_stop_error").show();
			$("#sell_stop_error").html("<center>Please Enter Below Current rate</center>");
			return false;
		}
		else if(f_balance < amnt)
		{
			$("#sell_stop_error").show();
			$("#sell_stop_error").html("<center>Insufficient Balance</center>");
			return false;
		}
		else if(tot <= 0)
		{
			$("#sell_stop_error").show();
			$("#sell_stop_error").html("<center>Incorrect Amount</center>");
			return false;
		}
		if(oamt < lamt)
		{		
			showAlert("Amount should be greater than "+lamt + " " + fc , "danger"); //By Mujeeb
			return false;
		}
		/*else if(amnt < 0.05)
		{
			$("#sell_stop_error").show();
			$("#sell_stop_error").html("<center>please Enter Above 0.05 </center>");
			return false;
		}*/



		var dataform=$('#sell_stop').serialize();
		$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createsellorder',
					 beforeSend:function(){
					 $("#sell_stop_error").hide();
						$('.pageloadingBG, .pageloading').css('display', '');
					 },
                     success:function(output) 
                     {					
					 	$('.pageloadingBG, .pageloading').css('display', 'none');
					
                        var doutput = output.trim();
						
						if(doutput == "true")
                        {
							$("#stat_suc").html("Success");
							$("#msg_suc").html("Your Order Has been Placed");
							$('#success-modal').modal('show');						
						}
						else if(doutput == "balance")
						{
							$("#stat").html("Error");
							$("#msg").html("Insufficient Balance");
							$('#error-modal').modal('show');						
						}
						else if(doutput == "empty_value")
						{
							$("#stat").html("Error");
							$("#msg").html("Please Enter Valid Values");
							$('#error-modal').modal('show');						
						}
						else
                        {	
							$("#sell_stop_subtot").html("0.000");
							$('#sell_stop')[0].reset();
							$("#stat_suc").html("Success");
							$("#msg_suc").html("Your Order Has been Placed");
							$('#success-modal').modal('show');
							mapping(doutput);
						}
					}//End of Success
			});
		}//End of Outer If
		else 
		{
			$("#sell_stop_error").show();
			$("#sell_stop_error").html("<center>Please Enter Valid Numeric Values</center>");
		}
});//End of Stop Sell
			
			
			function sell_l_input(amnt)
			{
				if(amnt != "")
				{
					//alert(instant_price);
					var tot = parseFloat(amnt) * parseFloat(instant_sell_price);
					var fee=parseFloat(tot) * parseFloat(comm) /100;			
					var total=parseFloat(tot) - parseFloat(fee);
					$("#sell_l_subtot").html(total.toFixed(sec_cur_dec));
					
				}
				else 
				{
					$("#sell_l_subtot").html("0.000");
				}
			
			}//End of Function
			
			function sell_limit_input()
			{
			
				var amnt=$("#sell_limit_amnt").val();
				var price=$("#sell_limit_price").val();
				if(amnt != "" && price != "")
				{
					//alert(instant_price);
					var tot = parseFloat(amnt) * parseFloat(price);			
					var fee=parseFloat(tot) * parseFloat(comm) /100;			
					var total=parseFloat(tot) - parseFloat(fee);
					$("#sell_limit_subtot").html(total.toFixed(sec_cur_dec));
				}
				else 
				{
					$("#sell_limit_subtot").html("0.000");
				}
			
			}//End of Function
			
			function sell_stop_input()
			{
			
				var amnt=$("#sell_stop_amnt").val();
				var price=$("#sell_stop_price").val();
				if(amnt != "" && price != "")
				{
					//alert(instant_price);
					var tot = parseFloat(amnt) * parseFloat(price);			
					var fee=parseFloat(tot) * parseFloat(comm) /100;			
					var total=parseFloat(tot) - parseFloat(fee);
					$("#sell_stop_subtot").html(total.toFixed(sec_cur_dec));
				}
				else 
				{
					$("#sell_stop_subtot").html("0.000");
				}
			
			}//End of Function
			
			
			
			//common
function mapping(id)
{   	
   var cs_name = $('input[name="cs_name"]').val();
     $.ajax({
	    url: base_url+'mapping',
	    data: "freshorderid="+id+"&cs_name="+cs_name+"&firstCurrency="+firstCurrency+'&secondCurrency='+secondCurrency,
	    type: 'POST',            
	    success: function(res) 
	    {
		//alert(res);
		// if(res == "failure")
		// {
		// successfully placed
		// }
		// else 
		// {
		//successfully completed
		// }
		
		}
    });
}//End of Function


/* setInterval(function(){     
    refreshsellbuyorders(firstCurrency, secondCurrency);
  }, 3000);*/
 
 setInterval(function(){     
	    refreshsellbuyorders_new(firstCurrency, secondCurrency);
 }, 1000);
 
 //Function for updating latest market prices "By Mujeeb"
 function refreshsellbuyorders_new(pair1,pair2)
 {
	 var selected = pair1+'_'+pair2;   
	    //var csrf_test_name = $('input[name="csrf_test_name"]').val();
	    $.get(base_url+'refreshsellbuyorders/'+pair1+'/'+pair2, function(data){ 
		      var sell_orders        = data.sell_orders;
		      var buy_orders         = data.buy_orders;
		      var stop_orders        = data.stop_orders;
		      var trade_history      = data.trade_history;
		      var sell_summary       = data.sell_summary;
		      var buy_summary        = data.buy_summary;
		  
			  
		      var pair_buy_orders    = '';
		      var pair_sell_orders   = '';
		      var pair_stop_orders   = '';
		      var pair_active_orders = '';
		      var pair_trade_history = '';
		      var sec_cur_dec=2;
			  var fir_cur_dec=8;

				if(secondCurrency == "INR" ||  secondCurrency == "AED")
				{
				 var sec_cur_dec=2;
					  var fir_cur_dec=8;
				}	  
				else 
				{
				      var sec_cur_dec=8;
					  var fir_cur_dec=8;
				}
				//BY Mujeeb
				if(firstCurrency=="ETH" || firstCurrency=="ETC" || firstCurrency=="LTC" || firstCurrency=="DASH" || firstCurrency=="BCH" || firstCurrency=="ZEC")
				{
					var fir_cur_dec=6;
				}
				else if(firstCurrency=="XMR")
				{
					var fir_cur_dec=5;
				}
				else if(firstCurrency=="XRP")
				{
					var fir_cur_dec=8;
				}
				
				//---------------------- Active Orders --------------------------------------
				var active_orders = data.active_orders;
				if(active_orders.length>0){
					//alert("M : " + active_orders.length);
			        var act_tot = '',act_fee = '';
			        for(var ka=0;ka<active_orders.length;ka++){
			          act_fee = parseFloat(active_orders[ka].fee);
			          act_fee_per = parseFloat(active_orders[ka].fee_per);
			          cal_fee = parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].amount)*(act_fee_per/100);
			          act_tot = (active_orders[ka].Type=='Buy')?(parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].Amount)+parseFloat(active_orders[ka].Fee)):(parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].Amount)-parseFloat(active_orders[ka].Fee));
			          pair_active_orders += '<tr style="color:#000;font-size:10px;font-weight:normal;"><td>'+active_orders[ka].datetime+'</td><td><span class="'+((active_orders[ka].Type=='Buy')?'green':'red')+'">'+active_orders[ka].Type+'</span></td><td class="sort">'+parseFloat(active_orders[ka].amount).toFixed(fir_cur_dec)+'</td><td>'+parseFloat(active_orders[ka].Price).toFixed(sec_cur_dec)+'</td><td>'+pair1+"-"+pair2+'</td><td>'+active_orders[ka].Fee+'</td><td>'+act_tot.toFixed(sec_cur_dec)+'</td><td><a href="javascript:void(0);" onclick="close_active_order(\''+active_orders[ka].trade_id+'\',this)">Cancel</a></td>';
			        }     
			      }else{
			        pair_active_orders += '<tr><td colspan="8" class="no-records">No orders available</td></tr>';
			      } 
				$('#'+selected+'_history_active_orders tbody').html(pair_active_orders);//Active Orders
				//----------------------------------------------------------------------------
				
				//----------------------Order Book--------------------------------------------
				updateActiveOrders(active_orders,pair1, pair2)
				updateOrderBookLive(buy_orders, sell_orders);
				updateTradeBookLive(trade_history);
				/*if(sell_orders.length>0)
			      {      
			        var sell_title = '';
			        for(var j=0;j<sell_orders.length;j++){
			          sell_title = 'Total '+pair1+': '+parseFloat(sell_orders[j].amount).toFixed(fir_cur_dec)+', Total '+pair2+': '+(parseFloat(sell_orders[j].Price)*parseFloat(sell_orders[j].amount)).toFixed(sec_cur_dec);
			          pair_sell_orders += '<tr title="'+sell_title+'" class="text-danger" href="javascript:void(0);" onclick="set_price(\''+sell_orders[j].Type+'\',\''+parseFloat(sell_orders[j].amount).toFixed(fir_cur_dec)+'\',\''+parseFloat(sell_orders[j].Price).toFixed(sec_cur_dec)+'\')"><td><span class="text-danger">'+parseFloat(sell_orders[j].Price).toFixed(sec_cur_dec)+'</span></td><td>'+parseFloat(sell_orders[j].amount).toFixed(fir_cur_dec)+'</td><td>'+(parseFloat(sell_orders[j].Price)*parseFloat(sell_orders[j].amount)).toFixed(sec_cur_dec)+'</td></tr>';
			        }     
			      }
			      else
			      {
			        pair_sell_orders += '<tr><td colspan="3" class="no-records">No orders available</td></tr>';
			      }*/
				
				
				//console.log(pair_sell_orders);
				//----------------------------------------------------------------------------
			
			    $("#fc_balance").html(data.balances[firstCurrency]);
			    $("#fc_balance").html(parseFloat(data.balances[firstCurrency]).toFixed(fir_cur_dec));
			    $("#sc_balance").html(parseFloat(data.balances[secondCurrency]).toFixed(sec_cur_dec));
			    
			    $("#bprice").val(parseFloat(data.sell_summary.minPrice).toFixed(sec_cur_dec));
			    $("#sprice").val(parseFloat(data.buy_summary.maxPrice).toFixed(sec_cur_dec));
			    
			    $("#last_price").html(data.mprice['last_price']);//By Mujeeb
			    //$("#dchange").html(data.mprice['daily_change']);//By Mujeeb
			   // $("#dpercent").html(data.mprice['daily_ptc']);//By Mujeeb
			    $("#openprice").html(data.mprice['open_price']);//By Mujeeb
			    $("#range").html(data.mprice['range']);//By Mujeeb
			    
			    document.title = "( "+data.mprice['last_price'] + " ) Trade Exchange( " + selected +" )" + " Bitunio"; //By Mujeeb
			    
			    updateDailyChange(data.mprice['daily_change'], data.mprice['daily_ptc']);
		
	    },'json');	
 }//End of Function
 
 function updateDailyChange(dchange, dpercent)
 {
	 var spanPath = '.trade-summary span span';
	 if (dchange > 0) {
         var dailyChangeIconClass = 'fa-caret-up';
         var dailyChangeColor = '#009900';
     } else {
         var dailyChangeIconClass = 'fa-caret-down';
         var dailyChangeColor = '#d9534f';
     }
	 
	 $(spanPath + '.daily-change').html('');
	 $(spanPath + '.daily-change').html(dchange + '&nbsp;<i class="fa '+dailyChangeIconClass+'"></i>&nbsp;('+dpercent+'%)').css('color', dailyChangeColor);
 }//End of Function

function refreshsellbuyorders(pair1,pair2)
  {
	var selected = pair1+'_'+pair2;   
    //var csrf_test_name = $('input[name="csrf_test_name"]').val();
    $.get(base_url+'refreshsellbuyorders/'+pair1+'/'+pair2, function(data){ 
      var sell_orders        = data.sell_orders;
      var buy_orders         = data.buy_orders;
      var stop_orders        = data.stop_orders;
      var trade_history      = data.trade_history;
      var sell_summary       = data.sell_summary;
      var buy_summary        = data.buy_summary;
	  
	  
      var pair_buy_orders    = '';
      var pair_sell_orders   = '';
      var pair_stop_orders   = '';
      var pair_active_orders = '';
      var pair_trade_history = '';
      var sec_cur_dec=2;
	  var fir_cur_dec=8;

	if(secondCurrency == "INR" ||  secondCurrency == "AED")
	{
		  var sec_cur_dec=2;
		  var fir_cur_dec=8;
	}	  
	else
	{
	      var sec_cur_dec=8;
		  var fir_cur_dec=8;
	}
	
	
	$("#fc_balance").html(data.balances[firstCurrency]);
	$("#fc_balance").html(parseFloat(data.balances[firstCurrency]).toFixed(fir_cur_dec));
	$("#sc_balance").html(parseFloat(data.balances[secondCurrency]).toFixed(sec_cur_dec));
	$("#buy_rate").html(parseFloat(data.sell_summary.minPrice).toFixed(sec_cur_dec));
	//$("#buy_limit_price").val(data.sell_summary.minPrice);
	$("#sell_rate").html(parseFloat(data.buy_summary.maxPrice).toFixed(sec_cur_dec));
	//$("#sell_limit_price").val(data.buy_summary.maxPrice);
      if(buy_orders.length>0)
      {
		var buy_title = '';
        for(var i=0;i<buy_orders.length;i++){
		//alert(buy_orders[i].Price); 
          buy_title = 'Total '+pair1+': '+parseFloat(buy_orders[i].amount).toFixed(fir_cur_dec)+', Total '+pair2+': '+((parseFloat(buy_orders[i].Price)*parseFloat(buy_orders[i].amount))).toFixed(sec_cur_dec);
          pair_buy_orders += '<tr title="'+buy_title+'" href="javascript:void(0);" onclick="set_price(\''+buy_orders[i].type+'\',\''+parseFloat(buy_orders[i].amount).toFixed(fir_cur_dec)+'\',\''+parseFloat(buy_orders[i].Price).toFixed(sec_cur_dec)+'\')"><td><span class="green">'+parseFloat(buy_orders[i].Price).toFixed(sec_cur_dec)+'</span></td><td>'+parseFloat(buy_orders[i].amount).toFixed(fir_cur_dec)+'</td><td>'+((parseFloat(buy_orders[i].Price)*parseFloat(buy_orders[i].amount))).toFixed(sec_cur_dec)+'</td></tr>';
        }     
      }
      else
      {
        pair_buy_orders += '<tr><td colspan="3" class="no-records">No orders available</td></tr>';
      }
	  if(sell_orders.length>0)
      {
        // $('.'+pair1+'_'+pair2+'_sell_rate').text(parseFloat(sell_orders[0].price).toFixed(sec_cur_dec));
        // $('#'+pair1+'_'+pair2+' .sell_rate').val(parseFloat(sell_orders[0].price).toFixed(sec_cur_dec));      
        var sell_title = '';
        for(var j=0;j<sell_orders.length;j++){
          sell_title = 'Total '+pair1+': '+parseFloat(sell_orders[j].amount).toFixed(fir_cur_dec)+', Total '+pair2+': '+(parseFloat(sell_orders[j].Price)*parseFloat(sell_orders[j].amount)).toFixed(sec_cur_dec);
          pair_sell_orders += '<tr title="'+sell_title+'" class="text-danger" href="javascript:void(0);" onclick="set_price(\''+sell_orders[j].type+'\',\''+parseFloat(sell_orders[j].amount).toFixed(fir_cur_dec)+'\',\''+parseFloat(sell_orders[j].Price).toFixed(sec_cur_dec)+'\')"><td><span class="text-danger">'+parseFloat(sell_orders[j].Price).toFixed(sec_cur_dec)+'</span></td><td>'+parseFloat(sell_orders[j].amount).toFixed(fir_cur_dec)+'</td><td>'+(parseFloat(sell_orders[j].Price)*parseFloat(sell_orders[j].amount)).toFixed(sec_cur_dec)+'</td></tr>';
        }     
      }
      else
      {
        pair_sell_orders += '<tr><td colspan="3" class="no-records">No orders available</td></tr>';
      }
	  
	  if(stop_orders.length > 0)
      {
        // $('.'+pair1+'_'+pair2+'_sell_rate').text(parseFloat(sell_orders[0].price).toFixed(sec_cur_dec));
        // $('#'+pair1+'_'+pair2+' .sell_rate').val(parseFloat(sell_orders[0].price).toFixed(sec_cur_dec)); active_orders price
        
         for(var js=0;js<stop_orders.length;js++){
          act_fee = parseFloat(stop_orders[js].fee);act_fee_per = parseFloat(stop_orders[js].fee_per);
          cal_fee = parseFloat(stop_orders[js].stoporderprice)*parseFloat(stop_orders[js].amount)*(act_fee_per/100);
          act_tot = (stop_orders[js].Type=='Buy')?(parseFloat(stop_orders[js].stoporderprice)*parseFloat(stop_orders[js].Amount)+parseFloat(stop_orders[js].Fee)):(parseFloat(stop_orders[js].stoporderprice)*parseFloat(stop_orders[js].Amount)-parseFloat(stop_orders[js].Fee));
          pair_stop_orders += '<tr><td>'+stop_orders[js].datetime+'</td><td><span class="'+((stop_orders[js].Type=='Buy')?'green':'red')+'">'+stop_orders[js].Type+'</span></td><td class="sort">'+parseFloat(stop_orders[js].amount).toFixed(fir_cur_dec)+'</td><td>'+parseFloat(stop_orders[js].stoporderprice).toFixed(sec_cur_dec)+'</td><td>'+pair1+"-"+pair2+'</td><td>'+stop_orders[js].Fee+'</td><td>'+act_tot.toFixed(sec_cur_dec)+'</td><td><a href="javascript:void(0);" onclick="cancel_stop_order(\''+stop_orders[js].trade_id+'\',this)">Cancel</a></td>';
        }   
      }
      else
      {
        pair_stop_orders += '<tr><td colspan="8" class="no-records">No orders available</td></tr>';
      }
	  
	  
      
      $('.top_'+pair1+'_'+pair2+' .top_buy_rate').text(to_decimal_with_place(sell_summary.minPrice,sec_cur_dec));
      $('.top_'+pair1+'_'+pair2+' .top_sell_rate').text(to_decimal_with_place(buy_summary.maxPrice,sec_cur_dec));
      $('.top_'+pair1+'_'+pair2+' .top_buy_total').text(to_decimal_with_place(buy_summary.TotalAmount,sec_cur_dec));
      $('.top_'+pair1+'_'+pair2+' .top_sell_total').text(to_decimal_with_place(sell_summary.TotalAmount,sec_cur_dec));

      $('.cur_buy_price').html(to_decimal_with_place(sell_summary.minPrice,sec_cur_dec));
      $('.cur_sell_price').html(to_decimal_with_place(buy_summary.maxPrice,sec_cur_dec));
      
      var active_orders = data.active_orders;
      
      balances[pair1] = data.balances[pair1];
      balances[pair2] = data.balances[pair2];
      $('.'+pair1+'_balance').text(to_decimal_with_place(data.balances[pair1],fir_cur_dec));
      $('.'+pair2+'_balance').text(to_decimal_with_place(data.balances[pair2],sec_cur_dec));

      $('.'+pair1+'_bal').text(to_decimal_with_place(data.balances[pair1],fir_cur_dec));
      $('.'+pair2+'_bal').text(to_decimal_with_place(data.balances[pair2],sec_cur_dec));
      
      if(active_orders.length>8){$('.'+selected+'_history_orders .pair_active_orders').parent().addClass('custome');}
      else{$('#'+selected+'_history_orders .pair_active_orders').parent().removeClass('custome');}

      if(active_orders.length>0){
        var act_tot = '',act_fee = '';
        for(var ka=0;ka<active_orders.length;ka++){
          act_fee = parseFloat(active_orders[ka].fee);act_fee_per = parseFloat(active_orders[ka].fee_per);
          cal_fee = parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].amount)*(act_fee_per/100);
          act_tot = (active_orders[ka].Type=='Buy')?(parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].Amount)+parseFloat(active_orders[ka].Fee)):(parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].Amount)-parseFloat(active_orders[ka].Fee));
          pair_active_orders += '<tr><td>'+active_orders[ka].datetime+'</td><td><span class="'+((active_orders[ka].Type=='Buy')?'green':'red')+'">'+active_orders[ka].Type+'</span></td><td class="sort">'+parseFloat(active_orders[ka].amount).toFixed(fir_cur_dec)+'</td><td>'+parseFloat(active_orders[ka].Price).toFixed(sec_cur_dec)+'</td><td>'+pair1+"-"+pair2+'</td><td>'+active_orders[ka].Fee+'</td><td>'+act_tot.toFixed(sec_cur_dec)+'</td><td><a href="javascript:void(0);" onclick="close_active_order(\''+active_orders[ka].trade_id+'\',this)">Cancel</a></td>';
        }     
      }else{
        pair_active_orders += '<tr><td colspan="8" class="no-records">No orders available</td></tr>';
      } 
      
      /*if(trade_history.length>8){$('#'+selected+'_history_orders .pair_trade_history').parent().addClass('custome');}
      else{$('#'+selected+'_history_orders .pair_trade_history').parent().removeClass('custome');}*/

      if(trade_history.length>0)
      {
        var tot = '';
        for(var k=0;k<trade_history.length;k++){
          tot = (trade_history[k].Type=='Buy')?(parseFloat(trade_history[k].Price)*parseFloat(trade_history[k].Amount)).toFixed(sec_cur_dec):(parseFloat(trade_history[k].Price)*parseFloat(trade_history[k].Amount)).toFixed(sec_cur_dec);
          pair_trade_history += '<tr><td><span class="'+((trade_history[k].Type=='Buy')?'green':'red')+'">'+trade_history[k].Price+'</span></td><td>'+trade_history[k].Amount+'</td><td>'+tot+'</td><td>'+trade_history[k].orderDate +' '+trade_history[k].orderTime+'</td>';
        } 
      }
      else
      {
        pair_trade_history += '<tr><td colspan="4" class="no-records">No Trades available</td></tr>';
      }

      //alert(pair_active_orders);
      $('#'+selected+'_buy_orders tbody').html(pair_buy_orders);
      $('#'+selected+'_sell_orders tbody').html(pair_sell_orders);
      $('#'+selected+'_stop_orders tbody').html(pair_stop_orders);
      $('#'+selected+'_history_active_orders tbody').html(pair_active_orders);//.sort('pair_active_orders','desc');
      //$('#'+selected+'_history_orders .pair_trade_history tbody').html(pair_trade_history);
      $('#'+selected+'_history_orders tbody').html(pair_trade_history);
    },'json');
 } //End of Function






// setInterval(function(){

 // var data = $('#load_form').serialize();
 // $.ajax ({
		// type:"POST",
		// //dataType: 'json',
		// url:base_url+'load_form',
		// data:data,
		// success:function(output){
		
		// var array = output.split('######');
		// $("#fc_balance").html(array[1]);
		// $("#sc_balance").html(array[0]);
		// $("#scrollDivContent").html(array[2]);
		// $("#scrollDivContent1").html(array[3]);
		// $("#scrollDivContent3").html(array[4]);
		// $("#scrollDivContent2").html(array[5]);
		// $("#scrollDivContent4").html(array[6]);
		// $("#load_btc_twd").html(array[7]);
		// $("#load_eth_twd").html(array[8]);
		// $("#load_btc_eth").html(array[9]);
		// }
		// });  

 // }, 2000);
 
 
 function close_active_order(id, obj){
	if(confirm('Are you sure you want to cancel this?')){
		$(obj).parent().parent().css('opacity', '0.2');
		$.get(base_url+'close_active_order/'+id, function(){
			$(obj).parent().parent().remove();
		
		});
	}
}

//cancel_stop_order
function cancel_stop_order(id, obj){
	if(confirm('Are you sure you want to cancel this order?')){
		$(obj).parent().parent().css('opacity', '0.2');
	$.get(base_url+'cancel_stop_order/'+id, function(){
			$(obj).parent().parent().remove();
			refreshpairorderslist();
		});
	}
}


 //draw_chart_data(firstCurrency,secondCurrency, 'chart_div', 1);
 
 draw_chart_data_new(firstCurrency,secondCurrency, 'chart_div', 1);
 
 function draw_chart_data_new(firstCurrency,secondCurrency, divID, segment){
		
	 $.getJSON(base_url+'bitunio/chart_data_new/'+firstCurrency+'/'+secondCurrency, function (data) {
	 //console.log(data);
	 //var dataLength = data.length, i = 0;  
	 // split the data set into ohlc and volume
	 var jsonData = data;
	 var dataLength = jsonData.Data.length, i = 0; 
	 var database = "";
	 var newData = [];
	 
	 for (var j = 0; j < jsonData.Data.length; j++) {
		    var counter = jsonData.Data[j];
		    // console.log(counter.counter_name);
		    // console.log(counter.time +  " : " + counter.high + "<br>");
		    newData.push([(parseInt(counter.time)), parseFloat(counter.high),]); 
	 }

	// return;
	 
	/* var newData = [];
     for (i; i < dataLength; i += 1) {          
          newData.push([(parseInt(data[i].Timeinteger)*1000), parseFloat(data[i].firstPrice),]); 
     }*/
     
     
     $('#'+divID).highcharts({
         chart: {
             zoomType: 'x'
         },
         title: {
             text: firstCurrency+' to '+secondCurrency+' exchange rate over time'
         },
         subtitle: {
             text: document.ontouchstart === undefined ?
                     'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
         },
         xAxis: {
             type: 'datetime'
         },
         yAxis: {
             title: {
                 text: 'Exchange rate'
             }
         },
         legend: {
             enabled: false
         },
         plotOptions: {
             area: {
                 fillColor: {
                     linearGradient: {
                         x1: 0,
                         y1: 0,
                         x2: 0,
                         y2: 1
                     },
                     stops: [
                         [0, Highcharts.getOptions().colors[0]],
                         [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                     ]
                 },
                 marker: {
                     radius: 2
                 },
                 lineWidth: 1,
                 states: {
                     hover: {
                         lineWidth: 1
                     }
                 },
                 threshold: null
             }
         },

         series: [{
             type: 'area',
             name: firstCurrency+' to '+secondCurrency,
             data: newData
         }]
     });
     if(segment == '1')
		$('#'+divID).removeClass('active');
	});	
	 
	 
		
}	//End of Function	


 function draw_chart_data(firstCurrency,secondCurrency, divID, segment){
		
		$.getJSON(base_url+'bitunio/chart_data/'+firstCurrency+'/'+secondCurrency, function (data) {
		//console.log(data);
	    var dataLength = data.length, i = 0;  
		// split the data set into ohlc and volume
        var newData = [];
        for (i; i < dataLength; i += 1) {          
             newData.push([
               (parseInt(data[i].Timeinteger)*1000), // the date
                parseFloat(data[i].firstPrice), // open
            ]); 
        }
        
        
        $('#'+divID).highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: firstCurrency+' to '+secondCurrency+' exchange rate over time'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Exchange rate'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: firstCurrency+' to '+secondCurrency,
                data: newData
            }]
        });
        if(segment == '1')
		$('#'+divID).removeClass('active');
	});	
		
}	//End of Function	
	
	
	
	function to_decimal_with_place(value,places){

	    if(value=='' || value == null)
	    	return 0;
	    else if(parseFloat(value)==0)
	    	return 0;
	    if(parseFloat(value)==parseInt(value))
	    	return parseInt(value);   
	    else{   
	      value = parseFloat(value).toFixed(places);
	      return parseFloat(value);
	    }
	}//End of FUnction
	
  function get_time_string(time_stamp){
    w = new Date(time_stamp * 1000);
    return w.getDate() + '-' + (w.getMonth()+1) + '-' + w.getFullYear() + ' ' + w.getHours() + ':' + w.getMinutes() + ':' + w.getSeconds();
  }//End of Function
  
  $('.number-8').keypress(function(event) {
      var $this = $(this);
      if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
         ((event.which < 48 || event.which > 57) &&
         (event.which != 0 && event.which != 8))) {
             event.preventDefault();
      }

      var text = $(this).val();
      if ((event.which == 46) && (text.indexOf('.') == -1)) {
          setTimeout(function() {
              if ($this.val().substring($this.val().indexOf('.')).length > 9) {
                  $this.val($this.val().substring(0, $this.val().indexOf('.') + 9));
              }
          }, 1);
      }

      if ((text.indexOf('.') != -1) &&
          (text.substring(text.indexOf('.')).length > 8) &&
          (event.which != 0 && event.which != 8) &&
          ($(this)[0].selectionStart >= text.length - 8)) {
              event.preventDefault();
      }      
  });//End of FUnction

  $('.number-8').bind("paste", function(e) {
    var text = e.originalEvent.clipboardData.getData('Text');
    if ($.isNumeric(text)) {
        if ((text.substring(text.indexOf('.')).length > 9) && (text.indexOf('.') > -1)) {
            e.preventDefault();
            $(this).val(text.substring(0, text.indexOf('.') + 9));
       }
    }
    else {
            e.preventDefault();
         }
  });//End of Function

  $('.number-2').keypress(function(event) {
      var $this = $(this);
      if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
         ((event.which < 48 || event.which > 57) &&
         (event.which != 0 && event.which != 8))) {
             event.preventDefault();
      }

      var text = $(this).val();
      if ((event.which == 46) && (text.indexOf('.') == -1)) {
          setTimeout(function() {
              if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                  $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
              }
          }, 1);
      }

      if ((text.indexOf('.') != -1) &&
          (text.substring(text.indexOf('.')).length > 2) &&
          (event.which != 0 && event.which != 8) &&
          ($(this)[0].selectionStart >= text.length - 2)) {
              event.preventDefault();
      }      
  });

  $('.number-2').bind("paste", function(e) {
    var text = e.originalEvent.clipboardData.getData('Text');
    if ($.isNumeric(text)) {alert(text);
        if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
            e.preventDefault();
            $(this).val(text.substring(0, text.indexOf('.') + 3));
       }
    }
    else {alert('text');
            e.preventDefault();
         }
  });
  
  //By Mujeeb
  var carrow = $('.charts h3');
  var carea = $('.charts > div');  
  
  var isShow = false;
  carrow.click(function() {	  
	  carea.toggle();
	  
	  var arrowEl = $(this).children('i');
		if (arrowEl.length) {
			if (arrowEl.hasClass('fa-chevron-down')) {
				arrowEl.removeClass('fa-chevron-down').addClass('fa-chevron-up');
			} else {
				arrowEl.removeClass('fa-chevron-up').addClass('fa-chevron-down');
			}
		}
  });
   
  //End of Function

var arrow = $('.balances-arrow i');
var area = $('#more-currencies');
var isShown = false;
arrow.click(function() {
	if(isShown) {
		area.slideUp({
			complete: function() {
				arrow.removeClass('fa-angle-up');
				arrow.addClass('fa-angle-down');
			}
		});
	} else {
		area.slideDown({
			complete: function() {
				arrow.removeClass('fa-angle-down');
				arrow.addClass('fa-angle-up');
			}
		});
	}
	isShown = !isShown;
});

$('.balances-sidebar h3').click(function(e) {
	$('.balances-sidebar > div').toggle();
	var arrowEl = $(this).children('i');
	if (arrowEl.length) {
		if (arrowEl.hasClass('fa-chevron-down')) {
			arrowEl.removeClass('fa-chevron-down').addClass('fa-chevron-up');
		} else {
			arrowEl.removeClass('fa-chevron-up').addClass('fa-chevron-down');
		}
	}
});


$('.menu-sidebar h3').click(function(e) {
	$('.menu-sidebar ul.nav').toggle();
	var arrowEl = $(this).children('i');
	if (arrowEl.length) {
		if (arrowEl.hasClass('fa-chevron-down')) {
			arrowEl.removeClass('fa-chevron-down').addClass('fa-chevron-up');
		} else {
			arrowEl.removeClass('fa-chevron-up').addClass('fa-chevron-down');
		}
	}
});



var isBuy = function() {
	var sideVal = ($("#type").val()).toLowerCase() === "buy"; //by Mujeeb
	return sideVal;
	//return $("#order-side").val() === "buy";
};
var isSell = function() {
	var sideVal = ($("#type").val()).toLowerCase() === "sell"; //by Mujeeb
	return sideVal;
	//return $("#order-side").val() === "buy";
};

var isLimit = function() {
	var sideVal = ($("#order_type").val()).toLowerCase(); //by Mujeeb
	if(sideVal=="limit"){
		return true;
	}	
	else{		
		return false;	
	}
	//return $("#order-side").val() === "buy";
};


var isPriceInputRequired = function() {
	type = $("#order_type").val();
	//alert("Type : " + type);
	return type !== "instant" && type !== "stop";
};
var isStopPriceInputRequired = function() {
	type = $("#order_type").val();
	return type === "stop_limit" || type === "stop";
};
var getPriceFromInputBySelector = function(selector) {
	var price = parseFloat($(selector).val());
	if (isNaN(price)) {
		return 0.0;
	}
	return price;
};
var getPriceFromInput = function() {
	return getPriceFromInputBySelector('input[name=price]');
};
var getStopPriceFromInput = function() {
	return getPriceFromInputBySelector('input[name=stopPrice]');
};



var updateTotalAmount = function() {
	//return;
	//var sideVal = $("#order-side").val();
	var sideVal = ($("#type").val()).toLowerCase();
	var typeVal = $("#order_type").val();
	//var isBuy = sideVal === "buy";
	var isBuy = sideVal === "buy";   //isBuy = TRUE/FALSE     sideVal = "BUY/SELL
	
	
	//var isMarket = typeVal === "market";
	var isMarket = typeVal === "instant";
	var isStop = typeVal === "stop";
	var isStopLimit = typeVal === "stop_limit";
	var isMarketOrStop = isMarket || isStop;
	//if ((!isBuy && sideVal !== "sell")
	if ((!isBuy && sideVal !== "sell") || (!isMarketOrStop && !isStopLimit && typeVal !== "limit")) {
		//console.log("Error1");
		return;
	}

	var btcAmount = parseFloat($('input[name=amount]').val());
	var price     = getPriceFromInput();
	var stopPrice = getStopPriceFromInput();
	
	//console.log(btcAmount + " : " + price + " : " + stopPrice);
	
	var makerFee = parseFloat($('input[name=fee]').val());//0.0025;
	var takerFee = parseFloat($('input[name=fee]').val());//0.005;
	
	var feeDecimal = takerFee;
	var minAskPriceTmp = isStop || isStopLimit ? stopPrice : minAskPrice;
	var maxBidPriceTmp = isStop || isStopLimit ? stopPrice : maxBidPrice;
	if(!isMarketOrStop) {
		if(isBuy && (minAskPriceTmp === null || price < minAskPriceTmp)) {
			feeDecimal = makerFee;
		}
		if(!isBuy && (maxBidPriceTmp === null || price > maxBidPriceTmp)) {
			feeDecimal = makerFee;
		}
	}
	
	
	
	if (isBuy) {
		var fiatAmountWithoutFee = btcAmount * (isMarketOrStop ? minAskPriceTmp : (isStopLimit ? Math.max(stopPrice, price) : price));
		var fiatAmountInclFee = fiatAmountWithoutFee * (1 + feeDecimal);
		var fiatAmount = fiatAmountInclFee;
	} else {
		var fiatAmountInclFee = btcAmount * (isMarketOrStop ? maxBidPriceTmp : (isStopLimit ? Math.min(stopPrice, price) : price));
		var fiatAmountWithoutFeeAmount = fiatAmountInclFee * (1 - feeDecimal);
		var fiatAmount = fiatAmountWithoutFeeAmount;
	}

	
	
	var pricePrefix = (isMarketOrStop || isStopLimit) ? '~' : '';
	if (fiatAmount===null || isNaN(fiatAmount))
	{
		var fiatAmount = "0.00"
	}
	
	$("#totalamt").html(pricePrefix +"" + parseFloat(fiatAmount).toFixed(8)); //Where 8 is for bitcoin's by Mujeeb
	//alert("" + pricePrefix);
	
	
};//End of updateTotalAmount

$('input[name=amount]').on('change keyup', updateTotalAmount);
$('input[name=price]').on('change keyup', updateTotalAmount);
$('input[name=stopPrice]').on('change keyup', updateTotalAmount);

updateTotalAmount();

var displayOrHidePrice = function() {
	var priceRow = $("input[name='price']");
	if(isPriceInputRequired()) {
		priceRow.show();
	} else {
		priceRow.hide();
	}
	var stopPriceRow = $("input[name='stopPrice']");
	if(isStopPriceInputRequired()) {
		stopPriceRow.show();
	} else {
		stopPriceRow.hide();
	}
	updateTotalAmount();
};

$("#order_type").change(displayOrHidePrice);
displayOrHidePrice();

var setActiveButton = function(allSelector, clickedElement) {
	$(allSelector).removeClass('active');
	clickedElement.addClass('active');
	updateTotalAmount();
};
var setTypeActiveButton = function(clickedElement) {
	setActiveButton('.orders .type a', clickedElement);
};
var setBuySellActiveButton = function(clickedElement) {
	setActiveButton('.orders .buy-sell-buttons a', clickedElement);
};


$('#market-btn').click(function(e){
	//$("#order_type").val("market");
	$("#order_type").val("instant");
	setTypeActiveButton($(this));
	displayOrHidePrice();
});

$('#limit-btn').click(function(e){
	$("#order_type").val("limit");
	setTypeActiveButton($(this));
	displayOrHidePrice();
});

$('#stop-btn').click(function(e){
	$("#order_type").val("stop");
	setTypeActiveButton($(this));
	displayOrHidePrice();
});

$('#stop-limit-btn').click(function(e){
	$("#order_type").val("stop_limit");
	setTypeActiveButton($(this));
	displayOrHidePrice();
});

$('#buy-btn').click(function(e){
	$("#order-side").val("buy");
	$("#type").val("Buy");
	$('#submit-order').removeClass('place-sell-order').addClass('place-buy-order');
	setBuySellActiveButton($(this));
});

$('#sell-btn').click(function(e){
	$("#order-side").val("sell");
	$("#type").val("Sell");
	$('#submit-order').removeClass('place-buy-order').addClass('place-sell-order');
	setBuySellActiveButton($(this));
});

$('#limit-btn').click(); //Click Default chnage by mujeeb

//Initial setup
orderTypeVal = $("#order_type").val();

//alert("order Type Value 1 : " + orderTypeVal);//
if(orderTypeVal === "limit") {
	$('#limit-btn').click();
} else if (orderTypeVal === "stop_limit") {
	$('#stop-limit-btn').click();
} else if (orderTypeVal === "stop") {
	$('#stop-btn').click();
} else {
	$('#market-btn').click();
}

if($("#order-side").val() === "sell") {
	$('#sell-btn').click();
} else {
	$('#buy-btn').click();
}

var updateBuyAndSellPrice = function(data) {
	maxBidPrice = (data.hasOwnProperty('maxBid') && data.maxBid !== null) ? data.maxBid : (data.bids.length > 0 ? data.bids[0].price : null);
	minAskPrice = (data.hasOwnProperty('minAsk') && data.minAsk !== null) ? data.minAsk : (data.asks.length > 0 ? data.asks[0].price : null);
};//End of Function

//Start of Functions
/*$('#submit-order', '#frm-buyAndSell-form').click(function(e) {

	//alert("Mujeeb");
	
	if ($(this).hasClass('disabled')) {		
		e.preventDefault();		
	} else {
		$(this).addClass('disabled');
	}
});//End of submit order*/

/*$('#submit-order').click(function(e) {
	var isStopOrder = isStopPriceInputRequired();
	
	if(!isStopOrder && !isPriceInputRequired()) {
		
		if(isBuy())
		{
			marketBuy();//Make Instat/Markey Buy Order
		}
		if(isSell())
		{
			marketSell();			
		}
	}
});*///End of submit order

//Limit Sell Should be greater also place order

//$('#submit-order', '#frm-buyAndSell-form').on("click", function (event) 

//By Mujeeb
var maxBidPrice = $("#sprice").val(); //23849.215; Sell Price
var minAskPrice = $("#bprice").val();// 23849.58225; Buy Price

$('#submit-order').on("click", function (event) {
	var isStopOrder = isStopPriceInputRequired();
	
	var price = isStopOrder ? getStopPriceFromInput() : getPriceFromInput();
	
	//console.log("Dat : Stop Order: " + isStopOrder + " Price Input: " + isPriceInputRequired() + " Limit Check : " + isLimit() + " Buy : " + isBuy() + " Sell : "+ isSell() + " Price: " + price + " : " + maxBidPrice+" : "+minAskPrice );
	
	if(!isStopOrder && !isPriceInputRequired()) {
		
		if(isBuy())
		{
			$.confirm({
				title: 'Bitunio',
				content: 'You are placing buy order on market price, click yes to proceed', //'You are placing buy order on market price ' + field + ', you want to proceed',
				type: 'blue',
				typeAnimated: true,
				buttons: {
					tryAgain: {
						text: 'Yes',
						btnClass: 'btn-blue',
						keys: ['enter', 'shift'],
						action: function(){
							marketBuy();//Make Instat/Market Buy Order;
						}
					},
					close: function () {								
							
					}
				}
			});
			
		}
		if(isSell())
		{
			 $.confirm({
					title: 'Bitunio',
					content: 'You are placing sell order on market price, click yes to proceed', //'You are placing buy order on market price ' + field + ', you want to proceed',
					type: 'blue',
					typeAnimated: true,
					buttons: {
						tryAgain: {
							text: 'Yes',
							btnClass: 'btn-blue',
							keys: ['enter', 'shift'],
							action: function(){
								marketSell(); //Make Instant/Market Sell Order
							}
						},
						close: function () {								
								
						}
					}
				});
			//marketSell(); //Make Instant/Market Sell Order
						
		}//End of Sell		
		return;
	}//End of if
	var message = null;
	
		
	var price = isStopOrder ? getStopPriceFromInput() : getPriceFromInput();
	
	var buysell = "";
	//alert("Price : " + price);
	if(isBuy() && minAskPrice !== null) {
		if (isStopOrder && price < minAskPrice) {
			message = 'You are placing stop buy order below market price, do you want to continue?';
			buysell = "stop_buy";
		}
		if(!isStopOrder && price > minAskPrice) {
			message = 'You are placing buy order above market price, do you want to continue?';
			buysell = "buy_limit";
		}
		if(!isStopOrder && price < minAskPrice){
			message = 'You are pacing limit buy order below market price, do y0u want to continue?';
			buysell = "buy_limit";
		}
	} else if (!isBuy() && maxBidPrice !== null) {
		if(isStopOrder && price > maxBidPrice) {
			message = 'You are placing stop sell order above market price, do you want to continue?';
			buysell = "stop_sell";
		}
		if(!isStopOrder && price < maxBidPrice) {
			message = 'You are placing sell order below market price, do you want to continue?';
			buysell = "sell_limit";
		}
		if(!isStopOrder && price > maxBidPrice){
			message = 'You are pacing limit sell order above market price, do y0u want to continue?';
			buysell = "sell_limit";
		}
	}
	//alert("Message : " + message);
	if (message !== null) {
		var e = jQuery.Event(event);
		if (!confirm(message)) {
			//alert("m");
			e.stopImmediatePropagation();
			e.preventDefault();
			$('#submit-order', '#frm-buyAndSell-form').removeClass('disabled');
		}
		else
		{
			if(buysell=="sell_limit")
			{
				$.confirm({
					title: 'Bitunio',
					content: 'You are placing Sell Order on Limit Price, click yes to proceed', //'You are placing buy order on market price ' + field + ', you want to proceed',
					type: 'blue',
					typeAnimated: true,
					buttons: {
						tryAgain: {
							text: 'Yes',
							btnClass: 'btn-blue',
							keys: ['enter', 'shift'],
							action: function(){
								marketSell(); //Make Instant/Market Sell Order
							}
						},
						close: function () {								
								
						}
					}
				});
				//marketSell(); //Make Limit Sell Order
			}
			else if(buysell=="stop_sell")
			{
				$.confirm({
					title: 'Bitunio',
					content: 'You are placing Sell Order on Stop Price, click yes to proceed', //'You are placing buy order on market price ' + field + ', you want to proceed',
					type: 'blue',
					typeAnimated: true,
					buttons: {
						tryAgain: {
							text: 'Yes',
							btnClass: 'btn-blue',
							keys: ['enter', 'shift'],
							action: function(){
								marketSell(); //Make Instant/Market Sell Order
							}
						},
						close: function () {								
								
						}
					}
				});
				//marketSell(); //Make Stop Sell Order
			}
			else if(buysell=="buy_limit")
			{
				$.confirm({
					title: 'Bitunio',
					content: 'You are placing Buy Order on Limit Price, click yes to proceed', //'You are placing buy order on market price ' + field + ', you want to proceed',
					type: 'blue',
					typeAnimated: true,
					buttons: {
						tryAgain: {
							text: 'Yes',
							btnClass: 'btn-blue',
							keys: ['enter', 'shift'],
							action: function(){
								marketBuy(); //Make Instant/Market Sell Order
							}
						},
						close: function () {								
								
						}
					}
				});
				//marketBuy();//Make Limit Buy Order
			}	
			else if(buysell=="stop_buy")
			{
				$.confirm({
					title: 'Bitunio',
					content: 'You are placing Buy Order on Limit Price, click yes to proceed', //'You are placing buy order on market price ' + field + ', you want to proceed',
					type: 'blue',
					typeAnimated: true,
					buttons: {
						tryAgain: {
							text: 'Yes',
							btnClass: 'btn-blue',
							keys: ['enter', 'shift'],
							action: function(){
								marketBuy(); //Make Instant/Market Sell Order
							}
						},
						close: function () {								
								
						}
					}
				});
				//marketBuy();//Make Stop Buy Order
			}
		}//End of Else
		
		
	}//end of if
});

function popupConfirm(msg, func)
{
	var conf = false;
	
	       $.confirm({
				title: 'Bitunio',
				content: msg + ', click yes to proceed', //'You are placing buy order on market price ' + field + ', you want to proceed',
				type: 'blue',
				typeAnimated: true,
				buttons: {
					tryAgain: {
						text: 'Yes',
						btnClass: 'btn-blue',
						keys: ['enter', 'shift'],
						action: function(){
							
						}
					},
					close: function () {								
							conf = false;
					}
				}
			});
	       
	       console.log("Status : " + conf);
	       return conf;
}//End of Function


function getMinTradeValue(currency)
{
	var amt = '{"ETH": 0.012, "XRP": 4, "XMR":0.02, "ETC": 0.2, "ZEC": 0.025, "LTC": 0.02,"DASH": 0.01, "BCH": 0.004}';
	var myObj = JSON.parse(amt);
	if(currency=="ETH")
	{
		return myObj.ETH;
	}
	else if(currency=="XRP")
	{
		return myObj.XRP;
	}
	else if(currency=="ETC")
	{
		return myObj.ETC;
	}
	else if(currency=="LTC")
	{
		return myObj.LTC;
	}
	else if(currency=="DASH")
	{
		return myObj.DASH;
	}
	else if(currency=="XMR")
	{
		return myObj.XMR;
	}
	else if(currency=="BCH")
	{
		return myObj.BCH;
	}
	else if(currency=="ZEC")
	{
		return myObj.ZEC;
	}
	
}//End of Function

