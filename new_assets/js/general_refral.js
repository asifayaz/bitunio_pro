jQuery.validator.addMethod("Passwordconditions", function(value, element) {
    return (/^(?=.*[A-Z])(?=.*\d)[A-Za-z\d$@+$!%*#?&]{8,}$/.test(value));
}, "Password must have minimum 8 characters at least 1 Alphabet in Caps, 1 Number 1 Special character");


$("#register").validate({
			rules: {
				username:{required:true},
				//check:{required:true},
				email:{required:true,email:true,},
				password:{required:true,Passwordconditions:true},
				cpassword:{required:true,equalTo:"#pwd",},
				},
			messages: {
				email:{required:'Required',email:'Enter Valid Email Id'},

			},

		   submitHandler: function(form) {
			   
			   var uname = $("#username").val();
			   if(uname=='undefined' || uname=='')
			   {
				   $("#reg_msg_errr").html("Please enter first name and last name...");
				   $('#reg_errr').show();
				   $('#reg_errr').fadeOut(6000);
				   return false;
			   }
			   
			   var email = $("#reg_email").val();
			   if(email=='undefined' || email=='')
			   {
				   $("#reg_msg_errr").html("Please enter valid email...");
				   $('#reg_errr').show();
				   $('#reg_errr').fadeOut(6000);
				   return false;
			   }
			   

           var dataform=$('#register').serialize();
           
             var gres = $('#reg_recaptcha').val();
		     if (gres == ''){
                      // if error I post a message in a div
                $( '#captcha_error2').html( '<p style="color:red;font-size:14px;">Please verify you are human</p>' );
			    return false;
             }
		     else
			 {
				$( '#captcha_error2').html('');
			 } 
				    
				    $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/register_refral',
					 beforeSend:function(){
						//$('.pageloadingBG, .pageloading').css('display', '');
						$("#loader").attr("data-text", "Account is creating please wait...");						
						$('#loader').show();						
						$('#register_btn').attr('disabled', true);
					 },
                     success:function(output)
                     {
						$('#captcha_error2' ).hide();
						$('#loader').hide();
						//$('.pageloadingBG, .pageloading').css('display', 'none');
						//$('#register_loader').hide();
                        var doutput = output.trim();

						if(doutput == "success")
                        {
							$('#loader').hide();	
							$('#register_btn').attr('disabled', false);
							$('#reg_succc').show();
							$("#reg_msg_succc").html("You have registered successfully, please check your mail and activate the account");
							$('#reg_succc').fadeOut(10000);
							setTimeout(function(){ window.location.href=base_url; }, 10000);

                         }
						else if(doutput == "email")
                        {
							$('#register_btn').attr('disabled', false);
							//$("#stat").html("Error");
							$("#reg_msg_errr").html("Email Already Exist Please try Another");
							$('#reg_errr').show();
							$('#reg_errr').fadeOut(6000);
						}
						else if(doutput == "agree")
                        {
							$('#register_btn').attr('disabled', false);
							//$("#stat").html("Error");
							$("#reg_msg_errr").html("Please verify you are human");
							$('#reg_errr').show();
							$('#reg_errr').fadeOut(6000);
						}
						else if(doutput == "captcha_error")
                        {
							$('#register_btn').attr('disabled', false);
							//$("#stat").html("Error");
							$("#reg_msg_errr").html("Captcha is incorrect");
							$('#reg_errr').show();
							$('#reg_errr').fadeOut(6000);
						}
						else
                        {
							$('#register_btn').attr('disabled', false);
							//$("#stat").html("Error");
							$("#reg_msg_errr").html("Some errors occured please try again later");
							$('#reg_errr').show();
							$('#reg_errr').fadeOut(4000);
						 }
				   }
                 });
			//}

           },
	});//End of Register

		//Affiliate User
		$("#login_form1").validate({
			rules: {
				clientid:{required:true,number:true},
				password:{required:true,},
			},
			messages: {

				password:{required:'Required'},
				clientid:{required:'Required',number:'Please enter valid Client Id, only numeric values'},
			},

			submitHandler: function(form) {
            var dataform = $('#login_form1').serialize();
		    var gres = $('#res_signup_recaptcha').val();
		   // var gres = 1;
		    if (gres == ''){
                // if error I post a message in a div
                $( '#captcha_error1').html( '<p style="color:red;font-size:14px;">Please verify you are human</p>' );
				  return false;
            }
			else
			{
			    $( '#captcha_error1').html('');
			}
		    /* if (gres == ''){
                      // if error I post a message in a div
                      $( '#captcha_error1').html( '<p style="color:red">Please verify you are human</p>' );
					  return false;
                  }
				  else
				  {*/
				  //  $( '#captcha_error1').html('');

		    	$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/login',
					 beforeSend:function(){
							//$('.pageloadingBG, .pageloading').css('display', '');
						 	$("#loader").attr("data-text", "Validating client please wait...");
							// $('#loader').data('text','Validating please wait....'); //setter
							$('#loader').show();
							$('#login_btna').attr('disabled', true);
					 },
                     success:function(output)
                     {
                    	 $('#loader').hide();
					   // $('.pageloadingBG, .pageloading').css('display', 'none');
						var doutput = output.trim();
						var doutput1 = doutput.substring(0, 8);

						if(doutput == "success" || doutput == "verifylocation")
                        {
							window.location.href='trade';
                        }
						if(doutput == "success1")
                        {
							window.location.href='profile/verification/1';
                        }
						else if(doutput == "error")
                        {
							$('#login_btna').attr('disabled', false);
							$("#msg_errr_login_bro").html("Invalid Client id / password");
							$('#errr_login_bro').show();
							$('#errr_login_bro').fadeOut(6000);
						}
						else if(doutput == "change_date")
                        {
							$('#login_btna').attr('disabled', false);
							$("#msg_errr_login_bro").html("Your Password has expired, Please proceed to reset the password with the forgot Password LInk below");
							$('#errr_login_bro').show();
							$('#errr_login_bro').fadeOut(6000);
						}
						else if(doutput == "deactive")
                        {
							$('#login_btna').attr('disabled', false);
							$("#msg_errr_login_bro").html("Your account has been deactivated temproarily, Please contact support@bitunio.com");
							$('#errr_login_bro').show();
							$('#errr_login_bro').fadeOut(6000);
						}
						else if(doutput == "emailcheck")
                        {
							$('#login_btna').attr('disabled', false);
							$("#msg_errr_login_bro").html("Please Check your mail and activate your Account");
							$('#errr_login_bro').show();
							$('#errr_login_bro').fadeOut(6000);
						}
						else if(doutput == "enable")
                        {
							$('#login').modal('hide');
							$('#login1').modal('hide');
							$('#tfa').modal('show'); //two factor enabled
						}
						else
						{
							if(doutput1 == "You have")
							{
								$('#login_btna').attr('disabled', false);
								$("#msg_errr_login_bro").html(doutput);
								$('#errr_login_bro').show();
								$('#errr_login_bro').fadeOut(6000);
							}
						}
                     }
                 });
			// }

         },


	});//End of Affliate Login



if(vstatus == "verified_ip")
{
	$('#login_btn').attr('disabled', false);
	//showAlert("Invalid Credientials!", "danger"); //By Mujeeb
	$("#msg_succc_login").html("Your new ip address has been verified, Please wait till it is redirected to login Page.");
	$('#succc_login').show();
	//$('#succc_login').fadeOut(30000);
	
	setTimeout(function() {
		var url = base_url+'login';
		window.location.href = url;		
	}, 10000);
}
else if(vstatus == "deactive")
{
	$('#login_btn').attr('disabled', true);
	//showAlert("Invalid Credientials!", "danger"); //By Mujeeb
	$("#msg_errr_login").html("Dear customer your account has been disabled due to unauthorised access, please contact bitunio support to enable the account...");
	$('#errr_login').show();
	//$('#succc_login').fadeOut(30000);
	
	setTimeout(function() {
		var url = base_url;
		window.location.href = url;		
	}, 10000);
}

//Login User		
$("#login_form").validate({
			rules: {
				email:{required:true,email:true},
				password:{required:true,},
			},
			messages: {
				email:{required:'Required',email:'Enter Valid Email Id'},
				password:{required:'Required'},
			},			
		   submitHandler: function(form) {
           var dataform=$('#login_form').serialize();       
           var gres = $('#res_recaptcha').val();
		   // var gres = 1;
		    	 if (gres == ''){
                      // if error I post a message in a div
                      $( '#captcha_error').html( '<p style="color:red;font-size:14px;">Please verify you are human</p>' );
					  return false;
                  }
				  else
				  {
				    $( '#captcha_error').html('');
				  }  
           
           
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/login',
					 beforeSend:function(){
						 	//$('.pageloadingBG, .pageloading').css('display', '');
						 	$('#login_btn').attr('disabled', true);
					 },
                     success:function(output)
                     {
					   // $('.pageloadingBG, .pageloading').css('display', 'none');
						var doutput = output.trim();
						var doutput1 = doutput.substring(0, 8);

						//console.log("URL : " + base_url+'bitunio/login' + "  :" + doutput +  " :  " + doutput1);
						//if(doutput == "success")
						if(doutput == "success-2fa")	
                        {			   
							//window.location.href = 'dashboard';
							//window.location.href = 'trade';
							window.location.href = 'profile';
							
                        }
						if(doutput == "success1")
                        {
							window.location.href='profile/verification/1';
                        }
						else if(doutput == "verifylocation")
						{
							$('#login_btn').attr('disabled', false);
							//showAlert("Invalid Credientials!", "danger"); //By Mujeeb
							$("#msg_errr_login").html("You tried to login from new location so for security reason, an email has been sent to registered email id, please click on the confirmation link");
							$('#errr_login').show();
							$('#errr_login').fadeOut(30000);
							
							//window.location.href='index';
						}
						else if(doutput == "error")//Incomplete Userid or passowrd
                        {
							$('#login_btn').attr('disabled', false);
							//showAlert("Invalid Credientials!", "danger"); //By Mujeeb
							$("#msg_errr_login").html("Invalid email id / password");
							$('#errr_login').show();
							$('#errr_login').fadeOut(6000);

						}
						else if(doutput == "deactive")//if user 3 times invalid logins or user not found account will be deactivated
                        {
							$('#login_btn').attr('disabled', false);
							$("#msg_errr_login").html("Your account has been deactivated temproarily, Please contact support@bitunio.com");
							$('#errr_login').show();
							$('#errr_login').fadeOut(6000);
						}
						else if(doutput == "change_date")//If Password Expired
                        {
							$('#login_btn').attr('disabled', false);
							$("#msg_errr_login").html("Your Password has expired, Please proceed to reset the password with the forgot Password LInk below");
							$('#errr_login').show();
							$('#errr_login').fadeOut(6000);
						}
						else if(doutput == "emailcheck")//Email Verification no done
                        {
							$('#login_btn').attr('disabled', false);
							$("#msg_errr_login").html("Please Check your mail and activate your Account");
							$('#errr_login').show(); 
							$('#errr_login').fadeOut(6000);
						}
						else if(doutput == "enable")//if two factor enabled  tfa_status=enable
                        {
							toggle(4); //Hide all ui
							$('#2faform').modal('show'); //Two factor enabled
						}
						else
						{
							if(doutput1 == "You have")
							{
								$('#login_btn').attr('disabled', false);
								$("#msg_errr_login").html(doutput);
								$('#errr_login').show();
								$('#errr_login').fadeOut(6000);
							}
						}
                     }
                 });

         },


});


$("#forgot_form").validate({
			rules: {
				email:{required:true,email:true},
			},
			messages: {

			},

			submitHandler: function(form) {
           var dataform=$('#forgot_form').serialize();

		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/forgotpasswords',
					 beforeSend:function(){
						 $("#loader").attr("data-text", "Validating please wait....");
						// $('#loader').data('text','Validating please wait....'); //setter
						 $('#loader').show();
						//$('.pageloadingBG, .pageloading').css('display', '');
						$('#forgot_btn').attr('disabled', true);
						},
                    success:function(output)
                    {
					//$('.pageloadingBG, .pageloading').css('display', 'none');
                    	$('#loader').hide();
                        var doutput = output.trim();

						if(doutput == "success")
                        {
							$('#forgot_btn').attr('disabled', false);
							$("#forg_mail").val('');
							$("#succc_forgot").show();
							$("#msg_succc_forgot").html("Please check your email and reset your password");
							$('#succc_forgot').fadeOut(10000);
							window.setTimeout(function(){
								window.location.href = base_url;},5000); 
							//$("#forgot").modal('hide');

                         }
						 else
                        {
							$('#forgot_btn').attr('disabled', false);
							$("#errr_forgot").show();
							$("#msg_errr_forgot").html("Email does not exist please enter valid email");
							$('#errr_forgot').fadeOut(4000);
						}
					}
                 });
         },
	});

$("#reset_form").validate({
			rules: {

				npassword:{required:true,Passwordconditions:true},
				rpassword:{required:true,equalTo:'#123'},
					},
			messages: {
				npassword:{required:'Required'},
				rpassword:{required:'Required',equalTo:'Password does not match'},

					},

					submitHandler: function(form) {
           var dataform=$('#reset_form').serialize();

		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/reset_password',
					 beforeSend:function(){
						//$('.pageloadingBG, .pageloading').css('display', '');
						$("#loader").attr("data-text", "Please wait resetting...");
						// $('#loader').data('text','Validating please wait....'); //setter
						 $('#loader').show();
						$('#reset_btn').attr('disabled', true);

						},
                     success:function(output)
                     {
                    	 //$('.pageloadingBG, .pageloading').css('display', 'none');
					 	$('#loader').hide();
						//$('#reset_loader').hide();
                        var doutput = output.trim();
						if(doutput == "success")
                        {
							$('#succc_reset').show();
							//$('#forgot_btn').attr('disabled', true);
							$("#msg_succc_reset").html("Your password has changed successfully, Your password will Expiry in "+pass_ex+" days");
							$('#succc_reset').fadeOut(15000);
							window.setTimeout(function(){
								window.location.href = base_url;},5000);
						}
						else
                        {
							$('#errr_reset').show();
							$("#msg_errr_reset").html("Some error Please try again");
							$('#errr_reset').fadeOut(4000);
						}
                     }
                 });

         },

});

function change_type1(firstname)
{
	//var ty       = $("#type").val();
	var lastname = $("#lname").val();
	var uname = "";
	
		uname = firstname + " " + lastname;
		//console.log( "M : " + firstname + " : " + lastname);
		$("#username").val(uname);
		//$("#trm").html('<div class="checkbox"><label><input type="checkbox" value="1" name="check"><span class="cr"><i class="cr-icon fa fa-check"></i></span>i agree the <a href="'+base_url+'terms" target="_blank">Terms & conditions</a></label></div>');
}//End of Function

function change_type2(lastname)
{
	//var ty       = $("#type").val();
	var firstname = $("#fname").val();
	var uname = "";
	
	uname = firstname + " " + lastname;
	
	//console.log( "M : " + firstname + " : " + lastname);
	$("#username").val(uname);
	
	/*if(type == "individual")
	{
		$("#trm").html('<div class="checkbox"><label><input type="checkbox" value="1" name="check"><span class="cr"><i class="cr-icon fa fa-check"></i></span>i agree the <a href="'+base_url+'terms" target="_blank">Terms & conditions</a></label></div>');
	}
	else
	{
		$("#trm").html('<div class="checkbox"><label><input type="checkbox" value="1" name="check"><span class="cr"><i class="cr-icon fa fa-check"></i></span>Agree the <a href="'+base_url+'terms" target="_blank">terms & conditions</a></label></div><div class="checkbox"><label><input type="checkbox" value="1" name="check1"><span class="cr"><i class="cr-icon fa fa-check"></i></span>Agree the <a href="'+base_url+'broker_terms" target="_blank">Affiliate terms & conditions</a></label></div>');
	}*/
}//End of Function


function change_type(type)
{
	if(type == "individual")
	{
		$("#trm").html('<div class="checkbox"><label><input type="checkbox" value="1" name="check"><span class="cr"><i class="cr-icon fa fa-check"></i></span>i agree the <a href="'+base_url+'terms" target="_blank">Terms & conditions</a></label></div>');
	}
	else
	{
		$("#trm").html('<div class="checkbox"><label><input type="checkbox" value="1" name="check"><span class="cr"><i class="cr-icon fa fa-check"></i></span>Agree the <a href="'+base_url+'terms" target="_blank">terms & conditions</a></label></div><div class="checkbox"><label><input type="checkbox" value="1" name="check1"><span class="cr"><i class="cr-icon fa fa-check"></i></span>Agree the <a href="'+base_url+'broker_terms" target="_blank">Affiliate terms & conditions</a></label></div>');
	}
}

$("#tfa_form").validate({
			rules: {
				onecode:{required:true,number:true,},

			},
			messages: {

			},

		   submitHandler: function(form) {
           var dataform=$('#tfa_form').serialize();
		   var email=$("#email2fa").val();
		   var clientid=$("#client2fa").val();

		   $.ajax({
                     type:'POST',
                     data:dataform+"&email="+email+"&clientid="+clientid,
                     url:base_url+'bitunio/tfa_confirm',
					 beforeSend:function(){
					//$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output)
                     {
						//$('.pageloadingBG, .pageloading').css('display', 'none');
                        var doutput = output.trim();

						if(doutput == "success")
                        {
						   // window.location.href='dashboard';
						    window.location.href='trade';
                        }
						else
                        {
							$('#errr_2fa').show();
							$("#msg_errr_2fa").html("Invalid Google Authentication Code");
							$('#errr_2fa').fadeOut(4000);
							$('#2fa_btn').attr('disabled', false);

						}

                     }
                 });

         },
});

//Check Account active or Not
function AccountActive()
{
	setInterval(function(){     
		checkaccountActive();
	}, 5000);
}//End of Function

function checkaccountActive()
{
	var dataform= "";
	$.ajax({
        type:'POST',
        data:dataform,
        url:base_url+'bitunio/AccountStatus',
		 beforeSend:function(){
		//$('.pageloadingBG, .pageloading').css('display', '');
			},
        success:function(output)
        {
		  // $('.pageloadingBG, .pageloading').css('display', 'none');
           var doutput = output.trim();

          // console.log("M : " + doutput);
          // alert("m" + doutput);
		   if(doutput == "pending")
           {
			  // var stat = "<?php $stat=$this->session->userdata('stat'); ?>";
			   var stat = "<?php echo $uri2; ?>";
			   
			  // console.log("View : " + stat);
			   if(stat == "verifiation")
			   {
				  /* var str = "<span style='color:orange'>Pending </br></br></br><span style='color:black'>We are reviewing your identity right now, we will get back to you after verification complete.</span></span>";
				   document.getElementById("verify").innerHTML = str; 
				   document.getElementById("verifybutton").style.display='none';*/
			   }
			   else if(stat == "")
			   {
				   //window.location.href='profile/verification';
			   }
			   else
			   {
				   //window.location.href='profile/verification'; 
			   }			
           }
		   else if(doutput == "deactive")
		   {
			   window.location.href = base_url; 
		   }

        }
    });
}//End of Function

//Checking Account verification status

checkaccountverify();

setInterval(function(){     
	checkaccountverify();
}, 5000);

function checkaccountverify()
{
	var dataform= "";
	$.ajax({
        type:'POST',
        data:dataform,
        url:base_url+'bitunio/verificationstatus',
		 beforeSend:function(){
		//$('.pageloadingBG, .pageloading').css('display', '');
			},
        success:function(output)
        {
		  // $('.pageloadingBG, .pageloading').css('display', 'none');
           var doutput = output.trim();

          // console.log("M : " + doutput);
          // alert("m" + doutput);
		   if(doutput == "pending")
           {
			  // var stat = "<?php $stat=$this->session->userdata('stat'); ?>";
			   var stat = "<?php echo $uri2; ?>";
			   
			  // console.log("View : " + stat);
			   if(stat == "verifiation")
			   {
				   var str = "<span style='color:orange'>Pending </br></br></br><span style='color:black'>We are reviewing your identity right now, we will get back to you after verification complete.</span></span>";
				   document.getElementById("verify").innerHTML = str; 
				   document.getElementById("verifybutton").style.display='none';
			   }
			   else if(stat == "")
			   {
				   window.location.href='profile/verification';
			   }
			   else
			   {
				   window.location.href='profile/verification'; 
			   }			
           }
		   else
           {
			   //window.location.href='profile/verification/1'; 
		   }

        }
    });
}//End of Function

function openTerms()
{
		window.location.href = "terms";
}
function openBrokerTerms()
{
		window.location.href = "broker_terms";
}



function showforgot()
{
	
	$("#forgotform").show();
	$("#2faform").hide();
	$("#login-link").show();
	$("#aff-link").hide();
	
	$("#signupform").hide();
	$("#loginform").hide();
	$("#resetform").hide();
	$("#affloginform").hide();
}//End of Function

if(stat=="present")
{
	$("#signupform").hide();
	$("#loginform").hide();
	$("#login-link").show();
	$("#forgotform").hide();
	$("#affloginform").hide();
	$("#resetform").show();	
	$("#2faform").hide();
	$("#123").focus();
}
else
{
	$("#signupform").hide();
	$("#loginform").show();
	$("#2faform").hide();
	$("#login-link").hide();
	$("#aff-link").show();	
	$("#forgotform").hide();
	$("#resetform").hide();	
	$("#affloginform").hide();
}

toggle(3);

function toggle(opt)
{
	if(opt==1)// if Login Clicked
	{		
		$("#login-link").hide();
		$("#aff-link").show();
		$("#2faform").hide();
		
		$("#affloginform").hide();
		$("#loginform").show();
		$("#resetform").hide();		
		$("#forgotform").hide();
		$("#signupform").hide();
	}
	else if(opt==2) // if Affliate Clicked
	{
		$("#login-link").show();
		$("#aff-link").hide();		
		$("#affloginform").show();
		$("#clientid").val('');
		$("#clientid").focus();
		$("#password").val('');
		
		$("#2faform").hide();
		$("#loginform").hide();
		$("#resetform").hide();		
		$("#forgotform").hide();
		$("#signupform").hide();
		
	}
	else if(opt==3)//Register/Signup
	{
		$("#signupform").show();
		$("#loginform").hide();
		$("#resetform").hide();		
		$("#forgotform").hide();
		$("#affloginform").hide();
		$("#2faform").hide();
		$("#login-link").show();
		$("#aff-link").hide();
		$("#password").val('');
		
		$("#fname").focus();
	}
	else if(opt==4)// Individual 2fa
	{		
		//$(".modal-backdrop").hide();
		$('.modal-backdrop').remove();
		
		//console.log("Closed");
		$("#2faform").show();
		$("#login-link").show();
		$("#aff-link").hide();
		
		$("#affloginform").hide();
		$("#loginform").hide();
		$("#resetform").hide();		
		$("#forgotform").hide();
		$("#signupform").hide();
	}
	else if(opt==5)// Client Id 2fa
	{
		//$(".modal-backdrop").hide();
		$('.modal-backdrop').remove();
		$("#2faform").show();
		$("#login-link").show();
		$("#aff-link").hide();
		
		$("#affloginform").hide();
		$("#loginform").hide();
		$("#resetform").hide();		
		$("#forgotform").hide();
		$("#signupform").hide();
	}
	
}//End of Function

function loadtotfae()
{	
	var email =$("#email").val();
	$("#email2fa").val(email);
	
}//End of Function

function loadtotfac() //Client ID
{
	var clid =$("#clientid").val();
	$("#client2fa").val(clid);
	
}

//end of Checking Verification status