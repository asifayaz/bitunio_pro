var data_key = '6Leqi2MUAAAAAAZEPp_NXtkcuzqyJ2IvKZ3CAwES';

var verifyCallback1 = function(response) {
	$('#res_recaptcha').val(response);
};

var verifyCallback2 = function(response1) {
	$('#res_signup_recaptcha').val(response1);
	//$('#res_signup_recaptcha').val("1");
};

var verifyCallback3 = function(response) {
	//console.log("Res : " + response);
	$('#reg_recaptcha').val(response);
};


var widgetId1;
var widgetId2;
var widgetId3;
var onloadCallback = function() {
// Renders the HTML element with id 'example1' as a reCAPTCHA widget.
// The id of the reCAPTCHA widget is assigned to 'widgetId1'.
	//Normal Login 
	widgetId1 = grecaptcha.render('recaptcha', {
	'sitekey' : data_key,
	'theme' : 'light',
	'callback' : verifyCallback1,
	});

	//Affliates Login
	widgetId2 = grecaptcha.render('signup_recaptcha', {
	'sitekey' : data_key,
	'theme' : 'light',
	'callback' : verifyCallback2,
	});
	
	//Signup Capacha
	widgetId3 = grecaptcha.render('sig_recaptcha', {
		'sitekey' : data_key,
		'theme' : 'light',
		'callback' : verifyCallback3,
	});
};



//Function by Mujeeb: To Show order status on notification
function showAlert(message, atype)
{
	 var sclass;
	 if(atype=="success")//Set success notification
	 {
		 sclass = "alert alert-success";
	 }
	 else if(atype=="info")//Set info notification
	 {
		 sclass = "alert alert-info";
	 }
	 else if(atype=="warning")//Set warning notification
	 {
		 sclass = "alert alert-warning";
	 }
	 else if(atype=="danger")//Set danger notification
	 {
		 sclass = "alert alert-danger";
	 }
	 $("#success-alert").removeClass(); //Remove previous all classes
	 
	 //$('#alert').css('display', 'block');//Alert Box CSS
 	 $("#error").html(message); //Set order status to notification	 
 	 $("#success-alert").addClass(sclass);//Set type of alert to notification
 	 
 	 //Show and hide order notification after 5 seconds
     $("#success-alert").fadeTo(5000, 500).slideUp(500, function(){
    	 $("#error").html("");
    	 $("#success-alert").slideUp(500);
     });   
 
}//End of Function