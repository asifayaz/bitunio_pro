<?php defined('BASEPATH') OR exit('No direct script access allowed');

defined('BASEPATH') OR exit('No direct script access allowed');
require "vendor/autoload.php";
use Monero\Wallet;


class Monero_wallet_model extends CI_Model {  
	/*
	* Mainly Source From : https://en.bitcoin.it/wiki/Original_Bitcoin_client/API_calls_list
	*/
	protected $id = 0;
	public function __construct() 
	{
		parent::__construct();	
		
		    //$wallet_portnumber = '18084';//Commented by Mujeeb
		    $wallet_portnumber = '18082';
			$wallet_allow_ip   = '35.177.165.200';//54.254.170.114';
			$this->wallet_port = $wallet_portnumber ;
			$this->wallet_ip   = $wallet_allow_ip ;
			$this->version	   = "2.0";
			$this->id = 0;
			$this->url = "http://$wallet_allow_ip:$wallet_portnumber/json_rpc";
	}
	
	public function balance()
	{

		$url 			=	'35.177.165.200/xmr_api.php';//'54.254.170.114/xmr_api.php';

		$name 			= 	$_SERVER['SERVER_NAME'];

		$data 			= 	array("method" => 'getBalance', "name" => $name, "keyword" => 'getBalance','data'=>'data');

		$data_string 	= 	json_encode($data);

		$ch 			=	curl_init($url);

		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$response 		= 	curl_exec( $ch );

		curl_close($ch);

		$result 		= 	json_decode($response);

        $balance		= 	$result->balance;   
		
		return $balance;
	}
	
	public function create_address()
	{
		$url 			=	'35.177.165.200/xmr_api.php';//'54.254.170.114/xmr_api.php';

		$name 			= 	$_SERVER['SERVER_NAME'];

		$data 			= 	array("method" => 'createaddress', "name" => $name, "keyword" => 'createaddress','data'=>'data');

		$data_string 	= 	json_encode($data);

		$ch 			=	curl_init($url);

		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$response 		= 	curl_exec( $ch );

		curl_close($ch);

		$result 		= 	json_decode($response);
				
        $walletdata["XMR"]			= 	$result->XMR;   

        $walletdata["payment_id"]	=	$result->payment_id;

        return $walletdata;
   	}
   
   	public function transfer_xmr($to_address,$amount,$payment_id,$options1)
	{		
		$url 			=	'35.177.165.200/xmr_api.php';//'54.254.170.114/xmr_api.php';

		$name 			= 	$_SERVER['SERVER_NAME'];

		$data 			= 	array("method" => 'transfer', "payment_id" => trim($payment_id), "amount" =>  $amount,'to_address'=>$to_address);

		/*echo '<pre>';
			print_r($data);
		echo '</pre>';
		die;*/
		$data_string 	= 	json_encode($data);

		$ch 			=	curl_init($url);

		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$response 		= 	curl_exec( $ch );

		curl_close($ch);

		$result 		= 	json_decode($response);

		if($result->tx_hash)
		{
			return $result->tx_hash;
		}
		else
		{
			return $result->message;
		}
	}

	  

} // end of class
