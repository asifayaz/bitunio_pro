<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Exchange extends CI_Controller
		{
			
	public function __construct() 
	{
		parent::__construct();

				error_reporting(E_ERROR);
				$this->load->database();		
				$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
				$this->output->set_header("Pragma: no-cache");
				header('X-Frame-Options: SAMEORIGIN'); 
				//header('Access-Control-Allow-Origin: true');
				header('X-XSS-Protection: 1; mode=block');
				header('X-Content-Type-Options: nosniff');
				$base=base_url();
				header("ALLOW-FROM: $base");
				header("X-Powered-By: $base");

				ini_set('session.gc_maxlifetime',300);
				ini_set('session.cookie_httponly', 1);	
				ini_set('session.use_only_cookies', 1);
				ini_set('session.cookie_secure', 1); 
				if(!log_in())
				redirect('');
				$this->username=username();
				
				$user_newid = log_in();
			if(!$user_newid)
			redirect("");
				
			}
			function index()
			{
				$customer_user_id	=  $this->session->userdata('user_id'); 				 
				 
				$less_amount_btc=$this->user_model->min_max_exchange("BTC","INR")->less_amount;
				
				$converted_amount = $this->user_model->get_Exchange_Market_Price("BTC_INR");//By Mujeeb
				//BTC_INR
				//$get              = file_get_contents("https://finance.google.com/finance/converter?a=1&from=BTC&to=INR&meta=ei%3DLIa6WdjlGoyouQT6oJnoDA");
				//$get              = explode("<span class=bld>",$get);
				//$get              = explode("</span>",$get[1]);
				//$converted_amount = preg_replace("/[^0-9.]/", null, $get[0]);
				$val=$converted_amount * 1;
				$vall=$val - $less_amount_btc;
				$data['BTC_INR']=number_format($vall,2, '.','');
			//	print_R($data); exit;
				//BTC_AED
				
				
				
				$less_amount_aed=$this->user_model->min_max_exchange("BTC","AED")->less_amount;
				
				$converted_amount1 = $this->user_model->get_Exchange_Market_Price("BTC_AED");//By Mujeeb
				/*$get1              = file_get_contents("https://finance.google.com/finance/converter?a=1&from=BTC&to=AED&meta=ei%3Dz4a6WaGYLJOKuwTijoe4DA");
				$get1              = explode("<span class=bld>",$get1);
				$get1              = explode("</span>",$get1[1]);
				$converted_amount1 = preg_replace("/[^0-9.]/", null, $get1[0]);*/
				
				$val1=$converted_amount1 * 1;
				$vall1=$val1 - $less_amount_aed;
				$data['BTC_AED']=number_format($vall1,2, '.','');
				//ETH_INR
				$less_amount_eth = $this->user_model->min_max_exchange("ETH","INR")->less_amount;
				
				$eth_less = $this->user_model->get_Exchange_Market_Price("ETH_INR");//By Mujeeb
				//$eth_less=55785.88;
				$valll=$eth_less - $less_amount_eth;
				$data['ETH_INR']=number_format($valll,2, '.','');			
				//ETH_AED				
				//$get_eth_aed=file_get_contents("https://fx-rate.net/ca.php?currency_pair=ETHAED");	
				$less_amount_ethaed = $this->user_model->min_max_exchange("ETH","AED")->less_amount;	
				
				$bal = $this->user_model->get_Exchange_Market_Price("ETH_AED");//By Mujeeb
				//$bal=998.85;
				$valo=$bal - $less_amount_ethaed;			
				$data['ETH_AED']=number_format($valo,2, '.','');
				$data['fee_BTC_INR']=$this->user_model->exchange_fee("BTC_INR");
				$data['fee_BTC_AED']=$this->user_model->exchange_fee("BTC_AED");
				$data['fee_ETH_INR']=$this->user_model->exchange_fee("ETH_INR");
				$data['fee_ETH_AED']=$this->user_model->exchange_fee("ETH_AED");
				$data['fee_INR_BTC']=$this->user_model->exchange_fee("INR_BTC");
				$data['fee_INR_ETH']=$this->user_model->exchange_fee("INR_ETH");
				$data['fee_AED_ETH']=$this->user_model->exchange_fee("AED_ETH");
				$data['fee_AED_BTC']=$this->user_model->exchange_fee("AED_BTC");
				
				//balances
				$data['btc_bal']=$this->user_model->fetchuserbalancebyId($customer_user_id,'BTC');
				$data['eth_bal']=$this->user_model->fetchuserbalancebyId($customer_user_id,'ETH');
				$data['inr_bal']=$this->user_model->fetchuserbalancebyId($customer_user_id,'INR');
				$data['aed_bal']=$this->user_model->fetchuserbalancebyId($customer_user_id,'AED');
				$query = $this->user_model->get_data('giZfInSoOcZeItSiOs','','','','','','row'); 
				$data['exchange_status']=$query->exchange_status;
				$this->load->view("front/exchange",$data);				
			}
			
			function index_old()
			{
				$customer_user_id	=  $this->session->userdata('user_id');
					
				$less_amount_btc=$this->user_model->min_max_exchange("BTC","INR")->less_amount;
				//BTC_INR
				$get              = file_get_contents("https://finance.google.com/finance/converter?a=1&from=BTC&to=INR&meta=ei%3DLIa6WdjlGoyouQT6oJnoDA");
				$get              = explode("<span class=bld>",$get);
				$get              = explode("</span>",$get[1]);
				$converted_amount = preg_replace("/[^0-9.]/", null, $get[0]);
				$val=$converted_amount * 1;
				$vall=$val - $less_amount_btc;
				$data['BTC_INR']=number_format($vall,2, '.','');
				//	print_R($data); exit;
				//BTC_AED
				$get1              = file_get_contents("https://finance.google.com/finance/converter?a=1&from=BTC&to=AED&meta=ei%3Dz4a6WaGYLJOKuwTijoe4DA");
			
			
				$less_amount_aed=$this->user_model->min_max_exchange("BTC","AED")->less_amount;
				$get1              = explode("<span class=bld>",$get1);
				$get1              = explode("</span>",$get1[1]);
				$converted_amount1 = preg_replace("/[^0-9.]/", null, $get1[0]);
				$val1=$converted_amount1 * 1;
				$vall1=$val1 - $less_amount_aed;
				$data['BTC_AED']=number_format($vall1,2, '.','');
				//ETH_INR
				$less_amount_eth = $this->user_model->min_max_exchange("ETH","INR")->less_amount;
				$eth_less=55785.88;
				$valll=$eth_less - $less_amount_eth;
				$data['ETH_INR']=number_format($valll,2, '.','');
				//ETH_AED
				//$get_eth_aed=file_get_contents("https://fx-rate.net/ca.php?currency_pair=ETHAED");
				$less_amount_ethaed = $this->user_model->min_max_exchange("ETH","AED")->less_amount;
				$bal=998.85;
				$valo=$bal - $less_amount_ethaed;
				$data['ETH_AED']=number_format($valo,2, '.','');
				$data['fee_BTC_INR']=$this->user_model->exchange_fee("BTC_INR");
				$data['fee_BTC_AED']=$this->user_model->exchange_fee("BTC_AED");
				$data['fee_ETH_INR']=$this->user_model->exchange_fee("ETH_INR");
				$data['fee_ETH_AED']=$this->user_model->exchange_fee("ETH_AED");
				$data['fee_INR_BTC']=$this->user_model->exchange_fee("INR_BTC");
				$data['fee_INR_ETH']=$this->user_model->exchange_fee("INR_ETH");
				$data['fee_AED_ETH']=$this->user_model->exchange_fee("AED_ETH");
				$data['fee_AED_BTC']=$this->user_model->exchange_fee("AED_BTC");
			
				//balances
				$data['btc_bal']=$this->user_model->fetchuserbalancebyId($customer_user_id,'BTC');
				$data['eth_bal']=$this->user_model->fetchuserbalancebyId($customer_user_id,'ETH');
				$data['inr_bal']=$this->user_model->fetchuserbalancebyId($customer_user_id,'INR');
				$data['aed_bal']=$this->user_model->fetchuserbalancebyId($customer_user_id,'AED');
				$query = $this->user_model->get_data('giZfInSoOcZeItSiOs','','','','','','row');
				$data['exchange_status']=$query->exchange_status;
				$this->load->view("front/exchange",$data);
			}
			
			function refreshexchange()
			{
				$ex_history = $this->user_model->get_exchange();
				if(empty($ex_history))
				$ex_history = array();
				$return  = array('ex_history'=>$ex_history);
				die(json_encode($return));
			}
			function exchange_submit()
			{
				echo $this->user_model->exchange_submit();
			}
			function close_exchange_order($id)
			{
				$this->db->where("order_id",$id);
				$data=$this->db->get("redroZeIgSnOaZhIcSxOe");
				$datas=$data->row();
				$amount= $datas->amount;
				$amount1= $datas->askprice;
				$cur= $datas->f_currency;
				$user_id=$datas->DiZrIeSsOu;
				if($datas->status == "pending")
				{
				
				$this->db->where("DiZrIeSsOu",$user_id);
				$blnc=$this->db->get('ecnZaIlSaObZrIeSsOuZnIiSoOc');
				$user_blnc=$blnc->row();
				$o_blnce=$user_blnc->$cur;	
				$this->db->where('order_id',$id);
				$this->db->update('redroZeIgSnOaZhIcSxOe',array('status'=>'cancelled'));
				$n_balance=$amount + $o_blnce;

				$this->db->where("DiZrIeSsOu",$user_id);
				$this->db->update('ecnZaIlSaObZrIeSsOuZnIiSoOc',array($cur=>$n_balance));
				echo "true";
				}
				else 
				{
					echo "error";
				}
			}
		}
