<?php
if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Bitunio extends CI_Controller {
	public function __construct() {
		parent::__construct();
		error_reporting(E_ERROR);
		$this->load->database();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		header('X-Frame-Options: SAMEORIGIN');
		header('X-XSS-Protection: 1; mode=block');
		header('X-Content-Type-Options: nosniff');
		$base = base_url();
		header("ALLOW-FROM: $base");
		header("X-Powered-By: $base");
		
		//By Mujeeb
		/*$this->output->set_header('Strict-Transport-Security: max-age=19768000; includeSubDomains; preload');		
		header("Referrer-Policy: no-referrer");
		header("Set-Cookie: Secure;Secure");	
		
		
		$contentSecurityPolicy = "Content-Security-Policy:".
				"connect-src * ;". // XMLHttpRequest (AJAX request), WebSocket or EventSource.
				"default-src * 'unsafe-eval' 'unsafe-inline';". // Default policy for loading html elements
				"frame-ancestors 'self' ;". //allow parent framing - this one blocks click jacking and ui redress
				"frame-src *;". // vaid sources for frames
				"media-src * *.bitunio.com;". // vaid sources for media (audio and video html tags src)
				"object-src 'none'; ". // valid object embed and applet tags src
				"font-src *; ". // valid object embed and applet tags src
				"form-action *;" . //Valid Source for Form Submit		
				"base-uri 'none';" .				
				"report-uri https://www.bitunio.com/;". //A URL that will get raw json data in post that lets you know what was violated and blocked
				"script-src 'unsafe-inline' http: https: data: https://bitunio.com/ https://*.bitunio.com  https://code.jquery.com/ https://www.google.com https://www.gstatic.com/;" .
				//"script-src 'self' 'unsafe-inline' 'unsafe-eval' data: https://code.jquery.com/;". // allows js from self, jquery and google analytics.  Inline allows inline js
				"style-src https: http: 'unsafe-inline';";// allows css from self and inline allows inline css
		header($contentSecurityPolicy);*/
		
		//End of Mujeeb
		

		ini_set('session.gc_maxlifetime', 300);
		ini_set('session.cookie_httponly', 1);
		ini_set('session.use_only_cookies', 1);
		ini_set('session.cookie_secure', 1);

		$this->username = username();

	}
	function index() {

		$refer_id = $this->uri->segment(3);
		$is_refer = $this->user_model->is_refer($refer_id);
		if ($is_refer) {
			$refer_id_check = $refer_id;
		} else {
			$refer_id_check = "";
		}

		$data['BTC_INR'] = $this->user_model->get_trade_summary('BTC', 'INR');
		$data['BTC_AED'] = $this->user_model->get_trade_summary('BTC', 'AED');
		$data['ETH_INR'] = $this->user_model->get_trade_summary('ETH', 'INR');
		$data['ETH_AED'] = $this->user_model->get_trade_summary('ETH', 'AED');
		$data['ETH_BTC'] = $this->user_model->get_trade_summary('ETH', 'BTC');
		$data['XRP_BTC'] = $this->user_model->get_trade_summary('XRP', 'BTC');
		$data['XMR_BTC'] = $this->user_model->get_trade_summary('XMR', 'BTC');
		$data['ETC_BTC'] = $this->user_model->get_trade_summary('ETC', 'BTC');
		$data['ZEC_BTC'] = $this->user_model->get_trade_summary('ZEC', 'BTC');
		$data['LTC_BTC'] = $this->user_model->get_trade_summary('LTC', 'BTC');
		$data['DOGE_BTC'] = $this->user_model->get_trade_summary('DOGE', 'BTC');
		$data['DASH_BTC'] = $this->user_model->get_trade_summary('DASH', 'BTC');
		$data['BCH_BTC'] = $this->user_model->get_trade_summary('BCH', 'BTC');

		$data['users_count'] = $this->user_model->user_count();
		$data['trans_count'] = $this->user_model->trans_count();

		$data['exchange'] = $this->user_model->get_cms(6);
		$data['order'] = $this->user_model->get_cms(7);
		$data['custom'] = $this->user_model->get_cms(8);
		$data['security'] = $this->user_model->get_cms(9);
		$data['multi'] = $this->user_model->get_cms(10);
		$data['bitunio_key'] = $this->user_model->get_cms(11);
		$data['sec_key'] = $this->user_model->get_cms(12);
		$data['rec_key'] = $this->user_model->get_cms(13);
		$data['ref_id'] = $refer_id_check;
		
		$data['refralstatus']  = $this->user_model->get_refral_status_admin('active'); //Get Refral Status of Admin
		//$this->load->view("front/index", $data);
		//$this->load->view("front/coming/coming", $data);
		$this->load->view("v2/index", $data);
	}//End of Functio
	
	function dashboard() {
		if (!log_in()) {
			redirect('');
		}
	
		$id = user_id();
		$data['details'] = $this->user_model->userdetails($id);
		$data['histry'] = array_reverse($this->user_model->yroZtIsSiOh1($id));
	
		$data['allcurrency'] = $this->user_model->allcurrency();
	
		$data['balances'] = $this->user_model->get_balances();
		$data['income_his'] = $this->user_model->get_income($id);
		$query = $this->user_model->get_data('giZfInSoOcZeItSiOs', '', '', '', '', '', 'row');
		$data['trade_status'] = $query->trade_status;
		$data['exchange_status'] = $query->exchange_status;
		$data['type'] = user_type();
		
		$data['refralstatus']  = $this->user_model->get_refral_status_admin('active'); //Get Refral Status of Admin
		
		$this->load->view('front/dashboard', $data);
		//$this->load->view('v2/trade', $data);
	}//End of Function
	
	function register1_old() {

		$type = $this->input->post('type');
		if ($type == "individual") {
			$check = $this->input->post('check');
			$check1 = 1;
		} else {
			$check = $this->input->post('check');
			$check1 = $this->input->post('check1');
		}

		if ($check != "" && $check1 != "") {
			$qry = $this->user_model->email_exist();
			if ($qry) {
				echo "email";
			} else {
				$this->user_model->register();
				echo "success";
			}

		} else {
			echo "agree";
		}
	}//End of Register1
	
	function register1() {
	
		$gcheck = $this->input->post('reg_recaptcha');
		
		/*$type = $this->input->post('type');
		if ($type == "individual") {
			$check = $this->input->post('check');
			$check1 = 1;
		} else {
			$check = $this->input->post('check');
			$check1 = $this->input->post('check1');
		}*/
	
		//if ($check != "" && $check1 != "") {
		if ($gcheck != "") {
			$qry = $this->user_model->email_exist();
			if ($qry) {
				echo "email";
			} else {
				$this->user_model->register();
				echo "success";
			}
	
		} 
		else 
		{
			echo "agree";
		}
	}//End of Register1
	
	function register_refral() {
	
		$gcheck = $this->input->post('reg_recaptcha');
	
		//if ($check != "" && $check1 != "") {
		if ($gcheck != "") {
			$qry = $this->user_model->email_exist();
			if ($qry) {
				echo "email";
			} else {
				$this->user_model->register_refral();
				echo "success";
			}
	
		}
		else
		{
			echo "agree";
		}
	}//End of Register1

	function reg_confirm($id) {

		$stat = $this->db->query("select * from SliIaStOeZdIrSeOsu where encrypt_id='$id'")->row();

		if ($stat->email_check == "no") {
			$data['verify_status'] = 'no';
			$qry = $this->user_model->reg_confirm($id);
		} else {
			$data['verify_status'] = 'yes';
		}

		//print_r($data); exit;
		$this->load->view("front/index", $data);
	}
	
	function newipverify($recid) {
		// if(log_in())
		// redirect('profile');
		$ip = encrypt_decrypt("2", $recid);
		
	
		$rec = array(
				"Status" => 1,				
		);
		
		//$this->db->where("ipAddress", $ip);
		$this->db->where("recid", $recid);
		$this->db->update("yroZtIsSiOh", $rec);
							
		$data['status_reset'] = 'verified_ip';				
		$this->load->view('v2/login', $data);//New by Mujeeb
			
		
	}//End of Function
	
	function disable($id)
	{
		$user_id = encrypt_decrypt("2", $id);
		
		$updatedata = array(				
				'status' => 'deactive',
		);
		//$this->db->where("DiZrIeSsOu", $user_id);
		$this->db->where("DiZrIeSsOu", $user_id);
		$this->db->update("SliIaStOeZdIrSeOsu", $updatedata);	
		
		//echo "Mujeeb : " . $user_id;
		$this->user_model->sendMailAccountDisabledSuccess($user_id);
		
		$data['status_reset'] = 'deactive';
		$this->load->view('v2/login', $data);//New by Mujeeb
	}//End of Function
	
	function login() 
	{
		$logged = $this->user_model->login();
		
		if ($logged) 
		{
			echo $logged;
		} 
		else 
		{
			echo "error";
		}
	}
	
	function NewLogin()
	{
		$this->load->view("v2/login", "");
	}//End of Function
	
	function TransactionFee()
	{
		$this->load->view("v2/transactionfee");
	}//End of Function
	
	
	function verificationstatus()
	{
		$status = $this->user_model->get_verification_status();
		echo $status;
	}//End of Function
	
	function AccountStatus()
	{
		$status = $this->user_model->get_account_status();
		echo $status;
	}//End of Function

	function forgotpasswords() {

		if ($this->security->xss_clean($this->input->post('email'))) {
			$qry = $this->user_model->forgotpasswords();
			if ($qry) {
				echo "success";
			} else {
				echo "error";
			}
		}

	}//End of Function

	function forgot1($id) {
		// if(log_in())
		// redirect('profile');
		$id = encrypt_decrypt("2", $id);
		//echo $id; exit;

		$qry = $this->user_model->forgot_check($id);
		$data['data'] = $this->user_model->forgot_check($id);
		$data['em'] = encrypt_decrypt("2", $this->db->query("select txid from tneZtInSoOc where 	DiZrIeSsOu='$id'")->row()->txid);
		$date1 = $qry->forget_date;
		if ($date1 != "") {
			$cur_date = date('Y-m-d H:i:s');
			$dispute_time = $date1;
			$dispute_time = date('Y-m-d H:i:s', strtotime($dispute_time . ' + 1440 minutes'));

			//if($cur_date > $dispute_time) right
			if ($cur_date > $dispute_time) {
				$data['status_reset'] = 'expire';
				//$this->load->view('front/index', $data);//Old
				$this->load->view('v2/login', $data);//New by Mujeeb
			} else {
				$data['status_reset'] = 'present';
				//$this->load->view('front/index', $data);//Old
				$this->load->view('v2/login', $data);//New by Mujeeb
			}

		} else {
			$data['status_reset'] = 'expire';
			//$this->load->view('front/index', $data);//Old
			$this->load->view('v2/login', $data);//New by Mujeeb
		}
	}

	function reset_password() {

		$qry = $this->user_model->reset_pass();
		if ($qry) {
			echo "success";
		} else {
			echo "fail";
		}

	}

	function logout() {
		$this->session->sess_destroy();
		redirect('');
	}
	function notfound() {
		$this->load->view('front/fnf');
	}
	function chart_data($pair1, $pair2) {

		$interval = 1440;
		$duration = '1m';
		$chartdatas = $this->user_model->get_chartdata($pair1, $pair2, $interval, $duration);
		die(json_encode($chartdatas));
	}
	
	//Function by mujeeb
	function chart_data_new($pair1, $pair2) {
	
		$interval = 1440;
		$duration = '1m';
		
		$url = "https://min-api.cryptocompare.com/data/histoday?fsym=".$pair1."&tsym=".$pair2."&limit=50&aggregate=5&e=CCCAGG";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		$chartdatas = curl_exec($ch);
		curl_close($ch);
		header('Content-type: application/json');
		//$chartdatas = $this->user_model->get_chartdata($pair1, $pair2, $interval, $duration);
		echo $chartdatas;
		//die(json_decode($chartdatas));
	}//End of Function
	
	function get_email() {
		if (!log_in()) {
			redirect('');
		}

		$this->user_model->get_email();
	}
	

	function bank_verification() {
		if (!log_in()) {
			redirect('');
		}

		echo $this->user_model->bank_verification();
	}

	function bank_verify() {
		if (!log_in()) {
			redirect('');
		}

		$data['bank_verification'] = $this->user_model->get_bank();
		$this->load->view('front/bank_verify', $data);
	}
	function subscribe() {

		echo $this->user_model->subscribe();
	}
	
	function index1() {
	
		$data['pages'] = $this->user_model->get_cms(2);
		//$this->load->view('front/index', $data);
		$this->load->view('v2/index', $data);
	}
	
	//By Mujeeb
	function testpage()
	{
		echo $this->user_model->test_page();
		
	}//End of Function
	
	function about() {

		$data['pages'] = $this->user_model->get_cms(2);
		$this->load->view('front/about', $data);
	}
	function terms() {

		$data['pages'] = $this->user_model->get_cms(3);
		$this->load->view('front/terms', $data);
	}
	function broker_terms() {

		$data['pages'] = $this->user_model->get_cms(5);
		$this->load->view('front/terms', $data);
	}
	function blogs($cat_id = '') {
		$data['blogs'] = array_reverse($this->user_model->get_blogs($cat_id));
		$data['blog_cat'] = $this->user_model->blog_cat('active');
		$this->load->view('front/blogs', $data);
	}
	function blog_details($blog_id) {

		$data['blog'] = $this->user_model->get_blog($blog_id);
		$data['comments'] = $this->user_model->get_comments($blog_id);
		$data['comments_count'] = $this->user_model->get_comments_count($blog_id);
		$data['comments_lim'] = $this->user_model->get_comments($blog_id, "admin");
		$data['blog_cat'] = $this->user_model->blog_cat('active');
		$this->load->view('front/blog_details', $data);
	}
	function safe_secure() {

		$data['content'] = $this->user_model->get_cms(18);
		$this->load->view('front/safe_secure', $data);
	}
	function testimonials() {

		$data['testi'] = $this->user_model->get_testi();
		$this->load->view('front/testimonials', $data);
	}
	function fees() {

		$data['trade_fee'] = $this->db->get("egatnZeIcSrOeZpIeSeOf")->result();
		// echo "<pre>";
		// print_r($data); exit;
		$data['fees'] = $this->user_model->get_cms(17);
		$this->load->view('front/fees', $data);
	}
	function faq() {

		$data['faq'] = $this->user_model->faq();
		$this->load->view('front/faq', $data);
	}
	function contact() {

		$data['contact'] = $this->user_model->get_cms(14);
		$data['site'] = $this->db->get('giZfInSoOcZeItSiOs')->row();
		$this->load->view('front/contact', $data);
	}
	function refer() {

		if (!log_in()) {
			redirect('');
		}

		$customer_user_id = $this->session->userdata('user_id');
		$where = "DiZrIeSsOu='" . $customer_user_id . "'";
		$userdetails = $this->user_model->get_data('SliIaStOeZdIrSeOsu', $where, '', '', '', '', 'row');
		$data['profile'] = $userdetails;
		$data['type'] = user_type();
		$referral_code = $userdetails->refer_id;
		if ($referral_code != "") {

			$data['ref_friends'] = $this->db->query("select * from SliIaStOeZdIrSeOsu where refered_id='$referral_code'")->result();
		} else {
			$data['ref_friends'] = "";
		}
		$data['cms'] = $this->user_model->get_cms(20);
		$this->load->view('front/refer', $data);
	}

	function referFriend() {
		$postdata = $this->input->post();
		$email = $postdata['email'];
		$result = $this->user_model->email_exist();
		if ($result) {
			echo "email";
		} else {
			$customer_user_id = $this->session->userdata('user_id');
			$where = "DiZrIeSsOu='" . $customer_user_id . "'";
			$userdeails = $this->user_model->get_data('SliIaStOeZdIrSeOsu', $where, '', '', '', '', 'row');

			$get_email_info = $this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='8'")->row();
			$msg = $get_email_info->message;
			$link = base_url() . "bitunio/index/" . $userdeails->refer_id . '/' . $userdeails->emanZrIeSsOu;

			$msg = str_replace("##FRIENDNAME##", $postdata['friendname'], $msg);
			$msg = str_replace("##MESSAGE##", $postdata['refermessage'], $msg);
			$msg = str_replace("##USERNAME##", $userdeails->emanZrIeSsOu, $msg);
			$msg = str_replace("##LINK##", $link, $msg);
			$msg = str_replace("##COMPANYNAME##", '<b>Bitunio</b>', $msg);

			$this->user_model->mailsettings();
			$this->email->from(admin_email());
			$this->email->to($email);
			$this->email->subject("Refer Friend");
			$this->email->message($msg);
			$this->email->send();
			die('success');
		}
	}

	function blog_comments() {
		echo $this->user_model->blog_comments_ins();
	}

	function tfa_confirm() {

		$user_email1 = $this->security->xss_clean($this->input->post('email'));
		$clientid = $this->security->xss_clean($this->input->post('clientid'));

		if ($user_email1 != "") {
			$user_email = $user_email1;
		} else {
			$id = $this->db->query("select DiZrIeSsOu from SliIaStOeZdIrSeOsu where client_id='$clientid'")->row()->DiZrIeSsOu;
			$user_email = $this->user_model->get_email_admin($id);
		}

		$em = substr($user_email, 0, 4);
		$ail = substr($user_email, 4);

		$em1 = encrypt_decrypt('1', $em);

		$quer = $this->db->query("SELECT SliIaStOeZdIrSeOsu.* FROM SliIaStOeZdIrSeOsu INNER JOIN tneZtInSoOc ON tneZtInSoOc.DiZrIeSsOu=SliIaStOeZdIrSeOsu.DiZrIeSsOu where SliIaStOeZdIrSeOsu.dilZiIaSmOe='$ail' AND tneZtInSoOc.txid='$em1'");

		require_once 'GoogleAuthenticator.php';

		$ga = new PHPGangsta_GoogleAuthenticator();

		$query1 = $quer->row();
		$secret_code = $query1->tfa_code;

		$onecode = $this->security->xss_clean($this->input->post('onecode'));

		$code = $ga->verifyCode($secret_code, $onecode, $discrepancy = 1);
		if ($code == 1) {
			$login_date = date('Y-m-d');
			$login_time = date("h:i:s");
			$datetime = $login_date . " " . $login_time;
			$login_data['user_id'] = $query1->DiZrIeSsOu;
			$this->session->set_userdata($login_data);
			/*$user_ip = $this->input->ip_address();
			$this->load->library('user_agent');
			$user_browser = $this->agent->agent_string();

			//$user_browser	=	$_SERVER['HTTP_USER_AGENT'];
			$historydata = array(
				'DiZrIeSsOu' => $query1->DiZrIeSsOu,
				'ipAddress' => $user_ip,
				'Browser' => $user_browser,
				'Action' => "Logged in",
				'datetime' => $datetime,
			);
			$this->db->insert('yroZtIsSiOh', $historydata);*/
			$date = date("d-m-Y H:i:s");
			$id = $this->session->userdata("user_id");
			$data = array("last_login" => $date);
			$this->db->where("DiZrIeSsOu", $id);
			$this->db->update("SliIaStOeZdIrSeOsu", $data);
			echo "success";
		} else {
			echo "false";
		}
	}

	function tfa_confirm1() {

		require_once 'GoogleAuthenticator.php';
		$ga = new PHPGangsta_GoogleAuthenticator();
		$id = user_id();
		$query1 = $this->user_model->userdetails($id);
		$secret_code = $query1->tfa_code;

		$onecode = $this->security->xss_clean($this->input->post('onecode'));

		$code = $ga->verifyCode($secret_code, $onecode, $discrepancy = 1);
		if ($code == 1) {
			$amount = $this->input->post('amount');
			$currency = $this->input->post('currency');
			$customer_user_id = $this->session->userdata('user_id');
			if (($customer_user_id == "") || $amount == 0 || $amount == "") {
				echo "login";
			} else {
				$balance = $this->user_model->fetchuserbalancebyId($customer_user_id, $currency);
				if ($amount <= $balance) {
					echo $result = $this->user_model->withdrawcoinrequestmodel();
				} else {
					echo "balance";
				}
			}
		} else {
			echo "false";
		}
	}

	function tfa_confirm2() {

		require_once 'GoogleAuthenticator.php';
		$ga = new PHPGangsta_GoogleAuthenticator();
		$id = user_id();
		$query1 = $this->user_model->userdetails($id);
		$secret_code = $query1->tfa_code;

		$onecode = $this->security->xss_clean($this->input->post('onecode'));

		$code = $ga->verifyCode($secret_code, $onecode, $discrepancy = 1);

		if ($code == 1) {
			$amount = $this->input->post('amount');
			$currency = $this->input->post('wcur');
			$customer_user_id = $this->session->userdata('user_id');
			if (($customer_user_id == "") || $amount == 0 || $amount == "") {
				echo "login";
			} else {

				$balance = $this->user_model->fetchuserbalancebyId($customer_user_id, $currency);
				if ($amount <= $balance) {
					echo $this->user_model->international_withdraw_req();
				} else {
					echo "balance";
				}
			}
		} else {
			echo "false";
		}
	}
	function transactions() {

		if (!log_in()) {
			redirect('');
		}
		
		$data['refralstatus']  = $this->user_model->get_refral_status_admin('active'); //Get Refral Status of Admin
		$data['transactions'] = $this->user_model->transactions();
		$this->load->view("front/transactions", $data);
	}
	
	//By Mujeeb Load default Refral Page
	function referral()
	{
		if (!log_in()) {
			redirect('');
		}
		
		$user_id = user_id();
		
		//$data['transactions'] = $this->user_model->transactions();
		$data['transactions'] = $this->user_model->refral_transactions($user_id);
		$totalusers = $this->user_model->get_total_refral($user_id); //Total User's
		$totalverifyuser = $this->user_model->get_total_refral_with_verified($user_id, 1);//Total Verified Users
		$totalunverifyuser = $this->user_model->get_total_refral_with_verified($user_id, 0);//Total UnVerified Users
		
		$getrefralamtadmin = $this->user_model->get_refral_amount_admin('active');//Get Refral Amount From Admin
		$getrefralnoteadmin = $this->user_model->get_refral_note_admin('active');//Get Refral note From Admin
		
		$data['refralstatus']  = $this->user_model->get_refral_status_admin('active'); //Get Refral Status of Admin
		$data['refralamt']     = $getrefralamtadmin; //Refral Amount from Admin
		$data['refralnote']    = $getrefralnoteadmin; //Refral Note from Admin
		$data['total_earned']  = ($totalverifyuser * $getrefralamtadmin);
		$data['pending_total'] = ($totalunverifyuser * $getrefralamtadmin);
		$data['total_users']   = $totalusers;
		//$data['refral_link'] = $id;
		$data['refralid']      = $this->user_model->refral_encryptdecrypt(1, $user_id); //Encrypt ID
		$this->load->view("front/refral", $data);
	}//End of Function
	
	//By Mujeeb Register for Referal Program
	function register($id)
	{
		$data['refral_link'] = $id;
		$this->load->view("v2/register", $data);
	}//End of Function
	
	

	function bro_status() {

		//echo "sdksdsd"; exit; id_number

		$currentDate = date('Y-m-d');
		$user_id = user_id();

		if ($user_id == "") {
			die('Your session expired. Please login again.');
		} else {

			$updatedata = array(
				'bro_status' => 'pending',
				'bro_date' => date("Y-m-d H:i:s"),
			);
			//print_r($updatedata); exit;

			$this->db->where("DiZrIeSsOu", $user_id);
			$result = $this->db->update('SliIaStOeZdIrSeOsu', $updatedata);

			if ($result) {
				die('success');
			} else {
				die('Oops! Your information could not saved');
			}
		}
	}

	function enquiry() {

		$mdate = date('Y-m-d');
		$status = "no-reply";
		$data = array('username' => $this->input->post('contact_name'), 'email_id' => $this->input->post('contact_email'), 'message' => $this->input->post('contact_message'), 'created_date' => $mdate, 'status' => $status, 'subject' => $this->input->post('subject'));
		$res = $this->db->insert('sZuItSrOoZpIpSuOs', $data);
		if ($res) {
			echo "success";
		} else {
			echo "error";
		}
	}

	function trade_his() {

		$this->user_model->fetchHistory('BTC', 'INR', true);
	}
	
	function getHomePagePrice()
	{
		$rec = $this->user_model->getHomePagePrice();
		$return  = array('record'=>$rec);
		die(json_encode($return));
		//echo $this->user_model->getHomePagePrice();
	}
	
	function cron_api_price() {
		
		//BTC_INR
		$get = file_get_contents("https://finance.google.com/finance/converter?a=1&from=BTC&to=INR");
		$get = explode("<span class=bld>", $get);
		$get = explode("</span>", $get[1]);
		$converted_amount = preg_replace("/[^0-9.]/", null, $get[0]);
		$val = $converted_amount * 1;
		$BTC_INR = number_format($val, 2, '.', '');

		$this->user_model->update_api_price('BTC', 'INR', $BTC_INR);

		//BTC_AED
		$get1 = file_get_contents("https://finance.google.com/finance/converter?a=1&from=BTC&to=AED");
		$get1 = explode("<span class=bld>", $get1);
		$get1 = explode("</span>", $get1[1]);
		$converted_amount1 = preg_replace("/[^0-9.]/", null, $get1[0]);
		$val1 = $converted_amount1 * 1;
		$BTC_AED = number_format($val1, 2, '.', '');

		$this->user_model->update_api_price('BTC', 'AED', $BTC_AED);

		//ETH_BTC
		$get2 = file_get_contents("https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=BTC");
		$res = json_decode($get2, 0);
		$ETH_BTC = ($res[0]->price_btc);
		$this->user_model->update_api_price('ETH', 'BTC', $ETH_BTC);

		//XRP_BTC
		$get3 = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Ripple/?convert=BTC");
		$res = json_decode($get3, 0);
		$XRP_BTC = ($res[0]->price_btc);
		$this->user_model->update_api_price('XRP', 'BTC', $XRP_BTC);

		//ETC_BTC
		$get4 = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Ethereum-Classic/?convert=BTC");
		$res = json_decode($get4, 0);
		$ETC_BTC = ($res[0]->price_btc);
		$this->user_model->update_api_price('ETC', 'BTC', $ETC_BTC);

		//ZEC_BTC
		$get5 = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Zcash/?convert=BTC");
		$res = json_decode($get5, 0);
		$ZEC_BTC = ($res[0]->price_btc);
		$this->user_model->update_api_price('ZEC', 'BTC', $ZEC_BTC);

		//MAID_BTC
		$get6 = file_get_contents("https://api.coinmarketcap.com/v1/ticker/MaidSafeCoin/?convert=BTC");
		$res = json_decode($get6, 0);
		$MAID_BTC = ($res[0]->price_btc);
		$this->user_model->update_api_price('MAID', 'BTC', $MAID_BTC);

		//DOGE_BTC
		$get7 = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Dogecoin/?convert=BTC");
		$res = json_decode($get7, 0);
		$DOGE_BTC = ($res[0]->price_btc);
		$this->user_model->update_api_price('DOGE', 'BTC', $DOGE_BTC);

		//DASH_BTC
		$get8 = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Dash/?convert=BTC");
		$res = json_decode($get8, 0);
		$DASH_BTC = ($res[0]->price_btc);

		$this->user_model->update_api_price('DASH', 'BTC', $DASH_BTC);

		// //NEO_BTC
		$get9 = file_get_contents("https://api.coinmarketcap.com/v1/ticker/neo/?convert=BTC");
		$res = json_decode($get9, 0);
		$NEO_BTC = ($res[0]->price_btc);

		$this->user_model->update_api_price('NEO', 'BTC', $NEO_BTC);

		$get10 = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Monero/?convert=BTC");
		$res = json_decode($get10, 0);
		$XRM_BTC = ($res[0]->price_btc);

		$this->user_model->update_api_price('XRM', 'BTC', $XRM_BTC);
	}//End of Function
	
	
	/* Integrated by Mujeeb */
	
	function exchnage_market_price()
	{
		$pair = $this->input->post('pair');
		$price = $this->input->post('price');
		
		$this->user_model->update_api_exchange_price($pair, $price);
		echo "Saved : ". $pair , " : " . $price . " Success";
	}//End of Function
	
	function new_market_price()
	{
		$fc = $this->input->post('fc');
		$sc = $this->input->post('sc');
		$price = $this->input->post('price');
		$lp = $this->input->post('lp');
		$dc = $this->input->post('dc');
		$dp = $this->input->post('dp');
		$op = $this->input->post('op');
		$dt = $this->input->post('ltime');		
		$range = $this->input->post('range');
		
		
		$this->user_model->update_api_price_new($fc, $sc, $price, $lp, $dc, $dp, $range, $op, $dt);
		echo "Saved : ". $fc , " : " . $price . " Success";
		//echo "mujeeb";
	}//End of Function
	
	function cron_job_new1()//Altered cron_job_new
	{
		$url = "https://min-api.cryptocompare.com/data/pricemultifull?fsyms=ETH,XRP,XMR,ETC,LTC,DASH,BCH,ZEC&tsyms=BTC&extraParams=Bitunio.io";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		$result = curl_exec($ch);
		curl_close($ch);		
		$array = json_decode($result,true);
		
		//echo $result;
		//return;
		date_default_timezone_set("Asia/Kolkata");
		//$timestamp=1542439310;
		//echo date('Y-m-d H:i:s a', $timestamp);
		
		$ETHBTC = $array['RAW']['ETH']['BTC']['PRICE'];
		$ETHLASTPRICE = $array['RAW']['ETH']['BTC']['PRICE']; //Last Price
		$ETHDAILYCHANGE = $array['RAW']['ETH']['BTC']['CHANGEDAY']; //Daily Change
		$ETHDAILYCHANGEPCT = $array['RAW']['ETH']['BTC']['CHANGEPCTDAY']; //Daily Change Percent
		//$ETH24HRSRANGE = $array['RAW']['ETH']['BTC']['LOWDAY'] . "-" . $array['RAW']['ETH']['BTC']['HIGHDAY']; //24hrs Range
		$r1 = sprintf('%.6f', $array['RAW']['ETH']['BTC']['LOWDAY']);
		$r2 = sprintf('%.6f', $array['RAW']['ETH']['BTC']['HIGHDAY']);
		$ETH24HRSRANGE = $r1 . " - " . $r2; //Daily Change
		
		$ETHOPENPRICE = $array['RAW']['ETH']['BTC']['OPENDAY']; //OPEN PRICE
		$ETHLASTUPDATE = $array['RAW']['ETH']['BTC']['LASTUPDATE']; //Last Updated Time
		$ltime = date('Y-m-d H:i:s a', $ETHLASTUPDATE); //Last Updated Time
		
		$eth = sprintf('%.6f', $ETHBTC); //Current Price
		$lp = $eth; //Last Price
		$dc = sprintf('%.6f', $ETHDAILYCHANGE); //Daily Change
		$dp = sprintf('%.6f', $ETHDAILYCHANGEPCT); //Daily PCT Chnage
		$op = sprintf('%.6f', $ETHOPENPRICE); //Open Price
		$this->user_model->update_api_price_new('ETH', 'BTC', $eth,$lp, $dc, $dp, $ETH24HRSRANGE, $op, $ltime );
		echo "ETH : Success : " . $eth ."</br>";		
		//End of ETH
		
		//Start XRP
		$XRPBTC = $array['RAW']['XRP']['BTC']['PRICE'];
		$XRPLASTPRICE = $array['RAW']['XRP']['BTC']['PRICE']; //Last Price
		$XRPDAILYCHANGE = $array['RAW']['XRP']['BTC']['CHANGEDAY']; //Daily Change
		$XRPDAILYCHANGEPCT = $array['RAW']['XRP']['BTC']['CHANGEPCTDAY']; //Daily Change Percent
		//$XRP24HRSRANGE = $array['RAW']['XRP']['BTC']['LOWDAY'] . "-" . $array['RAW']['XRP']['BTC']['HIGHDAY']; //24hrs Range
		
		$r1 = sprintf('%.8f', $array['RAW']['XRP']['BTC']['LOWDAY']);
		$r2 = sprintf('%.8f', $array['RAW']['XRP']['BTC']['HIGHDAY']);		
		$XRP24HRSRANGE = $r1 . " - " . $r2; //Daily Change
		$XRPOPENPRICE = $array['RAW']['XRP']['BTC']['OPENDAY']; //OPEN PRICE
		$XRPLASTUPDATE = $array['RAW']['XRP']['BTC']['LASTUPDATE']; //Last Updated Time
		$ltime = date('Y-m-d H:i:s a', $XRPLASTUPDATE); //Last Updated Time
		
		$xrp = sprintf('%.8f', $XRPBTC); //Current Price
		$lp = $xrp; //Last Price
		$dc = sprintf('%.8f', $XRPDAILYCHANGE); //Daily Change
		$dp = sprintf('%.8f', $XRPDAILYCHANGEPCT); //Daily PCT Chnage
		$op = sprintf('%.8f', $XRPOPENPRICE); //Open Price
		$this->user_model->update_api_price_new('XRP', 'BTC', $xrp, $lp, $dc, $dp, $XRP24HRSRANGE, $op, $ltime );
		echo "XRP : Success</br>";
		//End of XRP
		
		//Start XMR
		$XMRBTC = $array['RAW']['XMR']['BTC']['PRICE'];
		$XMRLASTPRICE = $array['RAW']['XMR']['BTC']['PRICE']; //Last Price
		$XMRDAILYCHANGE = $array['RAW']['XMR']['BTC']['CHANGEDAY']; //Daily Change
		$XMRDAILYCHANGEPCT = $array['RAW']['XMR']['BTC']['CHANGEPCTDAY']; //Daily Change Percent
		//$XMR24HRSRANGE = $array['RAW']['XMR']['BTC']['LOWDAY'] . "-" . $array['RAW']['XMR']['BTC']['HIGHDAY']; //24hrs Range
		$r1 = sprintf('%.5f', $array['RAW']['XMR']['BTC']['LOWDAY']);
		$r2 = sprintf('%.5f', $array['RAW']['XMR']['BTC']['HIGHDAY']);
		$XMR24HRSRANGE = $r1 . " - " . $r2; //Daily Change
		
		$XMROPENPRICE = $array['RAW']['XMR']['BTC']['OPENDAY']; //OPEN PRICE
		$XMRLASTUPDATE = $array['RAW']['XMR']['BTC']['LASTUPDATE']; //Last Updated Time
		$ltime = date('Y-m-d H:i:s a', $XMRLASTUPDATE); //Last Updated Time
		
		$xmr = sprintf('%.5f', $XMRBTC); //Current Price
		$lp = $xmr; //Last Price
		$dc = sprintf('%.5f', $XMRDAILYCHANGE); //Daily Change
		$dp = sprintf('%.5f', $XMRDAILYCHANGEPCT); //Daily PCT Chnage
		$op = sprintf('%.5f', $XMROPENPRICE); //Open Price
		$this->user_model->update_api_price_new('XMR', 'BTC', $xmr, $lp, $dc, $dp, $XMR24HRSRANGE, $op, $ltime );
		echo "XMR : Success</br>";
		//End of XMR
		
		//Start ETC
		$ETCBTC = $array['RAW']['ETC']['BTC']['PRICE'];
		$ETCLASTPRICE = $array['RAW']['ETC']['BTC']['PRICE']; //Last Price
		$ETCDAILYCHANGE = $array['RAW']['ETC']['BTC']['CHANGEDAY']; //Daily Change
		$ETCDAILYCHANGEPCT = $array['RAW']['ETC']['BTC']['CHANGEPCTDAY']; //Daily Change Percent
		//$ETC24HRSRANGE = $array['RAW']['ETC']['BTC']['LOWDAY'] . "-" . $array['RAW']['ETC']['BTC']['HIGHDAY']; //24hrs Range
		$r1 = sprintf('%.6f', $array['RAW']['ETC']['BTC']['LOWDAY']);
		$r2 = sprintf('%.6f', $array['RAW']['ETC']['BTC']['HIGHDAY']);
		$ETC24HRSRANGE = $r1 . " - " . $r2; //Daily Change
		$ETCOPENPRICE = $array['RAW']['ETC']['BTC']['OPENDAY']; //OPEN PRICE
		$ETCLASTUPDATE = $array['RAW']['ETC']['BTC']['LASTUPDATE']; //Last Updated Time
		$ltime = date('Y-m-d H:i:s a', $ETCLASTUPDATE); //Last Updated Time
		
		$etc = sprintf('%.6f', $ETCBTC); //Current Price
		$lp = $etc; //Last Price
		$dc = sprintf('%.6f', $ETCDAILYCHANGE); //Daily Change
		$dp = sprintf('%.6f', $ETCDAILYCHANGEPCT); //Daily PCT Chnage
		$op = sprintf('%.6f', $ETCOPENPRICE); //Open Price
		$this->user_model->update_api_price_new('ETC', 'BTC', $etc,$lp, $dc, $dp, $ETC24HRSRANGE, $op, $ltime );
		echo "ETC : Success</br>";
		//End of ETC
		
		//Start LTC
		$LTCBTC = $array['RAW']['LTC']['BTC']['PRICE'];
		$LTCLASTPRICE = $array['RAW']['LTC']['BTC']['PRICE']; //Last Price
		$LTCDAILYCHANGE = $array['RAW']['LTC']['BTC']['CHANGEDAY']; //Daily Change
		$LTCDAILYCHANGEPCT = $array['RAW']['LTC']['BTC']['CHANGEPCTDAY']; //Daily Change Percent
		//$LTC24HRSRANGE = $array['RAW']['LTC']['BTC']['LOWDAY'] . "-" . $array['RAW']['LTC']['BTC']['HIGHDAY']; //24hrs Range
		$r1 = sprintf('%.6f', $array['RAW']['LTC']['BTC']['LOWDAY']);
		$r2 = sprintf('%.6f', $array['RAW']['LTC']['BTC']['HIGHDAY']);
		$LTC24HRSRANGE = $r1 . " - " . $r2; //Daily Change
		$LTCOPENPRICE = $array['RAW']['LTC']['BTC']['OPENDAY']; //OPEN PRICE
		$LTCLASTUPDATE = $array['RAW']['LTC']['BTC']['LASTUPDATE']; //Last Updated Time
		$ltime = date('Y-m-d H:i:s a', $LTCLASTUPDATE); //Last Updated Time
		
		$ltc = sprintf('%.6f', $LTCBTC); //Current Price
		$lp = $ltc; //Last Price
		$dc = sprintf('%.6f', $LTCDAILYCHANGE); //Daily Change
		$dp = sprintf('%.6f', $LTCDAILYCHANGEPCT); //Daily PCT Chnage
		$op = sprintf('%.6f', $LTCOPENPRICE); //Open Price
		$this->user_model->update_api_price_new('LTC', 'BTC', $ltc,$lp, $dc, $dp, $LTC24HRSRANGE, $op, $ltime );
		echo "LTC : Success</br>";
		//End of LTC
		
		//Start DASH
		$DASHBTC = $array['RAW']['DASH']['BTC']['PRICE'];
		$DASHLASTPRICE = $array['RAW']['DASH']['BTC']['PRICE']; //Last Price
		$DASHDAILYCHANGE = $array['RAW']['DASH']['BTC']['CHANGEDAY']; //Daily Change
		$DASHDAILYCHANGEPCT = $array['RAW']['DASH']['BTC']['CHANGEPCTDAY']; //Daily Change Percent
		//$DASH24HRSRANGE = $array['RAW']['DASH']['BTC']['LOWDAY'] . "-" . $array['RAW']['DASH']['BTC']['HIGHDAY']; //24hrs Range
		$r1 = sprintf('%.6f', $array['RAW']['DASH']['BTC']['LOWDAY']);
		$r2 = sprintf('%.6f', $array['RAW']['DASH']['BTC']['HIGHDAY']);
		$DASH24HRSRANGE = $r1 . " - " . $r2; //Daily Change
		$DASHOPENPRICE = $array['RAW']['DASH']['BTC']['OPENDAY']; //OPEN PRICE
		$DASHLASTUPDATE = $array['RAW']['DASH']['BTC']['LASTUPDATE']; //Last Updated Time
		$ltime = date('Y-m-d H:i:s a', $DASHLASTUPDATE); //Last Updated Time
		
		$dash = sprintf('%.6f', $DASHBTC); //Current Price
		$lp = $dash; //Last Price
		$dc = sprintf('%.6f', $DASHDAILYCHANGE); //Daily Change
		$dp = sprintf('%.6f', $DASHDAILYCHANGEPCT); //Daily PCT Chnage
		$op = sprintf('%.6f', $DASHOPENPRICE); //Open Price
		$this->user_model->update_api_price_new('DASH', 'BTC', $dash,$lp, $dc, $dp, $DASH24HRSRANGE, $op, $ltime );
		echo "DASH : Success</br>";
		//End of DASH
		
		//Start BCH
		$BCHBTC = $array['RAW']['BCH']['BTC']['PRICE'];
		$BCHLASTPRICE = $array['RAW']['BCH']['BTC']['PRICE']; //Last Price
		$BCHDAILYCHANGE = $array['RAW']['BCH']['BTC']['CHANGEDAY']; //Daily Change
		$BCHDAILYCHANGEPCT = $array['RAW']['BCH']['BTC']['CHANGEPCTDAY']; //Daily Change Percent
		//$BCH24HRSRANGE = $array['RAW']['BCH']['BTC']['LOWDAY'] . "-" . $array['RAW']['BCH']['BTC']['HIGHDAY']; //24hrs Range
		$r1 = sprintf('%.6f', $array['RAW']['BCH']['BTC']['LOWDAY']);
		$r2 = sprintf('%.6f', $array['RAW']['BCH']['BTC']['HIGHDAY']);
		$BCH24HRSRANGE = $r1 . " - " . $r2; //Daily Change
		$BCHOPENPRICE = $array['RAW']['BCH']['BTC']['OPENDAY']; //OPEN PRICE
		$BCHLASTUPDATE = $array['RAW']['BCH']['BTC']['LASTUPDATE']; //Last Updated Time
		$ltime = date('Y-m-d H:i:s a', $BCHLASTUPDATE); //Last Updated Time
		
		$bch = sprintf('%.6f', $BCHBTC); //Current Price
		$lp = $bch; //Last Price
		$dc = sprintf('%.6f', $BCHDAILYCHANGE); //Daily Change
		$dp = sprintf('%.6f', $BCHDAILYCHANGEPCT); //Daily PCT Chnage
		$op = sprintf('%.6f', $BCHOPENPRICE); //Open Price
		$this->user_model->update_api_price_new('BCH', 'BTC', $bch,$lp, $dc, $dp, $BCH24HRSRANGE, $op, $ltime );
		echo "BCH : Success</br>";
		//End of BCH
		
		//Start ZEC
		$ZECBTC = $array['RAW']['ZEC']['BTC']['PRICE'];
		$ZECLASTPRICE = $array['RAW']['ZEC']['BTC']['PRICE']; //Last Price
		$ZECDAILYCHANGE = $array['RAW']['ZEC']['BTC']['CHANGEDAY']; //Daily Change
		$ZECDAILYCHANGEPCT = $array['RAW']['ZEC']['BTC']['CHANGEPCTDAY']; //Daily Change Percent
		//$ZEC24HRSRANGE = $array['RAW']['ZEC']['BTC']['LOWDAY'] . "-" . $array['RAW']['ZEC']['BTC']['HIGHDAY']; //24hrs Range
		$r1 = sprintf('%.6f', $array['RAW']['ZEC']['BTC']['LOWDAY']);
		$r2 = sprintf('%.6f', $array['RAW']['ZEC']['BTC']['HIGHDAY']);
		$ZEC24HRSRANGE = $r1 . " - " . $r2; //Daily Change
		$ZECOPENPRICE = $array['RAW']['ZEC']['BTC']['OPENDAY']; //OPEN PRICE
		$ZECLASTUPDATE = $array['RAW']['ZEC']['BTC']['LASTUPDATE']; //Last Updated Time
		$ltime = date('Y-m-d H:i:s a', $ZECLASTUPDATE); //Last Updated Time
		
		$zec = sprintf('%.6f', $ZECBTC); //Current Price
		$lp = $zec; //Last Price
		$dc = sprintf('%.6f', $ZECDAILYCHANGE); //Daily Change
		$dp = sprintf('%.6f', $ZECDAILYCHANGEPCT); //Daily PCT Chnage
		$op = sprintf('%.6f', $ZECOPENPRICE); //Open Price
		$this->user_model->update_api_price_new('ZEC', 'BTC', $zec, $lp, $dc, $dp, $ZEC24HRSRANGE, $op, $ltime );
		echo "ZEC : Success</br>";
		//End of BCH
		
		
		
		//$XMRBTC = $array['RAW']['XMR']['BTC']['PRICE'];
		//$ETCBTC = $array['RAW']['ETC']['BTC']['PRICE'];
		//$LTCBTC = $array['RAW']['LTC']['BTC']['PRICE'];
		//$DASHBTC = $array['RAW']['DASH']['BTC']['PRICE'];
		//$BCHBTC = $array['RAW']['BCH']['BTC']['PRICE'];
		//$ZECBTC = $array['RAW']['ZEC']['BTC']['PRICE'];
		
		
				
		/*echo sprintf('ETH-BTC : %.8f', $ETHBTC) . " : " . $eth;
		echo "</br>";
		$xrp = sprintf('%.8f', $XRPBTC);
		echo sprintf('XRP-BTC : %.8f', $XRPBTC) . " : " . $xrp;
		echo "</br>";
		$xmr = sprintf('%.5f', $XMRBTC);
		echo sprintf('XMR-BTC : %.5f', $XMRBTC) . " : " . $xmr;
		echo "</br>";
		$etc = sprintf('%.6f', $ETCBTC);
		echo sprintf('ETC-BTC : %.6f', $ETCBTC) . " : " . $etc;
		echo "</br>";
		$ltc = sprintf('%.6f', $LTCBTC);
		echo sprintf('LTC-BTC : %.5f', $LTCBTC) . " : " . $ltc;
		echo "</br>";
		$dash = sprintf('%.6f', $DASHBTC);
		echo sprintf('DASH-BTC : %.6f', $DASHBTC) . " : " . $dash;
		echo "</br>";
		$bch = sprintf('%.6f', $BCHBTC);
		echo sprintf('BCH-BTC : %.6f', $BCHBTC) . " : " . $bch;
		echo "</br>";
		$zec = sprintf('%.6f', $ZECBTC);
		
		
		$this->user_model->update_api_price('XRP', 'BTC', $xrp);
		$this->user_model->update_api_price('XMR', 'BTC', $xmr);
		$this->user_model->update_api_price('ETC', 'BTC', $etc);
		$this->user_model->update_api_price('LTC', 'BTC', $ltc);
		$this->user_model->update_api_price('DASH', 'BTC', $dash);
		$this->user_model->update_api_price('BCH', 'BTC', $bch);
		$this->user_model->update_api_price('ZEC', 'BTC', $zec);*/
		
		echo "Successfully Updated";
		//echo sprintf('ZEC-BTC : %.6f', $ZECBTC) . " : " . $zec;
	}//End of Function cron_job_new

	function cron_offer() {

		$today = date("m/d/Y");
		$expire = $this->user_model->get_data('giZfInSoOcZeItSiOs', '', '', '', '', '', 'row', '', 'offer_expire');
		$today_time = strtotime($today);
		$expire_time = strtotime($expire);

		if ($expire_time < $today_time) {
			echo "expired";
		} else {
			//echo "not expire";
			//exit;
			$status = 'active';
			$where = "status='" . $status . "'";
			$userdetails = $this->user_model->get_data('SliIaStOeZdIrSeOsu', $where, '', '', '', '', 'result');
			foreach ($userdetails as $userdetails1) {
				$email = $this->user_model->get_email_admin($userdetails1->DiZrIeSsOu);

				$get_email_info = $this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='15'")->row();
				$msg = $get_email_info->message;
				$subject = $get_email_info->subject;
				$this->user_model->mailsettings();
				$this->email->from(admin_email());
				$this->email->to($email);
				$this->email->subject($subject);
				$this->email->message($msg);
				$this->email->send();
			}
		}
	}

	function cronupdateReceivedamount() // cronjob for deposit
	{
		$result = $this->user_model->updateReceivedamount();
		if ($result) {
			echo "success";
		} else {
			echo "failure";
		}
	}

	function cronupdateReceivedamountXMR() // cronjob for deposit
	{

		$result = $this->user_model->updateReceivedAmountXMR();

		if ($result) {
			echo "success";
		} else {
			echo "failure";
		}
	}

	function cronupdateReceivedamountXRP() // cronjob for deposit
	{
		// echo "string";
		$result = $this->user_model->updateReceivedAmountXRP();
		if ($result) {
			echo "success";
		} else {
			echo "failure";
		}
	}

	function cronupdateReceivedamountETH() // cronjob for deposit
	{
		$result = $this->user_model->updateReceivedAmountETH();
		if ($result) {
			echo "success";
		} else {
			echo "failure";
		}
	}

	function cronupdateReceivedamountETC() // cronjob for deposit
	{
		$result = $this->user_model->updateReceivedAmountETC();
		if ($result) {
			echo "success";
		} else {
			echo "failure";
		}
	}

	function cronmovetoadminETH() // cronjob for deposit
	{
		$where = "id=1";
		$admin = $this->user_model->get_data('giZfInSoOcZeItSiOs', $where, '', '', '', '', 'row');
		$address = $admin->eth_address;
		$pass = $admin->eth_userkey;

		$data = array('key' => $pass, 'adminaddress' => $address);
		$output = connecteth('toadminwallet', $data);
		echo "<pre>";
		print_r($output);exit;
	}

	function cronmovetoadminETC() // cronjob for deposit
	{
		$where = "id=1";
		$admin = $this->user_model->get_data('giZfInSoOcZeItSiOs', $where, '', '', '', '', 'row');
		echo "<pre>";
		// print_r($admin);
		$address = $admin->etc_address;
		$pass = $admin->etc_uerkey;

		$data = array('key' => $pass, 'adminaddress' => $address);
		$output = connectetc('toadminwallet', $data);
		echo "<pre>";
		print_r($output);exit;
	}
	function check_balance($value) {
		if ($value == 'ETH') {

			echo "<pre>";
			$data = array('data' => '');
			$output = connecteth('blockcount', $data);
			echo "<pre>";
			print_r($output);exit;
		} else {
			require_once 'jsonRPCClient.php';
			$bitcoin_row = $this->user_model->fetchWallet($value);
			$bitcoin_username = $bitcoin_row->username;
			$bitcoin_password = $bitcoin_row->password;
			$bitcoin_portnumber = $bitcoin_row->portnumber;
			$bitcoin = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@54.254.170.114:$bitcoin_portnumber/");
			echo '<pre>';
			print_r($bitcoin->getinfo());
			echo '<pre>';
			echo "======================";
			echo '<pre>';
			print_r($bitcoin->getbalance());
			echo '<pre>';
			echo "======================";
			echo '<pre>';
			print_r($bitcoin->listtransactions());
			echo '<pre>';

		}
	}

	function check_balance1($value = '', $t = '') {
		error_reporting(-1);
		ini_set('display_errors', 1);
		/*$output = shell_exec('curl -X POST http://54.254.170.114:18084/json_rpc -d \'{"jsonrpc":"2.0","id":"0","method":"transfer","params":{"destinations":[{"amount":10000000000,"address":"463tWEBn5XZJSxLU6uLQnQ2iY9xuNcDbjLSjkn3XAXHCbLrTTErJrBWYgHJQyrCwkNgYvyV3z8zctJLPCZy24jvb3NiTcTJ"}],"mixin":4,"get_tx_key": true,"payment_id":"614d8fa5465f46d79a29fecfed0667547499db179c914aacbb4a26f7c7ab39a4"}}\' -H \'Content-Type: application/json\'');*/
		echo 'check block XMR';
		//$output = shell_exec('curl -X POST http://54.254.170.114:18084/json_rpc -d \'{"jsonrpc":"2.0","id":"0","method":"getheight"}\' -H \'Content-Type: application/json\'');
		$output = shell_exec('curl -X POST http://35.177.165.200:18084/json_rpc -d \'{"jsonrpc":"2.0","id":"0","method":"getheight"}\' -H \'Content-Type: application/json\'');
		$res = json_decode($output);
		echo "<pre>";
		print_r($res);
		echo "</pre>";
		die;
		echo "string";
		echo $payment_data = $this->monero_wallet_model->balance();
		echo "<pre>";
		print_r(json_decode($payment_data, "TRUE"));
		echo "<br>";
		// echo "string";
		// $d = $this->monero_wallet_model->create_address();
		// echo "<pre>";
		// print_r($d);
		// echo "</pre>";

// 	if($value == 'ETH'){

// 		echo "<pre>";
		// 		$data 		   	= array('data'=>'');
		// 		$output 	   	= connecteth('blockcount',$data);
		// 		echo "<pre>";print_r($output);exit;
		// 	}else{
		// 		$transfer_method = $this->user_model->monero_request('getaddress');
		// 		echo "<pre>";
		// 		echo "hiiii";
		// 		print_r($transfer_method);
		// 		echo "</pre>";
		// 		echo "=================================";
		// 		$transfer_method = $this->user_model->monero_request('getbalance');
		// 		echo "<pre>";
		// 		echo "hiiii3333";
		// 		print_r($transfer_method);
		// 		echo "</pre>";

// 		exit;

// 	}
	}

	function get_xmr_address() {
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		//$url = '54.254.170.114/xmr_api.php';
		$url ='35.177.165.200/xmr_api.php';
		$name = $_SERVER['SERVER_NAME'];
		$data = array("method" => 'incoming_transfers', "name" => $name, "keyword" => 'tbeistctuiantom', 'data' => 'data');
		$data_string = json_encode($data);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($ch);
		curl_close($ch);
		// echo $response;
		// exit;
		// header('Content-Type: application/json');
		$result = json_decode($response);
		echo '<pre>';
		print_r($result);
		echo '</pre>';die;
		echo $result->keyword;
		die;
		if ($result->type == 'success') {
			// print_r($result->result);
			return $result->result;
		} else {

		}
	}

	function get_transaction($method = '', $add = '') {
		$output = file_get_contents('https://neoscan.io/api/main_net/v1/' . $method . '/' . $add . '');
		$result = json_decode($output);
		echo '<pre>';
		print_r($result);
		echo '</pre>';
	}
	function get_neo_address() {

		$url = '35.177.165.200/neo_api.php';//54.254.170.114/neo_api.php';
		$name = $_SERVER['SERVER_NAME'];
		$data = array("method" => 'createaddress', "name" => $name, "keyword" => 'tbeistctuiantom', 'data' => 'data');
		$data_string = json_encode($data);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($ch);
		curl_close($ch);
		// echo $response;
		// exit;
		// header('Content-Type: application/json');
		$result = json_decode($response, true);
		echo '<pre>';
		print_r($result);
		echo '</pre>';
/*echo 'response 1 = '.$result['response']->result;
echo 'response 2 = '.$result['response']['result'];*/

		$con_params = array(
			'id' => 1,
			'version' => 2.0,
			'user' => "",
			'password' => "",
			'ip' => "localhost",
			'port' => "10332",
		);

		$response1 = $this->curl_method_crypto($con_params, 'getasset');
		echo " <pre> responseee1 ";
		print_r($response1);
		/* $response2 = $this->curl_method_crypto($con_params, 'sendaddress');
			echo " <pre> responseee2 ";
		*/
		$responseee3 = $this->curl_method_crypto($con_params, 'gettxout');
		echo " <pre> responseee3 ";
		print_r($responseee3);
		$responseee4 = $this->curl_method_crypto($con_params, 'getbalance');
		echo " <pre> responseee4 ";
		print_r($responseee4);
	}

	function curl_method_crypto($connection_parms, $cmd, $postfields = array()) {

		$data = array();
		// $data['jsonrpc'] = $connection_parms['version'];
		// $data['id'] = $connection_parms['id'];
		// $data['method'] = $cmd;
		$data['params'] = $postfields;

		// $url = "http://" . $connection_parms['ip'] . ':' . $connection_parms['port'] . '?jsonrpc=' . $connection_parms['version'] . '&method=' . $cmd . '&params=' . $postfields . '&id=' . $connection_parms['id'];
		$url = '35.177.165.200/neo_api.php';//54.254.170.114/neo_api.php';
		$name = $_SERVER['SERVER_NAME'];
		$data = array("method" => $cmd, "name" => $name, "keyword" => 'tbeistctuiantom', 'data' => 'data');
		$data_string = json_encode($data);
		$ch = curl_init($url);
		//echo $url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_POST, count($postfields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$ret = curl_exec($ch);
		curl_close($ch);
		$res = json_decode($ret);
		return $res;
	}

	function check_bch_info() {

	}
	
	function cronTradeOrderBook()
	{
		echo "success";
	}//End of function
	
	
	//Mati Cronjob https://cron-job.org
	function cronmatistatus()
	{
		$this->user_model->cronmatistatus();
	}//End of Function
	
	
	//Mati Webhook url https://www.bitunio.com/bitunio/mati
	function mati()
	{
		$myfile = null;
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$body = file_get_contents('php://input');//$_POST["payloadBody"];//http_get_request_body();		
						
			try{
				$myfile = fopen(APPPATH."logs/websock.txt", "a+") or die("Error : ". base_url());
				//$txt = $txt;
				fwrite($myfile, "\n".$body);
				//fclose($myfile);
			}
			catch(Exception $e)
			{
				echo "Error : " . $e->getMessage();
			}
		
			$obj = json_decode($body, false);
			$ename = $obj->eventName;
		
			if($ename=="verification_completed")
			{
				$this->user_model->updateMati($body, 'Mujeeb');				
				
				/*$updatedata = array(
						//'fname'=>$name,
						//'Identity_card_no'=>$id_number,
						//'document1'=>$images1,
						//'document2'=>$images2,
						//'document3'=>$images3,
						'reason3'=>$resurl,
						'verification_status'=>"pending",
						'created_date'=>$date,
				);*/
				
				
			}//End of if
			else //Other EventName 
			{
				$this->user_model->updateMati($body, 'Mujeeb');
			}
		
		}//End of Server If
		//return "success";
	}//End of function mati
	
	
	
	
	
	function writetofile($txt)
	{
		/*$myfile = fopen("https://bitunio.com/assests/resource/websock.txt", "a") or die("Unable to open file!");
		//$txt = $txt;
		fwrite($myfile, "\n".$txt);
		fclose($myfile);*/
		
		
		try{
			$myfile = fopen(APPPATH."logs/websock.txt", "a+") or die("Error : ". base_url());		
			//$txt = $txt;
			fwrite($myfile, "\n".$txt);
			fclose($myfile);
		}
		catch(Exception $e)
		{
			echo "Error : " . $e->getMessage();
		}
		
	}//End of Function
	
	//End of Mati Integration
	
	
	
	function verifyApprove()
	{
		
	}//End of Function
	
	function getETHAddress()
	{
		echo $this->user_model->getETHAddress();
	}//End of Function
	
	
	
	//API Model by mujeeb
	

	//It Should return prices for all of crypto currency pairs
	public function ticker()
	{
		$rec = $this->user_model->get_crypto_prices();
		$return  = array('swagger'=>'2.0','prices'=>$rec);
		die(json_encode($return));		
	}//End of Function
	
	public function bookticker()
	{
		echo "My Second API";
	}//End of Function
	
	public function buyorder()
	{
		$auth = isset(getallheaders()['API_KEY']) ? getallheaders()['API_KEY'] : '';
		//var_dump($auth);
		
		if($auth=='')
		{
			$row_array['code'] = 500;
			$row_array['status'] = "Please pass 'API_KEY' in header Authorization type is API_Key";
			
			return $row_array;//"balance";
			//echo "Permission Denied, Please pass API_KEY in header Authorization type is API_Key";
			//exit();
		}
		
		$id = $this->getUserIDFromAuth($auth);
		if($id==0)
		{
			$row_array['code'] = 501;
			$row_array['status'] = "Access Denied";
				
			return $row_array;//"balance";
			//echo "Access Denied";
			exit;
		}
		
		$return = $this->user_model->create_limit_buyorder($id);
		
		die(json_encode($return));
		//echo 
		
	}//End of Function
	
	public function sellorder()
	{
		$auth = isset(getallheaders()['API_KEY']) ? getallheaders()['API_KEY'] : '';
		//var_dump($auth);
		
		if($auth=='')
		{
			$row_array['code'] = 500;
			$row_array['status'] = "Please pass 'API_KEY' in header Authorization type is API_Key";
				
			return $row_array;//"balance";
			//echo "Permission Denied, Please pass API_KEY in header Authorization type is API_Key";
			//exit();
		}
		
		$id = $this->getUserIDFromAuth($auth);
		if($id==0)
		{
			$row_array['code'] = 501;
			$row_array['status'] = "Access Denied";
		
			return $row_array;//"balance";
			//echo "Access Denied";
			exit;
		}
		
		$return = $this->user_model->create_limit_sellorder($id);
		
		die(json_encode($return));
	}//End of Function
	
	
	//Get All coins balances
	public function balance()
	{
		
		//echo "Mujeeb : " . $id;		
		//echo "Mujeeb " . isset( $_POST['uid'] ) . " : " . $_POST['uid'] . "  : " . $_SERVER['REQUEST_METHOD'] . " :  " . $id;
		$auth = isset(getallheaders()['API_KEY']) ? getallheaders()['API_KEY'] : '';
		//var_dump($auth);
		
		if($auth=='')
		{
			echo "Permission Denied, Please pass API_KEY in header Authorization type is API_Key";
			exit();
		}
		
		$id = $this->getUserIDFromAuth($auth);
		if($id==0)
		{
			echo "Unauthorised access";
			exit;
		}
		
		
		
		
		$balance = $this->user_model->get_balances_api($id);
		
		die(json_encode($balance));
	}//End of Function
	
	public function cancelorder()
	{
		$cid = $this->input->post('clientOrderId');
		
		
		$auth = isset(getallheaders()['API_KEY']) ? getallheaders()['API_KEY'] : '';
		//var_dump($auth);
		
		if($auth=='')
		{
			echo "Permission Denied, Please pass API_KEY in header Authorization type is API_Key";
			exit();
		}
		
		$id = $this->getUserIDFromAuth($auth);
		if($id==0)
		{
			echo "Unauthorised access";
			exit;
		}
		
		if($cid=="" || !isset($_POST['clientOrderId'] ))
		{
			echo "Post 'clientOrderId' to cancel this order";
			exit;
		}
		
		$tradeid = $this->getTradeID($cid);
		
		//echo "Mujeeb : " .$tradeid;
		//exit;
		
		if($tradeid==0 || $tradeid=="" || $tradeid=="0")
		{
			echo "Invalid clientOrderId, Please post valid clientOrderId".
			exit;			
		}
		
		$this->session->set_userdata('sessionCloseorder', $tradeid);
		$res_order = $this->user_model->remove_active_model($tradeid);
		
		$return  = array('code'=>200,'status'=>'Successfully order has been canceled');
		die(json_encode($return));
		//die('success');
		
	}//End of Function
	
	public function orderstatus()
	{
		
	}//End of Function
	
	
	public function getUserIDFromAuth($auth)
	{
		$this->db->where('Api_key', $auth);
		$query = $this->db->get('bitoauth');
		if ($query->num_rows() > 0)
		{
			$id = $query->row()->DiZrIeSsOu;
			return $id;
		}
		else
		{
			return "0";
		}
	}//End of FUnction
	
	function getTradeID($id)
	{
		$where = "clientOrderId=" . $id;
		$order = $this->user_model->get_data('rZeIdSrOoZnIiSoOc', $where, '', '', '', '', 'row');
		
		if ($order) 
		{
			//$userId = $order->DiZrIeSsOu;
			$tid = $order->trade_id;
		}
		else {
			$tid = "0";
		}
		
		return $tid;
	}//End of Function
	
	
	//ENd of API Model
	
	//-------------------------------Bot Orders------------------------
	function botSell()
	{
		
		
		
	  for($i=0;$i<2;$i++)
	  {
		$pair = "XRP_BTC";
		$amount = $this->random_float(10, 20, 0);
		$userid = 753;//User1
		
		$price = $this->user_model->get_crypto_prices_curr('XRP','BTC');
		
		//echo $i . " = " . $price;
		
		$this->user_model->create_botlimit_sellorder($userid, $amount, $pair, $price);
		sleep($i);
	  }
	}//End of Function
	
	function botBuy()
	{
		
	}//End of Function
	
	
	function toFixed($number, $decimals) {
		return number_format($number, $decimals, '.', "");
	}//End of Function
	
	function random_float($min, $max, $float)
	{		
		return $this->toFixed((($min + ($max - $min)) * mt_rand(0, mt_getrandmax()) / mt_getrandmax()), $float);		
	}//End of Function
	
	function getBuySell()
	{
		$t = getRandomValue(1,2);
		if($t%2==0)
		{
			return "SELL";
		}
		else
		{
			return "BUY";
		}
	}//End of Function
	
	//-------------------------------End of Bot Orders-----------------
	
	
	
	
	
	
	
}//End of class


