<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class AOdSmIiZn extends CI_Controller {
	public function __construct() {
		error_reporting(E_ERROR);
		parent::__construct();
		$this->load->database();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		header('X-Frame-Options: SAMEORIGIN');
		//header('Access-Control-Allow-Origin: true');
		header('X-XSS-Protection: 1; mode=block');
		header('X-Content-Type-Options: nosniff');
		$base = base_url();
		header("ALLOW-FROM: $base");
		header("X-Powered-By: $base");

		ini_set('session.gc_maxlifetime', 300);
		ini_set('session.cookie_httponly', 1);
		ini_set('session.use_only_cookies', 1);
		ini_set('session.cookie_secure', 1);

		if ($this->input->post('username') == '' && !$this->session->userdata('loggeduser')) {
			$uri = $_SERVER['REQUEST_URI'];
			$uriArray = explode('/', $uri);
			if (count($uriArray) > 1) {
				$page_url = $uriArray[1];
				$page_url_2 = $uriArray[2];
				if ($page_url == 'AOdSmIiZn' || $page_url_2 == 'AOdSmIiZn' || $page_url == 'AOdSmIiZn.jsp' || $page_url_2 == 'AOdSmIiZn.jsp') {
					redirect('error', 'refresh');
				}

			}
		}

		$this->admin = $this->admin_model->get_admindetails();
	}
	////common-start
	function mail_settings() {
		$this->load->library('email');
		$config['wrapchars'] = 76;
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$this->email->initialize($config);
	}
	function index() {

		$query = $this->db->get('giZfInSoOcZeItSiOs');
		$data['admin_dir'] = $query->row()->admin_url;
		$data['company_name'] = $this->admin->company_name;

		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$sessionvar = $this->session->userdata('loggeduser');
		if ($sessionvar != "") {
			$this->load->view('admin/index', $data);
		} else {
			$this->form_validation->set_rules('username', 'User Name', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_message('required', "Enter %s ");
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('admin/login', $data);
			} else {
				if ($this->input->post('username')) {
					$result = $this->admin_model->logincheck();
					if (!$result) {
						$data = array(
							'uname' => $this->input->post('username'),
							'pwd' => $this->input->post('password'),
							'ip' => $_SERVER['REMOTE_ADDR'],
							'browser_name' => $_SERVER['HTTP_USER_AGENT'],
							'reason' => 'Invalid User name or Password or Pettern',
							'polute_date' => date('Y-m-d H:i:s'),
						);
						$this->db->insert("etZuIlSoOp", $data);

						$data['invalid'] = '<font color="#CC0000">Invalid Username or Password or Pattern</font>';
						$this->load->view('admin/login', $data);
					} else {
						$this->load->view('admin/index', $data);
					}
				}
			}
		}
	}

	function admin_change_password() {

		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$sessionvar = $this->session->userdata('loggeduser');
		$subId = $this->session->userdata('subId');
		if ($subId != "") {
			redirect('AOdSmIiZn/index', 'refresh');
		}
		if ($sessionvar == "") {
			//$this->load->view('admin_login',$data);
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$this->form_validation->set_rules('password', 'Current Password', 'required');
			$this->form_validation->set_rules('newpassword', 'New password', 'required');
			$this->form_validation->set_rules('repassword', "re-enter password", 'required|matches[newpassword]');
			$this->form_validation->set_message('required', "%s  Required");
			$this->form_validation->set_message('matches', "Missmatching Password");
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('admin/account', $data);
			} else {
				$result = $this->admin_model->admin_change_pswd();
				if (!$result) {
					$this->session->set_flashdata('error', 'Sorry Wrong Password details');
					redirect('AOdSmIiZn/admin_change_password');
				} else {
					$this->session->set_flashdata('success', ' Details has been Edited Successfully');
					redirect('AOdSmIiZn/admin_change_password');
				}
			}
		}
	}

	function forgot() {
		$query = $this->db->get('giZfInSoOcZeItSiOs');
		$data['admin_dir'] = $query->row()->admin_url;
		$this->load->view('admin/forgot', $data);
	}

	function pattern() {
		$query = $this->db->get('giZfInSoOcZeItSiOs');
		$data['admin_dir'] = $query->row()->admin_url;
		$this->load->view('admin/pattern', $data);
	}

	function logout() {

		$sessionvar = $this->session->userdata('loggeduser');
		$admin_url = site_config()->admin_url;
		if ($sessionvar == "") {
			redirect($admin_url, 'refresh');
		} else {
			$this->session->sess_destroy();
			redirect($admin_url, 'refresh');
		}
	}

	function admin_settings() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {

			$query = $this->admin_model->getadmindetails();

			if ($query) {

				foreach ($query as $adminrow) {
					$data['id'] = $adminrow->id;
					$data['companyname'] = $adminrow->company_name;
					$data['contactperson'] = $adminrow->contact_person;
					$data['emailid'] = $adminrow->dilZiIaSmOe;
					$data['address'] = $adminrow->sseZrIdSdOa;
					$data['city'] = $adminrow->city;
					$data['state'] = $adminrow->state;
					$data['phone'] = $adminrow->phno;
					$data['fax_no'] = $adminrow->fax_no;
					$data['admin_url'] = $adminrow->admin_url;
					$data['paypal_emailid'] = $adminrow->paypal_emailid;
					$data['intRows'] = $adminrow->intRows;
					$data['facebook_url'] = $adminrow->facebook_url;
					$data['twitter_url'] = $adminrow->twitter_url;
					$data['google_plus'] = $adminrow->google_plus;
					$data['medium_url'] = $adminrow->medium_url;
					$data['registration'] = $adminrow->registration;
					$data['pinterest'] = $adminrow->pinterest;
					$data['smtp_host'] = $adminrow->smtp_host;
					$data['smtp_port'] = $adminrow->smtp_port;
					$data['smtp_username'] = $adminrow->smtp_username;
					$data['smtp_password'] = $adminrow->smtp_password;
					$data['meta_keyword'] = $adminrow->meta_keyword;
					$data['trade_status'] = $adminrow->trade_status;
					$data['exchange_status'] = $adminrow->exchange_status;
					$data['offer_expire'] = $adminrow->offer_expire;
					$data['meta_description'] = $adminrow->meta_description;
					//$data['mollie_api_key'] 	=   $adminrow->mollie_api_key;
					$data['footer_content'] = $adminrow->footer_content;
					$data['referral_credit_amount'] = $adminrow->referral_credit_amount;
					$data['referral_credit_amount_exchange'] = $adminrow->referral_credit_amount_exchange;
					$data['password_expire'] = $adminrow->password_expire;
					$data['emailid1'] = $adminrow->email_id1;
					$data['emailid2'] = $adminrow->email_id2;
					$data['emailid3'] = $adminrow->email_id3;
					$data['emailid4'] = $adminrow->email_id4;
					$data['emailid5'] = $adminrow->email_id5;
					$data['paypal_mode'] = $adminrow->paypal_mode;
					//$data['username']		=$this->db->query("select username from egZaInSaOm where id=1")->row()->username;
					$data['username'] = 'Admin';
				}
				$this->load->view('admin/settings', $data);
			}
		}
	}

	function admin_setting() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$this->db->where("id", 1);
			$ft = $this->db->get("egZaInSaOm")->row()->ft;

			$dta = array(
				//'dilZiIaSmOe'			=>	$this->input->post('email'),
				'emanZrIeSsOu' => $this->input->post('username'));
			if ($this->input->post('ft1') != "") {

				if ($ft == $this->input->post('ft')) {
					$dta = array(
						//'dilZiIaSmOe'			=>	$this->input->post('email'),
						'emanZrIeSsOu' => $this->input->post('username'),
						'ft' => $this->input->post('ft1'),
					);
				} else {
					$this->session->set_flashdata('error', ' Please give right patten to proceed');
					redirect('AOdSmIiZn/admin_settings', 'refresh');
				}

			}

			$this->db->where("id", 1);
			$this->db->update("egZaInSaOm", $dta);

			$id = $this->input->post('id');
			$result = $this->admin_model->general_settings();
			if ($result) {
				$this->session->set_flashdata('success', ' Details has been Edited Successfully');
				redirect('AOdSmIiZn/admin_settings', 'refresh');
			} else {
				$this->session->set_flashdata('error', ' Details has not been Edited ');
				redirect('AOdSmIiZn/admin_settings', 'refresh');
			}

		}
	}

	function userdetails() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		$subId = $this->session->userdata('subId');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} elseif ($ad_id != "") {
			$data['view'] = "View";
			$data['count'] = $this->admin_model->userscount();
			$data['count_new'] = $this->admin_model->newuserscount();
			$data['result'] = $this->admin_model->getuser_details();
			$this->load->view('admin/userdetails', $data);
		} elseif ($subId != "") {
			$getsubadmindetail = $this->admin_model->getsubadmin_details();
			if ($getsubadmindetail) {
				foreach ($getsubadmindetail as $permision) {
					$permission = $permision->permission;
				}
			}
			$cats = explode(",", $permission);
			$inc = 0;
			foreach ($cats as $sidebar) {
				if ($sidebar == "1") {
					$data['view'] = "View";
					$data['result'] = $this->admin_model->getuser_details();
					$this->load->view('admin/userdetails', $data);
					$inc++;
				}
			}
			if ($inc == "0") {
				redirect('AOdSmIiZn/subadminlogin', 'refresh');
			}
		}
	}
	function broker_chang() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$data['userdetails'] = array_reverse($this->db->get_where("SliIaStOeZdIrSeOsu", array('bro_status!=' => ''))->result());
			$this->load->view('admin/broker_chang', $data);
		}
	}
	function changestatus_userdetails($id) {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			if ($this->admin_model->changestatus_userdetails($id)) {
				$queryy = $this->admin_model->getuserdetails($id);
				foreach ($queryy as $row) {
					$id = $row->user_id;
					$emailid = $row->emailid;
					$status = $row->status;
				}
				if ($status == "deactive") {
					$stat = "deactivated";
				} else {
					$stat = "Activated";
				}

				// $this->session->set_flashdata('success', $emailid." is  ".$status."  now");
				$this->session->set_flashdata('success', "User has been $stat successfully");
				redirect('AOdSmIiZn/userdetails', 'referesh');
			}
		}
	}

	function changestatus_tfa($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$data = array('Tfa_status' => '');
			$this->db->where('DiZrIeSsOu', $id);
			$query = $this->db->update('SliIaStOeZdIrSeOsu', $data);
			if ($query) {
				$this->session->set_flashdata('success', "TFA Status Disabled successfully");
				redirect('AOdSmIiZn/userdetails', 'referesh');
			} else {
				$this->session->set_flashdata('success', "Some error occured");
				redirect('AOdSmIiZn/userdetails', 'referesh');
			}
		}
	}

	function changestatus_subadmin($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			if ($this->admin_model->changestatus_subadmin($id)) {
				$queryy = $this->admin_model->getsubadmin($id);
				foreach ($queryy as $row) {
					$status = $row->status;
				}
				if ($status == "deactive") {
					$stat = "deactivated";
				} else {
					$stat = "Activated";
				}

				// $this->session->set_flashdata('success', $emailid." is  ".$status."  now");
				$this->session->set_flashdata('success', "Subadmin has been $stat successfully");
				redirect('AOdSmIiZn/subadmin/view', 'referesh');
			}
		}
	}

	function edit_userdetails($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$this->session->set_userdata('userdetailsid', $id);
			redirect('AOdSmIiZn/edit_userdetail', 'referesh');
		}
	}

	function edit_userdetail() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$id = $this->session->userdata('userdetailsid');
			$fetchdata = $this->admin_model->getuserdetails($id);
			if ($fetchdata) {
				$query = $this->admin_model->getuserdetails($id);
				foreach ($query as $row) {
					$data['id'] = $row->DiZrIeSsOu;
					$data['username'] = $row->emanZrIeSsOu;
					$data['emailid'] = $this->user_model->get_email_admin($row->DiZrIeSsOu);
					$data['password'] = $row->drowZsIsSaOp;
					$data['profilepicture'] = $row->profilepicture;
					$data['status'] = $row->status;
				}
				$data['edit'] = "Edit";
				$data['count'] = $this->admin_model->userscount();
				$data['count_new'] = $this->admin_model->newuserscount();
				$this->load->view('admin/userdetails', $data);
			}
		}
	}
	function edit_user_detail() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			if ($this->input->post('submit')) {

				$this->form_validation->set_rules('username', 'First Name', 'required');
				$this->form_validation->set_rules('emailid', 'Email Id', 'required|valid_email');
				// $this->form_validation->set_rules('password','Password', 'required');
				$this->form_validation->set_message('required', "%s Required");
				$this->form_validation->set_message('valid_email', "%s Should be valid");
				if ($this->form_validation->run() == FALSE) {
					$id = $this->input->post('id');
					$queryy = $this->admin_model->getuserdetails($id);
					foreach ($queryy as $row) {
						$data['id'] = $row->user_id;
						$data['username'] = $row->username;
						$data['emailid'] = $row->emailid;
						$data['password'] = $row->password;
						$data['profilepicture'] = $row->profilepicture;
						$data['status'] = $row->status;
						$data['dni'] = $row->dninumber;
					}
					$data['edit'] = "Edit Category";
					$this->load->view('admin/userdetails', $data);
				} else {

					$exist = "";
					$id = $this->input->post('id');
					$emailid = $this->input->post('emailid');
					$oldemail = $this->input->post('oldemail');
					$username = $this->input->post('username');

					$oldusername = $this->input->post('oldusername');

					if ($oldemail != $emailid || $username != $oldusername) {
						$rr = $this->admin_model->getuser_available($emailid, $username);

						if ($this->admin_model->getuser_available($emailid, $username)) {
							$exist = "exist";
							$query = $this->admin_model->getuserdetails($id);
							foreach ($query as $row) {
								$data['id'] = $row->DiZrIeSsOu;
								$data['username'] = $row->emanZrIeSsOu;
								$data['emailid'] = $row->dilZiIaSmOe;
								$data['password'] = $row->drowZsIsSaOp;

								$data['status'] = $row->status;
							}
							$data['edit'] = "Edit ";
							//Email of customer  ".$emailid."  Already Exist
							$data['error'] = " Email / Username is  Already Exist .";
							$this->load->view('admin/userdetails', $data);
						}
					}
					if ($exist == "") {
						$id = $this->input->post('id');
						$emailid = $this->input->post('emailid');
						$username = $this->input->post('username');
						$profile_image = $_FILES['profile_image']['name'];
						$random = rand(0, 99999);
						$filename = $random . "_" . $username;
						$config['upload_path'] = 'uploader/customers/profilepicture';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['file_name'] = $filename;
						//$config['max_size'] = '1000';
						//$config['max_width'] = '1920';
						//$config['max_height'] = '1280';
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if (($_FILES['profile_image']['name'] != "") & (!$this->upload->do_upload('profile_image'))) {
							//$this->session->set_flashdata('error',$this->upload->display_errors());
							$id = $this->input->post('id');
							$queryy = $this->admin_model->getuserdetails($id);
							foreach ($queryy as $row) {
								$data['id'] = $row->user_id;
								$data['username'] = $row->username;
								$data['emailid'] = $row->emailid;
								$data['password'] = $row->password;
								$data['profilepicture'] = $row->profilepicture;
								$data['status'] = $row->status;
								$data['dni'] = $row->dninumber;
							}
							$data['edit'] = "Edit Category";
							$data['error'] = $this->upload->display_errors();
							$this->load->view('admin/userdetails', $data);
						} else {
							$id = $this->input->post('id');
							$profileimage = $_FILES['profile_image']['name'];
							@list($imagename, $ext) = explode(".", $profileimage);
							$filename = $random . "_" . $username . "." . $ext;
							if ($this->admin_model->edit_userdetails($id, $filename)) {
								$this->session->set_flashdata('success', "Details of the Customer  " . $emailid . "  Has been Edited Successfully");
								redirect('AOdSmIiZn/userdetails', 'referesh');
							}
						}
					}
				}
			}
		}
	}

	function edit_userdetails_del($id) {

		$this->admin_model->get_delte_user($id);

		$this->session->set_flashdata('success', 'User deleted successfully');

		redirect('AOdSmIiZn/userdetails', 'referesh');
	}

	function editfaq_before($id) {

		$res['each_pay'] = $this->admin_model->get_each_faq($id);

		$this->load->view('admin/editfaq_before', $res);
	}

	function faq_submit() {

		$this->admin_model->update_myfaq();

		$this->session->set_flashdata('success', 'FAQ contents updated successfully');

		redirect("AOdSmIiZn/faq");
	}

	function faq_changestatus($id) {

		$this->admin_model->update_myfaq_status($id);

		$this->session->set_flashdata('success', 'FAQ Status updated successfully');

		redirect("AOdSmIiZn/faq");
	}

	function faq_delete($id) {

		$this->admin_model->update_faq_delete($id);

		$this->session->set_flashdata('success', 'FAQ Deleted successfully');

		redirect("AOdSmIiZn/faq");
	}

	function faq() {

		$res['faqlist'] = $this->admin_model->get_faqlist();
		$this->load->view('admin/faq', $res);
	}

	function editnews_before($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {

			$res['each_pay'] = $this->admin_model->get_each_news($id);
			$res['category'] = $this->user_model->blog_cat();

			$this->load->view('admin/editnews_before', $res);
		}
	}

	function news_submit() {
		//echo $this->input->post('language'); exit;

		if ($_FILES['img']['name'] != "" && $this->input->post('pagecontent') != "") {
			$this->admin_model->update_mynews();
			$this->session->set_flashdata('success', 'Blogs contents updated successfully');
		} else {
			$this->session->set_flashdata('error', 'Please fill all fields and upload image');
		}
		redirect("AOdSmIiZn/news");
	}

	function news_changestatus($id) {

		$this->admin_model->update_mynews_status($id);

		$this->session->set_flashdata('success', 'News Status updated successfully');

		redirect("AOdSmIiZn/news");
	}

	function news_delete($id) {

		$this->admin_model->update_news_delete($id);

		$this->session->set_flashdata('success', 'news Deleted successfully');

		redirect("AOdSmIiZn/news");
	}

	function news() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$res['newslist'] = array_reverse($this->admin_model->get_newslist());

			$this->load->view('admin/news', $res);
		}
	}
	
	
	//By Mujeeb
	function view_usr_reverify($id)
	{
		//$this->session->set_userdata('userVerifyId', $id);
		$this->admin_model->sendEmailReverify($id);
		
		redirect('AOdSmIiZn/user_verification', 'referesh');
	}//End of Function
	

	function view_usr_verify($id)
	{	
		$this->session->set_userdata('userVerifyId', $id);
		redirect('AOdSmIiZn/viewuser_verification', 'referesh');
	}//End of Function
	
	function viewuser_verification() {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$userVerifyId = $this->session->userdata('userVerifyId');
			$data['row'] = $this->admin_model->getuserverification($userVerifyId);
			$data['display'] = "Display Details";

			$this->load->view('admin/userverification', $data);
		}
	}

	function user_verification() {
    
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		$subId = $this->session->userdata('subId');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$data['result'] = $this->admin_model->getuserverificationstatus();
			$data['view'] = "View Details";
			$this->load->view('admin/userverification', $data);
		}
	}

	

	function update_userverificationstatus($str, $num, $id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$result = $this->admin_model->update_userverificationstatus($str, $num, $id);
			if ($result) {
				$get_id = $this->db->query("select DiZrIeSsOu from noitZaIcSiOfZiIrSeOvZrIeSsOu where verifyId='$id'")->row()->DiZrIeSsOu;
				if ($str == 'Approved') {
					$this->session->set_flashdata('success', 'Approved Successfully');
					$this->admin_model->sendemailuser($get_id);
				} else {
					$this->session->set_flashdata('success', 'Verification Rejected.');
					$this->admin_model->sendemailuser1($get_id);
				}
			} else {
				$this->session->set_flashdata('error', 'Oops! try again later.');
			}

			$this->session->set_userdata('userVerifyId', $id);
			redirect('AOdSmIiZn/user_verification', 'referesh');

		}
	}

	function bankdetail($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$row = $this->admin_model->bankdetail($id);
			if ($row) {
				$data['account_owner'] = $row->account_owner;
				$data['admin_address'] = $row->admin_address;
				$data['admin_city'] = $row->admin_city;
				$data['country'] = $row->country;
				$data['acc_number'] = $row->acc_number;
				$data['iban'] = $row->iban;
				$data['message'] = $row->message;
				$data['bank_name'] = $row->bank_name;
				$data['bank_address'] = $row->bank_address;
				$data['currency'] = $row->currency;
				$data['bank_city'] = $row->bank_city;
				$data['bank_country'] = $row->bank_country;
				$data['BIC'] = $row->BIC;
				$this->load->view('admin/bankdetail', $data);
			}
		}
	}
	function edit_bankdetail() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$error = "";
			if ($this->input->post('submit')) {
				$this->form_validation->set_rules('account_owner', 'Account Holder', 'required');
				// $this->form_validation->set_rules('admin_address','Address', 'required');
				// $this->form_validation->set_rules('admin_city','City', 'required');
				// $this->form_validation->set_rules('country','Country', 'required');
				$this->form_validation->set_rules('acc_number', 'Account Number', 'required');
				$this->form_validation->set_rules('iban', 'IBAN', 'required');
				$this->form_validation->set_rules('message', 'Message', 'required');
				$this->form_validation->set_rules('bank_name', 'Bank Name', 'required');
				//$this->form_validation->set_rules('bank_address','Bank Address', 'required');
				//$this->form_validation->set_rules('bank_city','Bank City', 'required');
				$this->form_validation->set_rules('bank_country', 'Bank Country', 'required');
				//$this->form_validation->set_rules('BIC','BIC', 'required');
				$this->form_validation->set_message('required', "%s Required");
				if ($this->form_validation->run() == FALSE) {
					$row = $this->admin_model->bankdetail();

					$data['account_owner'] = $row->account_owner;
					$data['admin_address'] = $row->admin_address;
					$data['admin_city'] = $row->admin_city;
					$data['country'] = $row->country;
					$data['acc_number'] = $row->acc_number;
					$data['iban'] = $row->iban;
					$data['message'] = $row->message;
					$data['bank_name'] = $row->bank_name;
					$data['bank_address'] = $row->bank_address;
					$data['bank_city'] = $row->bank_city;
					$data['bank_country'] = $row->bank_country;
					$data['BIC'] = $row->BIC;
					$this->load->view('admin/bankdetail', $data);
				} else {
					if ($this->admin_model->edit_bankdetail()) {
						$this->session->set_flashdata('success', "Details has been edited successfully");
					} else {
						$this->session->set_flashdata('error', "Details of the Post can't be edited successfully.");
					}
					redirect('AOdSmIiZn/banklist', 'referesh');
				}
			} else {
				redirect('AOdSmIiZn/banklist', 'referesh');
			}
		}
	}

	function international_depositreq() {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		$subId = $this->session->userdata('subId');
		if ($ad_id == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$data['result'] = $this->admin_model->international_deposit('confirmed');
			// $data['type']="Processing";
			$data['view'] = "View";
			$this->load->view('admin/international_deposit', $data);
		}
	}
	function deposit_confirm($id, $currency) {

		$result = $this->admin_model->deposit_confirm($id, $currency);
		$this->session->set_flashdata('success', "The Processing Successfully completed");
		redirect('AOdSmIiZn/international_depositreq', 'refresh');
	}
	function deposit_confirm1($id, $user_id) {
		$result = $this->admin_model->deposit_confirm1($id, $user_id);
		$this->session->set_flashdata('success', "The Processing Successfully Rejected");
		redirect('AOdSmIiZn/international_depositreq', 'refresh');
	}

	function commission() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {

			$data['currencylist'] = $this->admin_model->commission();
			$this->load->view('admin/commissionlist', $data);
		}

	}
	function commission1() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {

			$data['currencylist'] = $this->admin_model->commission1();

			$this->load->view('admin/commissionlist1', $data);
		}
	}
	function deposit() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {

			$data['tradelist'] = $this->admin_model->get_deposit1();
			$this->load->view('admin/deposit1', $data);
		}
	}

	function deposit_history() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {

			$data['deposit'] = $this->db->query("SELECT a.*,b.emanZrIeSsOu FROM `yrotsiZhInSoOiZtIcSaOsZnIaSrOt` as a inner join SliIaStOeZdIrSeOsu as b on a.DiZrIeSsOu=b.DiZrIeSsOu where a.type='Deposit' and find_in_set(a.currency,'BTC,ETC,DOGE,XRP,XMR,ETH,ETC,ZEC,DASH,LTC,NEO')")->result();
			$this->load->view('admin/deposit_history', $data);
		}
	}
	function withdraw($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			if ($this->input->post('sub')) {

				$data['withdrawlist'] = $this->admin_model->get_withdrawlist();
				$data['id'] = $this->input->post('hidden');

			} else {

				$data['id'] = $id;
				$data['withdrawlist'] = $this->admin_model->get_active_realcurrencies_withdraw($id);

			}
			$this->load->view('admin/withdrawlist', $data);
		}
	}

	function withdraw_view($id) {
		$data['row'] = $this->db->query("select * from tseZuIqSeOrZwIaSrOdZhItSiOw where req_id=$id")->row();
		$this->load->view('admin/withdraw_view', $data);
	}

	function withdraw_accept1() {

		$this->session->set_userdata('req_id', $this->uri->segment('3'));

		redirect('AOdSmIiZn/withdraw_accept', 'refresh');
	}
	function withdraw_reject1() {

		$this->session->set_userdata('req_id', $this->uri->segment('3'));
		$this->session->set_userdata('content', $this->input->post('mail_content'));
		redirect('AOdSmIiZn/withdraw_reject', 'refresh');
	}
	function withdraw_accept() {

		$req_id = $this->session->userdata('req_id');
		$this->db->where("req_id", $req_id);
		$data = $this->db->get("tseZuIqSeOrZwIaSrOdZhItSiOw");
		$datas = $data->row();
		$amount = $datas->amount;
		$askamount = $datas->askamount;
		$fee1 = $askamount - $amount;
		$fee = number_format((float) $fee1, 2, '.', '');
		$cur = $datas->currency;
		$user_id = $datas->DiZrIeSsOu;
		$this->db->where("DiZrIeSsOu", $user_id);
		$userdetails = $this->db->get("SliIaStOeZdIrSeOsu");
		$userde = $userdetails->row();
		//$email=$userde->emailid;
		$email = $this->admin_model->userdetailemail($user_id);

		$date = date('Y-m-d');
		$time = date("h:i:s");
		$data2 = array(
			'DiZrIeSsOu' => $user_id,
			'feeAmount' => $fee1,
			'feeCurrency' => $cur,
			'description' => "Withdraw",
			'date' => $date,
			'time' => $time,
		);
		$res = $this->db->insert('eefZnIiSoOc', $data2);

		$name = $this->admin_model->get_user_name($user_id);
		//$name=$userde->username;
		$date = date('Y-m-d');
		$time = date("h:i:s");

		$this->db->where('id', '13');
		$queryemail_temp = $this->db->get('setalpZmIeStOlZiIaSmOe');
		if ($queryemail_temp->num_rows() == 1) {
			$get_emailtemp = $queryemail_temp->row();
			$message = $get_emailtemp->message;
			$subject = $get_emailtemp->subject;
			$mail_content = array(

				'##AMOUNT##' => $amount,
				'##CURRENCY##' => $cur,
				'##FEE##' => $fee,

			);

			$content = strtr($message, $mail_content);
			$msg = $content;

		}
		//$admin_email=admin_email();
		$this->user_model->mailsettings();
		$this->email->from(admin_email());
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($msg);
		$this->email->send();

		$this->db->where('req_id', $req_id);
		$this->db->update('tseZuIqSeOrZwIaSrOdZhItSiOw', array('status' => 'accepted'));
		$this->session->set_flashdata('success', 'Successfully Accepted Withdraw Request');
		redirect('AOdSmIiZn/withdraw/1', 'refresh');
	}

	function withdraw_reject() {

		$content = $this->session->userdata('content');
		$req_id = $this->session->userdata('req_id');
		$this->db->where("req_id", $req_id);
		$data = $this->db->get("tseZuIqSeOrZwIaSrOdZhItSiOw");
		$datas = $data->row();
		$amount = $datas->amount;
		$amount1 = $datas->askamount;
		$cur = $datas->currency;
		$user_id = $datas->DiZrIeSsOu;
		$this->db->where("DiZrIeSsOu", $user_id);
		$userdetails = $this->db->get("SliIaStOeZdIrSeOsu");
		$userde = $userdetails->row();
		//$email=$userde->emailid;
		$email = $this->admin_model->userdetailemail($user_id);

		$this->db->where("DiZrIeSsOu", $user_id);
		$blnc = $this->db->get('ecnZaIlSaObZrIeSsOuZnIiSoOc');
		$user_blnc = $blnc->row();
		$o_blnce = $user_blnc->$cur;

		$date = date('Y-m-d');
		$time = date("h:i:s");

		$get_email_info = $this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='14'")->row();

		$msg = $get_email_info->message;
		$subject = $get_email_info->subject;
		$msg = str_replace("##REASON##", $content, $msg);
		$msg = str_replace("##COMPANYNAME##", company_name(), $msg);
		$this->user_model->mailsettings();
		$this->email->from(admin_email());
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($msg);
		$this->email->send();

		$this->db->where('req_id', $req_id);
		$this->db->update('tseZuIqSeOrZwIaSrOdZhItSiOw', array('status' => 'rejected'));
		$n_balance = $amount1 + $o_blnce;

		$this->db->where("DiZrIeSsOu", $user_id);
		$this->db->update('ecnZaIlSaObZrIeSsOuZnIiSoOc', array($cur => $n_balance));

		$this->session->set_flashdata('success', 'Successfully Rejected Withdraw Request');
		redirect('AOdSmIiZn/withdraw/1', 'refresh');
	}

	function withdraw_history() {

		$cur_users = $this->admin_model->separated_user();

		$id1 = $this->session->userdata('id');
		if ($id1 != "1") {
			if ($cur_users) {

				$data['withdraw'] = $this->db->query("SELECT a.*,b.emanZrIeSsOu FROM  `ZwIaSrOdZhItSiOwZnIiSoOc` as a inner join SliIaStOeZdIrSeOsu as b on a.DiZrIeSsOu=b.DiZrIeSsOu AND a.DiZrIeSsOu IN ($cur_users) order by a.with_id desc")->result();
			} else {
				$data['withdraw'] = "";
			}
		} else {
			$data['withdraw'] = $this->db->query("SELECT a.*,b.emanZrIeSsOu FROM  `ZwIaSrOdZhItSiOwZnIiSoOc` as a inner join SliIaStOeZdIrSeOsu as b on a.DiZrIeSsOu=b.DiZrIeSsOu order by a.with_id desc")->result();
		}

		$this->load->view('admin/withdraw_history', $data);
	}

	function trade1() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		$subId = $this->session->userdata('subId');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} elseif ($ad_id != "") {
			$data['result'] = $this->admin_model->trade_details();
			$this->load->view('admin/trade_history', $data);
		} elseif ($subId != "") {
			$getsubadmindetail = $this->admin_model->getsubadmin_details();

			if ($getsubadmindetail) {
				foreach ($getsubadmindetail as $permision) {
					$permission = $permision->permission;
				}
			}
			$cats = explode(",", $permission);
			$inc = 0;
			foreach ($cats as $sidebar) {
				if ($sidebar == 9) {
					$data['result'] = $this->admin_model->trade_details();
					$this->load->view('admin/trade_history', $data);
					$inc++;
				}
			}
			if ($inc == 0) {
				redirect('AOdSmIiZn/subadminlogin', 'refresh');
			}
		}
	}
	function orderlist() {
		$data['tradelist'] = $this->admin_model->orderlist();
		$this->load->view('admin/orderlist', $data);
	}
	function currencypairlist() {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index');
		} else {
			$data['currencylist'] = $this->admin_model->get_currencypairlist();
			$this->load->view('admin/currencypairlist', $data);
		}
	}

	function exchangepairlist() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index');
		} else {
			$data['exchangepairlist'] = $this->admin_model->exchangepairlist();
			$this->load->view('admin/exchangepairlist', $data);
		}
	}

	function categorylist() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index');
		} else {
			$data['categorylist'] = $this->user_model->blog_cat();
			$this->load->view('admin/categorylist', $data);
		}
	}

	function add_pair() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$this->load->view('admin/add_pair');
		}
	}
	function add_category($id) {
		$fet['each_curr'] = $this->admin_model->each_category($id);
		$this->load->view('admin/add_category', $fet);

	}
	function add_pair1() {
		$first = $this->input->post('currency_name');
		$second = $this->input->post('currency_symbol');

		$qry1 = $this->admin_model->check_pair($first, $second);
		if ($qry1) {
			$this->admin_model->add_pair($first, $second);
			$this->session->set_flashdata('success', 'Successfully Added New Pair'); //
			redirect('AOdSmIiZn/currencypairlist', 'refresh');
		} else {
			$this->session->set_flashdata('error', 'Some error Occured Try Again '); //
			redirect('AOdSmIiZn/currencypairlist', 'refresh');
		}
	}

	function add_currencypair($id) {

		$fet['each_curr'] = $this->admin_model->each_currencypair($id);
		$this->load->view('admin/add_currencypair', $fet);

	}
	function add_currency_updte() {

		$currency_id = $this->input->post('currency_id');

		if ($currency_id == '') {

			$this->admin_model->insert_currency();

			$this->session->set_flashdata('success', 'New Currency Added Successfully');

			redirect('AOdSmIiZn/currencylist', 'refresh');
		} else {
			$this->admin_model->update_currency();

			$this->session->set_flashdata('success', 'Currency Updated Successfully');

			redirect('AOdSmIiZn/currencylist', 'refresh');
		}
	}
	function add_currencypair_updte() {

		$currency_id = $this->input->post('currency_id');

		$this->admin_model->update_currencypair();

		$this->session->set_flashdata('success', 'Currency pair Updated Successfully');

		redirect('AOdSmIiZn/currencypairlist', 'refresh');

	}

	function add_category_updte() {

		$cat_id = $this->input->post('cat_id');
		$category = $this->input->post('category');
		$query = $this->db->query("select * from yrogZeItSaOcZgIoSlOb where category='$category'");
		//echo $query->num_rows(); exit;

		$data = array(
			'category' => $this->input->post('category'),
			'status' => $this->input->post('status'),
			'datetime' => date('Y-m-d H:i:s'),
		);
		if ($cat_id != "") {
			$this->db->where("cat_id", $cat_id);
			$this->db->update("yrogZeItSaOcZgIoSlOb", $data);
			$this->session->set_flashdata('success', 'Category Updated Successfully');
			redirect('AOdSmIiZn/categorylist', 'refresh');
		} else {
			if ($query->num_rows() <= 0) {

				$this->db->insert("yrogZeItSaOcZgIoSlOb", $data);
				$this->session->set_flashdata('success', 'Category Added Successfully');
				redirect('AOdSmIiZn/categorylist', 'refresh');

			} else {
				$this->session->set_flashdata('error', 'This category already Exist please try another');
				redirect('AOdSmIiZn/categorylist', 'refresh');
			}

		}

	}
	function view_userdetails($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			//$this->admin_model->fetchbal($id);
			$fetchdata = $this->admin_model->getuserdetails($id);
			if ($fetchdata) {
				$query = $this->admin_model->getuserdetails($id);
				foreach ($query as $row) {
					$data['id'] = $row->DiZrIeSsOu;
					$id = $row->DiZrIeSsOu;
					$data['emailid'] = $this->admin_model->userdetailemail($id);
					$data['username'] = $row->emanZrIeSsOu;
					$data['firstname'] = $row->firstname;
					$data['lastname'] = $row->lastname;
					$data['phonenumber'] = $row->phonenumber;
					$emailid = $this->admin_model->userdetailemail($id);
					$data['profilepicture'] = $row->profilepicture;
					$data['dateofreg'] = $row->dateofreg;
					$data['status'] = $row->status;
					$data['type'] = $row->type;
					$data['TFT'] = $row->TFT;
					$data['last_login'] = $row->last_login;
					$data['last_logout'] = $row->last_out;
					$data['Tfa_status'] = $row->Tfa_status;
					$data['doc_verify_status'] = $row->doc_verify_status;
					$data['country'] = $this->admin_model->fetchparticularcountry($row->country);
					$data['strt1'] = $row->strt1;
					$data['strt2'] = $row->strt2;
					$data['city'] = $row->city;
					$data['postal_code'] = $row->postal_code;
					$data['state'] = $this->admin_model->fetchparticularstate($row->state);
					$data['p_country'] = $this->admin_model->fetchparticularcountry($row->p_country);
					$data['p_strt1'] = $row->p_strt1;
					$data['p_strt2'] = $row->p_strt2;
					$data['p_city'] = $row->p_city;
					$data['p_postal_code'] = $row->p_postal_code;
					$data['p_state'] = $this->admin_model->fetchparticularstate($row->p_state);

				}
				$data['currencylist'] = $this->admin_model->get_active_currencies();
				$data['view'] = "View details";
				$this->load->view('admin/customeraccounts_details', $data);
			}
		}
	}

	function pages() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		$subId = $this->session->userdata('subId');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} elseif ($ad_id != "") {
			$perpage = $this->admin_model->getrowsperpage();
			$urisegment = $this->uri->segment(3);
			$result = $this->admin_model->getpage_details($perpage, $urisegment);
			if (!$result) {
				$data['notfound'] = "No Pages Found";
				$data['view'] = "View details";
				$this->load->view('admin/pages', $data);
			} else {
				$data['found'] = "Pages Found";
				$perpage = $this->admin_model->getrowsperpage();
				$urisegment = $this->uri->segment(3);
				$total_rows = $this->admin_model->getpagescount();
				$base = "pages";
				//$this->pageconfig($total_rows,$base);
				$data['result'] = $this->admin_model->getpage_details($perpage, $urisegment);
				$data['view'] = "View details";
				$data['langcms'] = "English";
				$this->load->view('admin/pages', $data);
			}
		} elseif ($subId != "") {
			$getsubadmindetail = $this->admin_model->getsubadmin_details();
			if ($getsubadmindetail) {
				foreach ($getsubadmindetail as $permision) {
					$permission = $permision->permission;
				}
			}
			$cats = explode(",", $permission);
			$inc = 0;
			foreach ($cats as $sidebar) {
				if ($sidebar == 12) {
					$perpage = $this->admin_model->getrowsperpage();
					$urisegment = $this->uri->segment(3);
					$result = $this->admin_model->getpage_details($perpage, $urisegment);
					if (!$result) {
						$data['notfound'] = "No Pages Found";
						$data['view'] = "View details";
						$this->load->view('admin/pages', $data);
					} else {
						$data['found'] = " Pages Found";
						$perpage = $this->admin_model->getrowsperpage();
						$urisegment = $this->uri->segment(3);
						$total_rows = $this->admin_model->getpagescount();
						$base = "pages";
						//$this->pageconfig($total_rows,$base);
						$data['result'] = $this->admin_model->getpage_details($perpage, $urisegment);
						$data['view'] = "View details";
						$data['langcms'] = "English";
						$this->load->view('admin/pages', $data);
					}
					$inc++;
				}
			}
			if ($inc == 0) {
				redirect('AOdSmIiZn/subadminlogin', 'refresh');
			}
		}
	}

	function editpage_before($id) {

		$res['each_pay'] = $this->admin_model->get_each_pagex($id);

		$this->load->view('admin/editpage_before', $res);
	}

	function page_submit() {

		$pageid = $this->input->post('pageid');
		if ($pageid == '') {

			$this->admin_model->insert_mycontents();

			$this->session->set_flashdata('success', 'Page contents Inserted successfully');

			redirect("AOdSmIiZn/pages");
		}
		$this->admin_model->update_mycontents();

		$this->session->set_flashdata('success', 'Page contents updated successfully');

		redirect("AOdSmIiZn/pages");
	}

	function page_changestatus($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$data['admin_logged'] = $this->session->userdata('loggeduser');
			if ($this->admin_model->changestatus_page($id)) {
				$query = $this->admin_model->getpage($id);
				foreach ($query as $row) {
					$data['pageid'] = $row->pageid;
					$data['title'] = $row->pagetitle;
					$data['content'] = $row->pagecontent;
					$data['description'] = $row->description;
					$data['displaytitle'] = $row->displaytitle;
					$language_id = $row->language_id;
					$status = $row->status;
					$title = $row->displaytitle;
				}
				$this->session->set_flashdata('success', $title . " is  " . $status . "  now");
				redirect('AOdSmIiZn/pages', 'referesh');

			}
		}
	}

	function delete_page($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {

			$queryy = $this->admin_model->getpage($id);
			foreach ($queryy as $row) {
				$lang_idd = $row->language_id;
			}
			if ($this->admin_model->remove_pagedetails($id)) {
				$this->session->set_flashdata('success', "Page has been Deleted ");
				redirect('AOdSmIiZn/pages', 'referesh');

			}
		}
	}

	function email_templates($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		$subId = $this->session->userdata('subId');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} elseif ($ad_id != "") {
			$data['result'] = $this->admin_model->email_templates($id);
			$data['view'] = "View details";
			$this->load->view('admin/admin_emailtemplates', $data);
		} elseif ($subId != "") {
			$getsubadmindetail = $this->admin_model->getsubadmin_details();
			if ($getsubadmindetail) {
				foreach ($getsubadmindetail as $permision) {
					$permission = $permision->permission;
				}
			}
			$cats = explode(",", $permission);
			$inc = 0;
			foreach ($cats as $sidebar) {
				if ($sidebar == 13) {
					$data['result'] = $this->admin_model->email_templates();
					$data['view'] = "View details";
					$this->load->view('admin/admin_emailtemplates', $data);
					$inc++;
				}
			}
			if ($inc == 0) {
				redirect('AOdSmIiZn/subadminlogin', 'refresh');
			}
		}
	}

	function edit_emailtemplates($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$this->session->set_userdata('email_template_id', $id);
			redirect('AOdSmIiZn/edit_emailtemplate', 'refresh');
		}
	}

	function edit_emailtemplate() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$id = $this->session->userdata('email_template_id');
			$fetchdata = $this->admin_model->get_emailtemplate($id);
			if ($fetchdata) {
				$query = $this->admin_model->get_emailtemplate($id);
				foreach ($query as $row) {
					$data['id'] = $row->id;
					$data['templatetitle'] = $row->title;
					$data['subject'] = $row->subject;
					$data['message'] = $row->message;
				}
			}
			$data['edit'] = "Edit";
			$this->load->view('admin/admin_emailtemplates', $data);
		}
	}

	function edit_email_template() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			if ($this->input->post('submit')) {
				$this->form_validation->set_rules('subject', 'Subject', 'required');
				$this->form_validation->set_rules('message', 'Message', 'required');
				$this->form_validation->set_rules('templatetitle', 'templatetitle', 'required');
				//$this->form_validation->set_rules('refid','Reference ID', 'required');
				$this->form_validation->set_message('required', "%s required");
				if ($this->form_validation->run() == FALSE) {
					$id = $this->input->post('id');
					$query = $this->admin_model->get_emailtemplate($id);
					foreach ($query as $row) {
						$data['id'] = $row->id;
						$data['templatetitle'] = $row->title;
						$data['subject'] = $row->subject;
						$data['message'] = $row->message;
					}
					$data['edit'] = "Edit";
					$this->load->view('admin/admin_emailtemplates', $data);
				} else {
					$id = $this->input->post('id');
					$title = $this->input->post('templatetitle');
					if ($this->admin_model->edit_emailtemplate($id)) {
						$this->session->set_flashdata('success', "Details of Email Template " . $title . " Has been Edited Successfully");
						redirect('AOdSmIiZn/email_templates/1', 'referesh');
					}
				}
			}
		}
	}

	//chart

	function chart_data() {

		$end_date = date("Y-m-d H:i:s");
		$start_date = date('Y-m-d H:i:s', strtotime($end_date . ' - 24 hours'));
		$start = strtotime($start_date);
		$end = strtotime($end_date);
		$interval = 0.5;

		$int = 60 * 60 * $interval;
		for ($i = $start; $i <= $end; $i += $int) {

			$test[] = date('d-m-Y H:i:s', $i);
		}
		$chart = "";
		foreach ($test as $taken) {
			//echo $taken;
			$exp = explode(' ', $taken);
			$curdate = $exp[0];
			$time = $exp[1];
			// echo $taken;
			$datetime = strtotime($taken) * 1000;
			//echo $datetime = $curdate." ".$time;*
			//echo $date = 1147651200000;
			//1147651200000
			//echo $tidme = date("m/d/Y h:i:s",$date);

			$value = $this->admin_model->get_chart_details($taken, $interval);

			if ($value == '') {$value = 0;}

			$chart .= '[' . $datetime . ',' . $value . '],';

		}

		echo "[" . trim($chart, ",") . "]";

	}

	function chart() {

		$this->load->view('admin/chart');
	}
	function chart_dataone() {

		$this->load->view('admin/chart_dataone');
	}

	function chart_datayearly() {

		$this->load->view('admin/yearchart');
	}
	function chart_datamonth() {

		$this->load->view('admin/monthdata');
	}

	function chart_data1() {

		$end_date = date("Y-m-d H:i:s");
		$start_date = date('Y-m-d H:i:s', strtotime($end_date . ' - 24 hours'));
		$start = strtotime($start_date);
		$end = strtotime($end_date);
		$interval = 0.5;

		$int = 60 * 60 * $interval;
		for ($i = $start; $i <= $end; $i += $int) {

			$test[] = date('d-m-Y H:i:s', $i);
		}
		$chart = "";
		foreach ($test as $taken) {
			//echo $taken;
			$exp = explode(' ', $taken);
			$curdate = $exp[0];
			$time = $exp[1];
			// echo $taken;
			$datetime = strtotime($taken) * 1000;
			//echo $datetime = $curdate." ".$time;*
			//echo $date = 1147651200000;
			//1147651200000
			//echo $tidme = date("m/d/Y h:i:s",$date);

			$value = $this->admin_model->get_chart_details1($taken, $interval);

			if ($value == '') {$value = 0;}

			$chart .= '[' . $datetime . ',' . $value . '],';
		}
		echo "[" . trim($chart, ",") . "]";
	}
	function admin_forgetpswd() {
		$admin_url = site_config()->admin_url;
		$result = $this->admin_model->get_forgetpswd();
		if (!$result) {
			$this->session->set_flashdata('error', 'Sorry Wrong Email');
			redirect("$admin_url/forgot");
		} else {
			$this->session->set_flashdata('success', 'Successfully reseted password please check your email');
			redirect("$admin_url/forgot");
		}
	}

	function admin_pattern() {

		$admin_url = site_config()->admin_url;
		$result = $this->admin_model->admin_pattern();
		if (!$result) {
			$this->session->set_flashdata('error', 'Sorry Wrong Email');
			redirect("$admin_url/pattern");
		} else {
			$this->session->set_flashdata('success', 'Successfully sent your pattern to your mail please check your email');
			redirect("$admin_url/pattern");
		}
	}

	function international_withdraw() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		$subId = $this->session->userdata('subId');
		if ($sessionvar == "" && $subId == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
//echo "sdksd"; exit;
			$where1 = "";
			$arrorder = array('withdraw_id' => 'desc');
			$data['result'] = $this->user_model->get_data('international_withdraw', '', '', $arrorder, '', '', 'result', $where1);
			// $data['type']="Processing";
			//print_r($data); exit;
			$data['view'] = "View";
			$this->load->view('admin/international_withdraw', $data);
		}

	}

	function viewwithdraw_details($id) {
		$data['row'] = $this->db->query("select * from international_withdraw where withdraw_id='$id'")->row();
		$data['display'] = "display";
		$this->load->view('admin/international_withdraw', $data);
	}
	function request_confirm($id) {

		$result = $this->admin_model->request_confirm($id);
		$this->session->set_flashdata('success', "request is confirmed");
		redirect('AOdSmIiZn/international_withdraw', 'refresh');
	}

	function bank_verification() {
		$data['result'] = $this->db->query("select * from noitZaIcSiOfZiIrSeOvZkInSaOb where  account_number !='' ORDER BY bank_id DESC")->result();
		$data['view'] = "view";
		$this->load->view('admin/bank_verification', $data);
	}
	function bank_verification_view($id) {
		$data['row'] = $this->db->query("select * from noitZaIcSiOfZiIrSeOvZkInSaOb where bank_id=$id")->row();
		$data['display'] = "display";
		$this->load->view('admin/bank_verification', $data);
	}

	function bank_confirm($id, $status, $user_id) {
		$result = $this->admin_model->bank_confirm($id, $status, $user_id);
		if ($result) {
			if ($status == "verified") {
				$this->session->set_flashdata('success', 'Bank Verification Approved Successfully');
				redirect('AOdSmIiZn/bank_verification');
			} else {
				$this->session->set_flashdata('success', 'The rejected reason should be sent mail to user');
				redirect('AOdSmIiZn/bank_verification');
			}
		} else {
			$this->session->set_flashdata('error', 'Some error Occured');
			redirect('AOdSmIiZn/bank_verification');
		}
	}

	function ticketview() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		$subId = $this->session->userdata('subId');
		if ($subId == "" && $sessionvar == '') {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$data['view'] = "View";
			$where1 = "parent_id IS NULL";
			$arrorder = array('id' => 'desc');
			$data['result'] = $this->user_model->get_data('liateZdItSrOoZpIpSuOs', $where1, '', $arrorder, '', '', 'result');
			$this->load->view('admin/admin_ticket', $data);
		}

	}

	function view_ticket_detail($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');

		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			if (!$this->input->post("post_ticket_reply")) {

				$fetchdata = $this->admin_model->get_ticketdetail($id);

				if ($fetchdata) {

					$data1 = array('status' => 'read');
					$this->db->where('id', $id);
					$res = $this->db->update('liateZdItSrOoZpIpSuOs', $data1);
					$data['result'] = $this->admin_model->get_ticketdetail($id);
					$data['detailview'] = "detailview";
					$this->load->view('admin/admin_ticket', $data);
				}
			} else {
				$this->form_validation->set_rules('message', 'messgae', 'required');
				if ($this->form_validation->run() != FALSE) {

					if ($this->admin_model->reply_to_ticket($id)) {

						$where1 = "id=" . $id;
						$data1 = array('status' => 'replied');
						$this->db->where("id", $id);
						$res = $this->db->update('liateZdItSrOoZpIpSuOs', $data1);
						redirect('AOdSmIiZn/ticketview', 'refresh');
					}
				} else {

					unset($_POST);
					$this->view_ticket_detail($id);
				}
			}
		}
	}

	function delete_ticket($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {

			$res = $this->db->delete('liateZdItSrOoZpIpSuOs', array('id' => $id));
			if ($res) {
				$this->session->set_flashdata('success', "Ticket has been deleted");
				redirect('AOdSmIiZn/ticketview', 'referesh');
			}
		}
	}

	function currency() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$data['result'] = $this->admin_model->get_active_currencies1();
			$this->load->view('admin/currency', $data);
		}
	}

	function currency_details($id = 4) {
		$data['row'] = $this->admin_model->currency_details($id);
		$this->load->view('admin/currency_details', $data);
	}
	function currency_submit() {
		$result = $this->admin_model->change_currency();
		if (!$result) {
			$this->session->set_flashdata('error', 'Sorry Wrong Password details');
			redirect('AOdSmIiZn/currency');
		} else {
			$this->session->set_flashdata('success', ' Details has been Edited Successfully');
			redirect('AOdSmIiZn/currency');
		}
	}

	function manage() {
		$this->load->view("admin/manage");
	}

	function exchange() {
		$data['list'] = array_reverse($this->admin_model->exchange_list());
		$this->load->view("admin/exchange", $data);
	}
	function exchange_view($order_id) {
		$data['detail'] = $this->admin_model->exchange_view($order_id);
		$this->load->view("admin/exchange_view", $data);
	}

	function exchange_accept() {

		$req_id = $this->session->userdata('ex_id');
		$content = $this->session->userdata('content');
		$stat = $this->session->userdata('stat');
		$this->db->where("order_id", $req_id);
		$data = $this->db->get("redroZeIgSnOaZhIcSxOe");
		$datas = $data->row();

		$amount = $datas->amount;
		$amount1 = $datas->askprice;
		$ex_user = $datas->DiZrIeSsOu;

		if ($stat == "accept") {
			$secondCurrency = $datas->t_currency;
			$firstCurrency = $datas->f_currency;
			$admin_blnce = get_admin_blnce($secondCurrency);
			if ($admin_blnce > $datas->price) {
				$admin_new_blnce = $admin_blnce - $datas->price;
				$this->admin_model->update_admin_blnce($secondCurrency, $admin_new_blnce);
				$admin_blnce1 = get_admin_blnce($secondCurrency);
				$admin_new_blnce1 = $admin_blnce1 + $datas->fees;
				$this->admin_model->update_admin_blnce($secondCurrency, $admin_new_blnce1);
				$date = date('Y-m-d');
				$time = date("h:i:s");
				$data = array(
					'DiZrIeSsOu' => $ex_user,
					'feeAmount' => $datas->fees,
					'feeCurrency' => $secondCurrency,
					'description' => 'exchange fee',
					'date' => $date,
					'time' => $time,
				);
				$this->db->insert('eefZnIiSoOc', $data);
				$pr = '-' . $datas->price;
				$data = array(
					'DiZrIeSsOu' => $ex_user,
					'feeAmount' => $pr,
					'feeCurrency' => $secondCurrency,
					'description' => 'exchange',
					'date' => $date,
					'time' => $time,
				);
				$this->db->insert('eefZnIiSoOc', $data);

				$mydetails = $this->db->get_where('SliIaStOeZdIrSeOsu', array('DiZrIeSsOu' => $ex_user))->row_array();
				if ($mydetails['refered_id'] != '') {
					$referral_credit_amount = $this->user_model->get_data('giZfInSoOcZeItSiOs', '', '', '', '', '', 'row', '', 'referral_credit_amount_exchange');

					$income = $datas->amount * ($referral_credit_amount / 100);
					$myFriendDetails = $this->db->get_where('SliIaStOeZdIrSeOsu', array('refer_id' => $mydetails['refered_id']))->row_array();

					$current_date = date('Y-m-d');
					$current_time = date('H:i A');

					$transdata = array(
						'DiZrIeSsOu' => $myFriendDetails['DiZrIeSsOu'],
						'type' => "Refer Income",
						'currency' => $firstCurrency,
						'token' => "",
						'amount' => $income,
						'user_id_ref' => $mydetails['DiZrIeSsOu'],
						//'askamount'=>$askamount,
						'date' => $current_date,
						'time' => $current_time,
						'status' => 'active',
					);
					$this->db->insert('yrotsiZhInSoOiZtIcSaOsZnIaSrOt', $transdata);

					$creditData = array('DiZrIeSsOu' => $myFriendDetails['DiZrIeSsOu'], 'friend_id' => $mydetails['DiZrIeSsOu'], 'credit_amount' => $income, 'credited_on' => date('Y-m-d H:i:s'), 'trade_id' => '', 'currency' => $firstCurrency);
					$this->db->insert('stiZdIeSrOcZlIaSrOrZeIfSeOr', $creditData);
					$friendbalance = $this->user_model->fetchuserbalancebyId($myFriendDetails['DiZrIeSsOu'], $firstCurrency);
					$this->db->where('DiZrIeSsOu', $myFriendDetails['DiZrIeSsOu']); //INR
					$this->db->update('ecnZaIlSaObZrIeSsOuZnIiSoOc', array($firstCurrency => ((float) $friendbalance + (float) $income)));
				}
				$user_id = $datas->DiZrIeSsOu;
				$this->db->where("DiZrIeSsOu", $user_id);
				$userdetails = $this->db->get("SliIaStOeZdIrSeOsu");
				$userde = $userdetails->row();
				$email = $this->user_model->get_email_admin($user_id);
				//$email=$userde->dilZiIaSmOe;
				$name = $userde->emanZrIeSsOu;
				$this->db->where("DiZrIeSsOu", $user_id);
				$blnc = $this->db->get('ecnZaIlSaObZrIeSsOuZnIiSoOc');
				$user_blnc = $blnc->row();
				$cur = $datas->t_currency;
				$o_blnce = $user_blnc->$cur;
				$date = date('Y-m-d');
				$time = date("h:i:s");
				$ip = $_SERVER['SERVER_ADDR'];
				$this->input->post('mail_content');
				$price = $datas->price;

				$status = "completed";
				$message = "Amount Transfered Successfully";

				$dis_get_email_info = $this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='5'")->row();

				$fee = $datas->askprice - $datas->price;
				$msg = $dis_get_email_info->message;
				$subject = $dis_get_email_info->subject;
				$msg = str_replace("##USERNAME##", $name, $msg);
				$msg = str_replace("##AMOUNT##", $amount, $msg);
				$msg = str_replace("##FCUR##", $datas->f_currency, $msg);
				$msg = str_replace("##SCUR##", $datas->t_currency, $msg);
				$msg = str_replace("##PRICE##", $datas->price, $msg);
				$msg = str_replace("##FEE##", $fee, $msg);
				$msg = str_replace("##IP##", $ip, $msg);
				$admin_email = admin_email();
				$this->user_model->mailsettings();
				$this->email->from(admin_email());
				$this->email->to($email);
				$this->email->subject($subject);
				$this->email->message($msg);
				$this->email->send();
				$this->db->where('order_id', $req_id);
				$this->db->update('redroZeIgSnOaZhIcSxOe', array('status' => $status));
				$this->db->where('orderId', $req_id);
				$this->db->update('yrotsiZhInSoOiZtIcSaOsZnIaSrOt', array('status' => $status));
				$n_balance = $price + $o_blnce;

				$this->db->where("DiZrIeSsOu", $user_id);
				$this->db->update('ecnZaIlSaObZrIeSsOuZnIiSoOc', array($cur => $n_balance));
			} else {
				$message1 = "Please Update your reserve amount";
			}
		} else {
			$cur = $datas->f_currency;
			$user_id = $datas->DiZrIeSsOu;
			$this->db->where("DiZrIeSsOu", $user_id);
			$userdetails = $this->db->get("SliIaStOeZdIrSeOsu");
			$userde = $userdetails->row();
			$email = $this->user_model->get_email_admin($user_id);
			//$email=$userde->dilZiIaSmOe;
			$name = $userde->emanZrIeSsOu;
			$this->db->where("DiZrIeSsOu", $user_id);
			$blnc = $this->db->get('ecnZaIlSaObZrIeSsOuZnIiSoOc');
			$user_blnc = $blnc->row();

			$o_blnce = $user_blnc->$cur;
			$date = date('Y-m-d');
			$time = date("h:i:s");
			$ip = $_SERVER['SERVER_ADDR'];
			$this->input->post('mail_content');
			$dis_get_email_info = $this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='6'")->row();

			$fee = $datas->askprice - $datas->price;
			$msg = $dis_get_email_info->message;
			$subject = $dis_get_email_info->subject;
			$msg = str_replace("##PRICE##", $datas->amount, $msg);
			$msg = str_replace("##FCUR##", $datas->f_currency, $msg);
			$msg = str_replace("##REASON##", $content, $msg);
			$msg = str_replace("##IP##", $ip, $msg);
			$admin_email = admin_email();
			$this->user_model->mailsettings();
			$this->email->from(admin_email());
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->message($msg);
			$this->email->send();
			$cur = $datas->f_currency;
			$price = $datas->amount;
			$status = "rejected";
			$message = "Successfully Rejected";
			$this->db->where('order_id', $req_id);
			$this->db->update('redroZeIgSnOaZhIcSxOe', array('status' => $status));
			$this->db->where('orderId', $req_id);
			$this->db->update('yrotsiZhInSoOiZtIcSaOsZnIaSrOt', array('status' => $status));
			$n_balance = $price + $o_blnce;

			$this->db->where("DiZrIeSsOu", $user_id);
			$this->db->update('ecnZaIlSaObZrIeSsOuZnIiSoOc', array($cur => $n_balance));
		}

		if ($message != "") {
			$this->session->set_flashdata('success', $message);
			redirect('AOdSmIiZn/exchange', 'refresh');
		} else {
			$this->session->set_flashdata('error', $message1);
			redirect('AOdSmIiZn/exchange', 'refresh');
		}
	}

	function exchange_accept1() {

		$this->session->set_userdata('ex_id', $this->uri->segment('3'));
		$this->session->set_userdata('stat', $this->uri->segment('4'));
		$this->session->set_userdata('content', $this->input->post('mail_content'));

		redirect('AOdSmIiZn/exchange_accept', 'refresh');
	}
	function change_stat_bro($id) {
		$refer_id = substr(str_shuffle(str_repeat('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890', 5)), 0, 32);
		$this->db->where("DiZrIeSsOu", $id);
		$this->db->update("SliIaStOeZdIrSeOsu", array("bro_status" => 'verified', 'type' => 'broker', 'refer_id' => $refer_id));
		$userdetails = $this->user_model->userdetails($id);
		$dis_get_email_info = $this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='9'")->row();
		$email = $this->user_model->get_email_admin($id);
		$ip = $_SERVER['SERVER_ADDR'];
		$name = $userdetails->emanZrIeSsOu;
		$client_id = $userdetails->client_id;
		$msg = $dis_get_email_info->message;
		$subject = $dis_get_email_info->subject;
		$msg = str_replace("##USERNAME##", $name, $msg);
		$msg = str_replace("##COMPANYNAME##", "Bitunio", $msg);
		$msg = str_replace("##ID##", $client_id, $msg);
		$msg = str_replace("##IP##", $ip, $msg);
		$admin_email = admin_email();
		$this->user_model->mailsettings();
		$this->email->from(admin_email());
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($msg);
		$this->email->send();
		$this->session->set_flashdata('success', 'Successfully changed individual user to Broker');
		redirect('AOdSmIiZn/broker_chang');
	}
	function reject_stat_bro($id) {
		$this->db->where("DiZrIeSsOu", $id);
		$this->db->update("SliIaStOeZdIrSeOsu", array("bro_status" => 'rejected'));
		$userdetails = $this->user_model->userdetails($id);
		$dis_get_email_info = $this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='10'")->row();
		$email = $this->user_model->get_email_admin($id);
		$ip = $_SERVER['SERVER_ADDR'];
		$name = $userdetails->emanZrIeSsOu;
		$client_id = $userdetails->client_id;
		$msg = $dis_get_email_info->message;
		$subject = $dis_get_email_info->subject;
		$msg = str_replace("##USERNAME##", $name, $msg);
		$msg = str_replace("##COMPANYNAME##", "Bitunio", $msg);
		$admin_email = admin_email();
		$this->user_model->mailsettings();
		$this->email->from(admin_email());
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($msg);
		$this->email->send();
		$this->session->set_flashdata('success', 'Successfully Rejected Broker request');
		redirect('AOdSmIiZn/broker_chang');

	}

	function edit_exchangepair($id) {

		$fet['each_curr'] = $this->admin_model->each_exchangepair($id);
		$this->load->view('admin/edit_exchangepair', $fet);

	}
	function edit_exchnagepair_updte() {

		$ex_id = $this->input->post('ex_id');

		$this->admin_model->update_exchangepair();

		$this->session->set_flashdata('success', 'Exchange pair details Updated Successfully');
		redirect('AOdSmIiZn/exchangepairlist', 'refresh');
	}
	function blocklist() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index');
		} else {
			$data['blocklist'] = $this->admin_model->blocklist();
			$this->load->view('admin/blocklist', $data);
		}
	}

	function remove_block($id) {
		$data = array("status" => 'active', "count" => '0');
		$this->db->where("DiZrIeSsOu", $id);
		$this->db->update("SliIaStOeZdIrSeOsu", $data);
		$this->session->set_flashdata('success', 'Successfully activated blocked user');
		redirect('AOdSmIiZn/blocklist', 'refresh');
	}

	function banklist() {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index');
		} else {
			$data['banklist'] = $this->admin_model->banklist();
			$this->load->view('admin/banklist', $data);
		}
	}

	function subadmin($mode, $id = "") {

		ini_set('display_errors', "1");
		$this->load->library("parser");
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('admin/index', 'refresh');
		}
		switch ($mode) {
		case "add":
			$data['add'] = "add";
			break;
		case "view":
			$data['view'] = "View";
			$data['result'] = get_data("egZaInSaOm", array("id !=" => "1"))->result();
			break;
		case "edit":
			if (!$id) {
				die("Id is missing");
			}

			if ($this->input->post("update")) {
				$idata = $this->input->post();
				$idata["permission"] = implode(',', $idata["permission"]);
				unset($idata["update"]);
				if (update_data("egZaInSaOm", $idata, array("id" => $id))) {
					$this->session->set_flashdata('success', "Sub admin information has been updated successfully");
				} else {
					$this->session->set_flashdata('error', "Sub admin information has not been updated");
				}

				redirect("AOdSmIiZn/subadmin/view");
			} else {
				$data['edit'] = "edit";
				$udata = get_data("egZaInSaOm", array("id" => $id))->row_array();
				$data = array_merge($data, $udata);
			}
			break;
		}
		//print_r($data); exit;
		$this->load->view('admin/subadmins', $data);
	}

	function checkusername($str) {

		echo $row = count_fun('egZaInSaOm', array('emanZrIeSsOu' => $str));
	}

	function add_subadmin() {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			if ($this->input->post('submit')) {

				if ($this->input->post('ft')) {
					if ($this->input->post('permission')) {
						$this->form_validation->set_rules('sub_username', 'User Name', 'required|is_unique[egZaInSaOm.emanZrIeSsOu]');
						$this->form_validation->set_rules('sub_emailid', 'Email Id', 'required|valid_email|is_unique[egZaInSaOm.dilZiIaSmOe]');
						$this->form_validation->set_rules('sub_password', 'Password', 'required'); //$this->form_validation->set_rules('permission','Permission', 'required');
						$this->form_validation->set_rules('ft', 'ft', 'required');
						$this->form_validation->set_message('required', "%s Required");
						$this->form_validation->set_message('valid_email', "%s Should be valid");
						$this->form_validation->set_message('is_unique', "%s Already Exist");
						if ($this->form_validation->run() == FALSE) {
							$data['add'] = "add ";
							$this->load->view('admin/subadmins', $data);
						} else {
							$queryy = $this->admin_model->add_subadmindetails();
							if ($queryy) {
								$this->session->set_flashdata('success', "Sub Admin has been added");

							} else {
								$this->session->set_flashdata('error', "Sub Admin has not been added");
							}
							redirect("AOdSmIiZn/subadmin/view");
						}

					} else {
						$this->session->set_flashdata('error', "Please select anyone permission");
						redirect("AOdSmIiZn/add_subadmin");
					}
				} else {
					$this->session->set_flashdata('error', "Please select pattern to proceed");
					redirect("AOdSmIiZn/add_subadmin");
				}
			} else {
				$data['add'] = "add ";
				$this->load->view('admin/subadmins', $data);
			}
		}
	}//End of Function
	
	
	//By Mujeeb
	function refral()
	{
		$res['refrallist'] = array_reverse($this->admin_model->get_refrallist());
		
		$this->load->view('admin/refral', $res);
	}//End of Function
	
	//By Mujeeb
	function editrefral_before($id) {
	
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
	
			$res['each_pay'] = $this->admin_model->get_each_refral($id);
			$this->load->view('admin/editrefral_before', $res);
		}
	}//End of Function
	
	//By Mujeeb Refral Save
	function refral_submit() {
		//echo $this->input->post('language'); exit;
	
		//if ($_FILES['img']['name'] != "" && $this->input->post('message') != "") {
		$st = $this->admin_model->insert_refral();
		if($st==true)
		{	
			//$this->session->set_flashdata('success', $st);
			$this->session->set_flashdata('success', 'Referal Activated Successfully');
		} 
		else 
		{
			$this->session->set_flashdata('error', 'Please fill all fields');
			//$this->session->set_flashdata('error', $st);
		}
		redirect("AOdSmIiZn/refral");
	}//End of Mujeeb
	
	

	function testimonial() {

		$res['testlist'] = array_reverse($this->admin_model->get_testlist());

		$this->load->view('admin/testimonials', $res);
	}

	function test_changestatus($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {

			$this->admin_model->update_mytest_status($id);

			$this->session->set_flashdata('success', 'Testimonials Status updated successfully');

			redirect("AOdSmIiZn/testimonial");
		}
	}

	function edittest_before($id) {

		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {

			$res['each_pay'] = $this->admin_model->get_each_testi($id);
			$this->load->view('admin/edittest_before', $res);
		}
	}

	function testi_submit() {
		//echo $this->input->post('language'); exit;

		if ($_FILES['img']['name'] != "" && $this->input->post('message') != "") {
			$this->admin_model->update_mytesti();
			$this->session->set_flashdata('success', 'Testimonials updated successfully');
		} else {
			$this->session->set_flashdata('error', 'Please fill all fields and upload image');
		}
		redirect("AOdSmIiZn/testimonial");
	}
	
	

	public function manage_contact_us() {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('admin/index', 'refresh');
		} else {
			$data['title'] = "Manage Contact Us";
			$data['view'] = "view";
			$data['result'] = $this->admin_model->getsupports();
			$this->load->view('admin/manage_contact_us', $data);

		}
	}
	function reply_message_for_support() {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$result = $this->admin_model->reply_support();
			if (!$result) {
				$this->session->set_flashdata('error', "Due to some problem you message cannot be sent right now..");
			} else {
				$this->session->set_flashdata('success', "Your reply has been sent successfully.");
			}
			redirect('AOdSmIiZn/manage_contact_us', 'refresh');
		}
	}
	function reply_con($id) {
		$data['row'] = $this->admin_model->get_support($id);
		$this->load->view("admin/reply_con", $data);
	}

	function withdraw_confirm_admin($token) {
		// echo $token;exit;
		require_once 'jsonRPCClient.php';
		$where = "token='" . $token . "'";
		$row = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
		$Userid = $row->DiZrIeSsOu;
		$Status = $row->status;
		$currency = $row->currency;
		if ($this->session->userdata('loggeduser') != "") {
			if ($Status == "cancelled" || $Status == "confirmed") {
				$this->session->set_flashdata('error', "Your withdraw request has already been confirmed or cancelled earlier");
				redirect('AOdSmIiZn/withdraw_history');
			} else {
				if ($currency == "BTC" || $currency == "DOGE" || $currency == "DASH" || $currency == "ZEC" || $currency == "LTC" || $currency == "BCH") {
					$where = "token='" . $token . "'";
					$data = array('status' => "confirmed");
					$confirmResuslt = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc', $data, $where);
					$where = "token='" . $token . "'";
					$taken = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
					$amount = $taken->amount;
					$askamount = $taken->askamount;
					$purse = $taken->purse;
					$currency = $taken->currency;
					$btc_amount = $amount;
					$fee = $askamount - $amount;

					$bitcoin_row = $this->user_model->fetchWallet($currency);
					$bitcoin_username = $bitcoin_row->username;
					$bitcoin_password = $bitcoin_row->password;
					$bitcoin_portnumber = $bitcoin_row->portnumber;
					//$bitcoin = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@54.254.170.114:$bitcoin_portnumber/");
					$bitcoin = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@35.177.165.200:$bitcoin_portnumber/");
					//$info = $bitcoin->getinfo();//Deprecated
					$info = $bitcoin->getwalletinfo();//By Mujeeb
					
					$w_bal = $info['balance'];
					$amount = number_format($amount, 8);
					if ($w_bal >= $amount) {
						$isvalid = $bitcoin->sendtoaddress($purse, (float) $amount);
					} else {
						$isvalid = "";
					}

					if (!isset($isvalid)) {
						$this->session->set_flashdata('error', "Sorry your withdraw tranaction failed. Please check your available balance or contact our support team.");
						redirect('AOdSmIiZn/withdraw_history');
					} else {

						$date = date('Y-m-d');
						$time = date("h:i:s");
						$data2 = array(
							'DiZrIeSsOu' => $Userid,
							'feeAmount' => $fee,
							'feeCurrency' => $currency,
							'description' => "Withdraw",
							'date' => $date,
							'time' => $time,
						);
						$res = $this->db->insert('eefZnIiSoOc', $data2);
						$where = "token='" . $token . "'";
						$data = array('trans_id' => $isvalid, 'status' => 'confirmed');
						$result = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc', $data, $where);
						$where = "token='" . $token . "'";
						$data = array('trans_id' => $isvalid, 'status' => 'confirmed');
						$result1 = $this->user_model->update_data('yrotsiZhInSoOiZtIcSaOsZnIaSrOt', $data, $where);
						if ($result1) {
							$this->session->set_flashdata('success', "Successfully Approved crypto currency Withdraw request");
							redirect('AOdSmIiZn/withdraw_history');
						}
					}
				} else if ($currency == "XMR") {

					$where = "token='" . $token . "'";
					$taken = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
					$amount = $taken->amount;
					$askamount = $taken->askamount;
					$purse = $taken->purse;
					$currency = $taken->currency;
					$btc_amount = $amount;
					$payment_id = $taken->tag;
					$fee = $askamount - $amount;

					$new_amount = $amount;
					//$new_amount = $amount  * 1000000000000;
					$destinations = array('amount' => $new_amount, 'address' => $purse);
					$transfer_parameters = array('destinations' => array($destinations), 'mixin' => 4, 'get_tx_key' => true, 'unlock_time' => 0, 'payment_id' => '');

					$isvalid = $this->monero_wallet_model->transfer_xmr($purse, $new_amount, $payment_id);
					if (!isset($isvalid)) {
						$this->session->set_flashdata('error', "Sorry your withdraw tranaction failed. Please check your available balance or contact our support team.");
						redirect('AOdSmIiZn/withdraw_history');
					} else {

						$date = date('Y-m-d');
						$time = date("h:i:s");
						$data2 = array(
							'DiZrIeSsOu' => $Userid,
							'feeAmount' => $fee,
							'feeCurrency' => $currency,
							'description' => "Withdraw",
							'date' => $date,
							'time' => $time,
						);
						$res = $this->db->insert('eefZnIiSoOc', $data2);
						$where = "token='" . $token . "'";
						$data = array('trans_id' => $isvalid, 'status' => 'confirmed');
						$where = "token='" . $token . "'";
						$result = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc', $data, $where);
						$data = array('trans_id' => $isvalid, 'status' => 'confirmed');
						$result1 = $this->user_model->update_data('yrotsiZhInSoOiZtIcSaOsZnIaSrOt', $data, $where);
						if ($result1) {
							$this->session->set_flashdata('success', "Successfully Approved crypto currency Withdraw request");
							redirect('AOdSmIiZn/withdraw_history');
						}
					}
				} else if ($currency == "XRP") {
					// $where = "token='".$token."'";
					// $data = array('status'=>"finished");
					// $confirmResult = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc',$data,$where);
					$where = "token='" . $token . "'";
					$taken = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
					$amount = $taken->amount;
					$askamount = $taken->askamount;
					$purse = $taken->purse;
					$tag = $taken->tag;
					$currency = $taken->currency;
					$btc_amount = $amount;
					$fee = $askamount - $amount;

					$result = $this->db->query('SELECT ripple_address as admin_address ,ripple_key as admin_key FROM giZfInSoOcZeItSiOs')->row();
					$admin_address = $result->admin_address;
					$admin_key = encrypt_decrypt(2, $result->admin_key);

					$output = shell_exec('cd /var/www/application/config/js; node ripple_balance.js "' . $admin_address . '"');

					$output1 = json_decode($output);

					foreach ($output1 as $key => $value) {
						$value = $value->value;
					}
					// echo $value;exit;
					$balance = $value - $amount;

					if ($admin_address != $purse) {
						if ($balance > 25 && (float) $balance >= (float) $amount) {
							$transaction = shell_exec('cd /var/www/application/config/js; node ripple_sendcoins.js "' . $purse . '" "' . $amount . '" "' . $admin_address . '" "' . $admin_key . '" "' . $tag . '"');
							if ($transaction) {
								$testt = explode('NaN', $transaction);
								$a = $testt[1];
								$b = $testt[2];
								$aa = json_decode($a, true);
								$txxid1 = $aa['txid'];
								$bb = json_decode($b, true);
								$messag2 = $bb['resultCode'];
								if ($messag2 == 'tesSUCCESS') {
									$isvalid = $txxid1;
								}
							}
						}
					}

					if (!isset($isvalid)) {
						$this->session->set_flashdata('error', "Sorry your withdraw tranaction failed. Please check your available balance or contact our support team.");
						redirect('AOdSmIiZn/withdraw_history');
					} else {

						$date = date('Y-m-d');
						$time = date("h:i:s");
						$data2 = array(
							'DiZrIeSsOu' => $Userid,
							'feeAmount' => $fee,
							'feeCurrency' => $currency,
							'description' => "Withdraw",
							'date' => $date,
							'time' => $time,
						);
						$res = $this->db->insert('eefZnIiSoOc', $data2);
						$where = "token='" . $token . "'";
						$data = array('trans_id' => $isvalid, 'status' => 'confirmed');
						$result = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc', $data, $where);
						$where = "token='" . $token . "'";
						$data = array('trans_id' => $isvalid, 'status' => 'confirmed');
						$result1 = $this->user_model->update_data('yrotsiZhInSoOiZtIcSaOsZnIaSrOt', $data, $where);
						if ($result1) {
							$this->session->set_flashdata('success', "Successfully Approved crypto currency Withdraw request");
							redirect('AOdSmIiZn/withdraw_history');
						}
					}

				} else if ($currency == "ETH") {
					$where = "token='" . $token . "'";
					//$data = array('status'=>"finished");
					//	$confirmResult = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc',$data,$where);
					$where = "token='" . $token . "'";
					$taken = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
					$amount = $taken->amount;
					$askamount = $taken->askamount;
					$purse = $taken->purse;
					$currency = $taken->currency;
					$btc_amount = (float) $amount;
					$fee = $askamount - $amount;

					$where = "id=1";
					$admin = $this->user_model->get_data('giZfInSoOcZeItSiOs', $where, '', '', '', '', 'row');
					$address = $admin->eth_address;
					$pass = encrypt_decrypt('2', $admin->eth_adminkey);
					$address = '"' . trim($address) . '"';

					$data = array('adminaddress' => $address);
					$balance = connecteth('checkbalance', $data);
					$balance = number_format($balance, 8);
					// echo $balance;exit;
					$to = trim($purse);

					if ((float) $balance >= (float) $btc_amount) {
						$data = array('adminaddress' => $address, 'toaddress' => $to, 'amount' => $btc_amount, 'key' => $pass);
						$isvalid = connecteth('ethwithdrawjson', $data);
						// print_R($isvalid);exit;
						if (!isset($isvalid)) {
							$this->session->set_flashdata('error', "Sorry your withdraw tranaction failed. Please check your available balance or contact our support team.");
							redirect('AOdSmIiZn/withdraw_history');
						} else {
							$date = date('Y-m-d');
							$time = date("h:i:s");
							$data2 = array(
								'DiZrIeSsOu' => $Userid,
								'feeAmount' => $fee,
								'feeCurrency' => $currency,
								'description' => "Withdraw",
								'date' => $date,
								'time' => $time,
							);
							$res = $this->db->insert('eefZnIiSoOc', $data2);
							$where = "token='" . $token . "'";
							$data = array('trans_id' => $isvalid, 'status' => 'confirmed');
							$result = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc', $data, $where);
							$where = "token='" . $token . "'";
							$data = array('trans_id' => $isvalid, 'status' => 'confirmed');
							$result1 = $this->user_model->update_data('yrotsiZhInSoOiZtIcSaOsZnIaSrOt', $data, $where);
							if ($result1) {
								$this->session->set_flashdata('success', "Successfully Approved crypto currency Withdraw request");
								redirect('AOdSmIiZn/withdraw_history');
							}
						}
					} else {
						$this->session->set_flashdata('error', "Withdraw is pending. Please contact to admin.");
					}
				} else if ($currency == "ETC") {
					$where = "token='" . $token . "'";
					//$data = array('status'=>"finished");
					//	$confirmResult = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc',$data,$where);
					$where = "token='" . $token . "'";
					$taken = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
					$amount = $taken->amount;
					$askamount = $taken->askamount;
					$purse = $taken->purse;
					$currency = $taken->currency;
					$btc_amount = (float) $amount;
					$fee = $askamount - $amount;

					$where = "id=1";
					$admin = $this->user_model->get_data('giZfInSoOcZeItSiOs', $where, '', '', '', '', 'row');
					$address = $admin->etc_address;
					$pass = $admin->etc_adminkey;
					$address = '"' . trim($address) . '"';

					$data = array('adminaddress' => $address);
					$balance = connectetc('checkbalance', $data);
					$balance = number_format($balance, 8);
					// echo $balance;exit;
					$to = trim($purse);

					if ((float) $balance >= (float) $btc_amount) {
						$data = array('adminaddress' => $address, 'toaddress' => $to, 'amount' => $btc_amount, 'key' => $pass);
						$isvalid = connectetc('ethwithdrawjson', $data);
						// print_R($isvalid);exit;
						if (!isset($isvalid)) {
							$this->session->set_flashdata('error', "Sorry your withdraw tranaction failed. Please check your available balance or contact our support team.");
							redirect('AOdSmIiZn/withdraw_history');
						} else {
							$date = date('Y-m-d');
							$time = date("h:i:s");
							$data2 = array(
								'DiZrIeSsOu' => $Userid,
								'feeAmount' => $fee,
								'feeCurrency' => $currency,
								'description' => "Withdraw",
								'date' => $date,
								'time' => $time,
							);
							$res = $this->db->insert('eefZnIiSoOc', $data2);
							$where = "token='" . $token . "'";
							$data = array('trans_id' => $isvalid, 'status' => 'confirmed');
							$result = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc', $data, $where);
							$where = "token='" . $token . "'";
							$data = array('trans_id' => $isvalid, 'status' => 'confirmed');
							$result1 = $this->user_model->update_data('yrotsiZhInSoOiZtIcSaOsZnIaSrOt', $data, $where);
							if ($result1) {
								$this->session->set_flashdata('success', "Successfully Approved crypto currency Withdraw request");
								redirect('AOdSmIiZn/withdraw_history');
							}
						}
					} else {
						$this->session->set_flashdata('error', "Withdraw is pending. Please contact to admin.");
					}
				}
			}
		} else {
			$admin_url = site_config()->admin_url;
			redirect($admin_url, 'refresh');
		}

	}

	function withdraw_cancel_admin($token) {

		// echo $token;exit;
		$where = "token='" . $token . "'";
		$row = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
		$Userid = $row->DiZrIeSsOu;
		$Status = $row->status;
		$askamount = $row->askamount;
		$currency = $row->currency;
		$customer_user_id = $this->session->userdata('user_id');
		if ($this->session->userdata('loggeduser') != "") {

			if ($Status == "cancelled" || $Status == "confirmed" || $Status == "confirmed") {
				$this->session->set_flashdata('error', "withdraw request has already been confirmed or cancelled earlier");
			} else {
				$updated = $this->user_model->updatecancellation($token, $Userid, $currency, $askamount);
				if ($updated) {
					$this->session->set_flashdata('success', "withdraw request successfully cancelled.");
				}
			}
			redirect('AOdSmIiZn/withdraw_history');
		} else {
			$admin_url = site_config()->admin_url;
			redirect($admin_url, 'refresh');

		}

	}

// xmr coin connection
	function monero_request($cmd, $postfields = null) {
		// $xmr_row=Basic::getsinglerow(array('coinname'=>"xmrcoin"),'wiix_cryptodetails');

		// $xmr_row_portnumber = 	Helper::encrypt_decrypt("decrypt",$xmr_row->portnumber);
		// $xmr_row_host 		= 	Helper::encrypt_decrypt("decrypt",$xmr_row->ipaddress);
		//print_r("hai");

		// $wallet_port = $wallet_portnumber = $xmr_row_portnumber;
		// $wallet_ip   = $wallet_allow_ip   =	$xmr_row_host;
		// $wallet_port = 18080;
		// $wallet_port = 18081;
		$wallet_port = 18082;
		$wallet_ip = "35.177.165.200";// "54.254.170.114";
		$version = "2.0";
		$id = 0;
		$url = "http://$wallet_ip:$wallet_port/json_rpc";
		$data = array();
		$data['jsonrpc'] = $version;
		$data['id'] = $id++;
		$data['method'] = $cmd;
		$data['params'] = $postfields;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_POST, count($postfields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$ret = curl_exec($ch);
		curl_close($ch);
		// print_r($ret);
		// exit;

		if ($ret !== FALSE) {
			$formatted = $this->format_response($ret);

			if (isset($formatted->error)) {
				echo $formatted->error;
			} else {
				return $formatted->result;
			}
		} else {
			echo ("Server did not respond");
		}
	}

	function format_response($response) {
		return @json_decode($response);
	}

	function news_letter() {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		if ($sessionvar == "") {
			redirect('admin/index', 'refresh');
		} elseif ($ad_id != "") {

			$fetchdata = $this->admin_model->get_news_letter();
			if ($fetchdata) {
				$query = $this->admin_model->get_news_letter();

			}
			$data['edit'] = "Edit";
			$data['result'] = $query;
			$data['title'] = "News Letter";
			$this->load->view('admin/newsletter', $data);
		}
	}
	function update_news_letter() {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			if ($this->input->post('title') != "") {

				if ($this->admin_model->update_news_letter_model()) {
					$this->session->set_flashdata('success', "E-Mail has been sent successfully");
					redirect('AOdSmIiZn/news_letter', 'referesh');
				}
			} else {
				$this->session->set_flashdata('error', "Please select any one email");
				redirect('AOdSmIiZn/news_letter', 'referesh');
			}

		}
	}
	function affiliate_users() {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		$subId = $this->session->userdata('subId');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$data['result'] = $this->db->query("select * from SliIaStOeZdIrSeOsu where type='broker'")->result();
			$data['view'] = "View Details";
			$this->load->view('admin/affiliate', $data);
		}
	}
	function refer_receive($id) {
		$sessionvar = $this->session->userdata('loggeduser');
		$data['admin_logged'] = $this->session->userdata('loggeduser');
		$ad_id = $this->session->userdata('id');
		$subId = $this->session->userdata('subId');
		if ($sessionvar == "") {
			redirect('AOdSmIiZn/index', 'refresh');
		} else {
			$data['income_his'] = $this->user_model->get_income($id);
			$data['view1'] = "View Details";
			$this->load->view('admin/affiliate', $data);
		}

	}

}
