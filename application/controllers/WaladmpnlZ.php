<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class WaladmpnlZ extends CI_Controller {
	public function __construct() {

		error_reporting(E_ERROR);
		parent::__construct();
		$this->load->database();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		header('X-Frame-Options: SAMEORIGIN');
		//header('Access-Control-Allow-Origin: true');
		header('X-XSS-Protection: 1; mode=block');
		header('X-Content-Type-Options: nosniff');
		$base = base_url();
		header("ALLOW-FROM: $base");
		header("X-Powered-By: $base");

		ini_set('session.gc_maxlifetime', 300);
		ini_set('session.cookie_httponly', 1);
		ini_set('session.use_only_cookies', 1);
		ini_set('session.cookie_secure', 1);

		if ($this->input->post('wallet_username') == '' && !$this->session->userdata('wallet_loggeduser')) {
			$uri = $_SERVER['REQUEST_URI'];
			$uriArray = explode('/', $uri);
			if (count($uriArray) > 1) {
				$page_url = $uriArray[1];
				$page_url_2 = $uriArray[2];
				if ($page_url == 'WaladmpnlZ' || $page_url_2 == 'WaladmpnlZ' || $page_url == 'WaladmpnlZ.jsp' || $page_url_2 == 'WaladmpnlZ.jsp') {
					redirect('error', 'refresh');
				}

			}
		}

		$this->admin = $this->wallet_model->get_admindetails();
	}
	////common-start
	function mail_settings() {
		$this->load->library('email');
		$config['wrapchars'] = 76;
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$this->email->initialize($config);
	}
	function index() {

		$query = $this->db->get('giZfInSoOcZeItSiOs');
		$data['admin_dir'] = $query->row()->admin_url;
		$data['company_name'] = $this->admin->company_name;

		$data['admin_logged'] = $this->session->userdata('wallet_loggeduser');
		$sessionvar = $this->session->userdata('wallet_loggeduser');
		if ($sessionvar != "") {
			$curr = $this->wallet_model->get_active_currencies1();
			foreach ($curr as $key => $value) {

				if ($value->id != '11' && $value->id != '12') {
					$data[$value->currency_symbol] = number_format(0, 8, '.', '');
					if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
						if($value->currency_symbol !='ZEC' || $value->currency_symbol !='BCH')//By Mujeeb
						{
							$balance = $this->getbalance($value->currency_symbol);
							$data[$value->currency_symbol] = number_format($balance, 8, '.', '');
						}
					}

				}
			}
			$this->db->where('type', 'Withdraw');
			$this->db->order_by('id', 'DESC');
			$query = $this->db->get('tZeIlSlOaZwIlSwOt');
			$data['withdraw'] = $query->result();

			$this->load->view('wallet/index', $data);
		} else {
			$this->form_validation->set_rules('username', 'User Name', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_message('required', "Enter %s ");
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('wallet/login', $data);
			} else {
				if ($this->input->post('username')) {
					$result = $this->wallet_model->logincheck();
					if (!$result) {
						$data = array(
							'uname' => $this->input->post('username'),
							'pwd' => $this->input->post('password'),
							'ip' => $_SERVER['REMOTE_ADDR'],
							'browser_name' => $_SERVER['HTTP_USER_AGENT'],
							'reason' => 'Invalid User name or Password or Pettern',
							'polute_date' => date('Y-m-d H:i:s'),
						);
						$this->db->insert("etZuIlSoOp", $data);

						$data['invalid'] = '<font color="#CC0000">Invalid Username or Password or Pattern</font>';
						$this->load->view('wallet/login', $data);
					} else {
						redirect('WaladmpnlZ/index', 'refresh');
					}
				}
			}
		}
	}

	function admin_change_password() {
		$data['admin_logged'] = $this->session->userdata('wallet_loggeduser');
		$sessionvar = $this->session->userdata('wallet_loggeduser');
		$subId = $this->session->userdata('subId');
		if ($subId != "") {
			redirect('WaladmpnlZ/index', 'refresh');
		}
		if ($sessionvar == "") {
			//$this->load->view('admin_login',$data);
			redirect('WaladmpnlZ/index', 'refresh');
		} else {

			$this->form_validation->set_rules('password', 'Current Password', 'required');
			$this->form_validation->set_rules('newpassword', 'New password', 'required');
			$this->form_validation->set_rules('repassword', "re-enter password", 'required|matches[newpassword]');
			$this->form_validation->set_message('required', "%s  Required");
			$this->form_validation->set_message('matches', "Missmatching Password");
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('wallet/account', $data);
			} else {
				$result = $this->wallet_model->admin_change_pswd();
				if (!$result) {
					$this->session->set_flashdata('error', 'Sorry Wrong Password details');
					redirect('WaladmpnlZ/admin_change_password');
				} else {
					$this->session->set_flashdata('success', ' Details has been Edited Successfully');
					redirect('WaladmpnlZ/admin_change_password');
				}
			}
		}
	}
	function admin_change_pattern() {

		$data['admin_logged'] = $this->session->userdata('wallet_loggeduser');
		$sessionvar = $this->session->userdata('wallet_loggeduser');
		$subId = $this->session->userdata('subId');
		if ($subId != "") {
			redirect('WaladmpnlZ/index', 'refresh');
		}
		if ($sessionvar == "") {
			//$this->load->view('admin_login',$data);
			redirect('WaladmpnlZ/index', 'refresh');
		} else {

			if ($this->input->post('submit')) {
				if ($this->input->post('ft') != '' && $this->input->post('ft1') != '') {

					$wheredata = array('id' => '1', 'name' => encrypt_decrypt(1, $this->input->post('ft')));
					$this->db->where($wheredata);
					$query = $this->db->get('nSiOgZoIlSeOrZuIcSeOS');

					if ($query->num_rows() == 0) {
						$this->session->set_flashdata('error', 'Incorrect pattern details');
						redirect('WaladmpnlZ/admin_change_pattern');
					} else {
						$this->db->where('id', '1');
						$this->db->update('nSiOgZoIlSeOrZuIcSeOS', array('name' => encrypt_decrypt(1, $this->input->post('ft1'))));

						$this->session->set_flashdata('success', ' Details has been Edited Successfully');
						redirect('WaladmpnlZ/admin_change_pattern');
					}
				} else {
					$this->session->set_flashdata('error', 'Pattern details required');
					redirect('WaladmpnlZ/admin_change_pattern');
				}
			} else {
				$this->load->view('wallet/settings', $data);

			}
		}
	}

	function forgot() {
		$query = $this->db->get('giZfInSoOcZeItSiOs');
		$data['admin_dir'] = $query->row()->admin_url;
		$this->load->view('wallet/forgot', $data);
	}

	function pattern() {
		$query = $this->db->get('giZfInSoOcZeItSiOs');
		$data['admin_dir'] = $query->row()->admin_url;
		$this->load->view('wallet/pattern', $data);
	}

	function logout() {

		$sessionvar = $this->session->userdata('wallet_loggeduser');
		$admin_url = site_config()->footer;
		if ($sessionvar == "") {
			redirect($admin_url, 'refresh');
		} else {
			$this->session->sess_destroy();
			redirect($admin_url, 'refresh');
		}
	}

	function deposit($coin = '') {

		$sessionvar = $this->session->userdata('wallet_loggeduser');
		$data['admin_logged'] = $this->session->userdata('wallet_loggeduser');
		if ($sessionvar == "") {
			redirect('WaladmpnlZ/index', 'refresh');
		} else {
			if ($coin == '') {
				$this->session->set_flashdata('error', 'Coin not exist');
				redirect('WaladmpnlZ/index', 'refresh');
			} else {
				$data['tag'] = '';
				$address = $this->wallet_model->get_deposit_addr($coin);
				if ($coin == 'XRP') {
					$data['tag'] = $address;
					$result = $this->db->query('SELECT ripple_address as address FROM giZfInSoOcZeItSiOs')->row();
					$address = $result->address;
				} else if ($coin == 'XMR') {
					$data['tag'] = 'ae9eb7deec27f97e';

					$address = '4AvVitT9VhDRncFJMktT76YoGVGDTQkkUJxbKoFNpUomiuZzoZbyfMLidNrxvEf99RUycPJAkkNBH9gLhDVAfH4DDQ3jZjj';
				}
				$this->db->where('type', 'Deposit');
				$this->db->where('currency', $coin);
				$this->db->order_by('id', 'DESC');
				$query = $this->db->get('tZeIlSlOaZwIlSwOt');
				$data['deposit'] = $query->result();

				$data['address'] = $address;
				$image_src = "https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=$address&choe=UTF-8&chld=L";
				$data['image'] = $image_src;
				$this->load->view('wallet/deposit', $data);
			}
		}
	}

	function withdraw($curr) {

		$sessionvar = $this->session->userdata('wallet_loggeduser');
		$data['admin_logged'] = $this->session->userdata('wallet_loggeduser');
		if ($sessionvar == "") {
			redirect('WaladmpnlZ/index', 'refresh');
		} else {
			if ($curr == '') {
				$this->session->set_flashdata('error', 'Coin not exist');
				redirect('WaladmpnlZ/index', 'refresh');
			} else {
				$data['coin'] = $curr;
				if ($this->input->post('save')) {
					$this->form_validation->set_rules('amount', 'Amount', 'required');
					$this->form_validation->set_rules('address', 'Address', 'required');
					if ($this->form_validation->run() == FALSE) {
						$this->session->set_flashdata('error', validation_errors());
						redirect('WaladmpnlZ/withdraw/' . $curr, 'refresh');
					} else {

						/*echo "<pre>";
							print_r($_POST);
						*/
						$balance = 0;
						$amount = $this->input->post('amount');
						$address = $this->input->post('address');
						/*echo */$tag = $this->input->post('tag') ? $this->input->post('tag') : '';
						if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
							$balance = $this->getbalance($curr);
						}
						if ($amount >= $balance) {
							$this->session->set_flashdata('error', 'Please enter the amount less than your balance');
							redirect('WaladmpnlZ/withdraw/' . $curr, 'refresh');
						}
						$token = time() . '11' . rand();
						$this->db->insert('tZeIlSlOaZwIlSwOt', array('currency' => $curr, 'amount' => $amount, 'address' => $address, 'tag' => $tag, 'type' => 'Withdraw', 'request_date' => date('Y-m-d'), 'request_time' => date('H:i:s'), 'status' => 'Pending', 'token' => $token));
						$wallet_url = site_config()->footer;
						$confirm = base_url() . $wallet_url . "/withdraw_confirm_wallet/" . $token;
						$cancel = base_url() . $wallet_url . "/withdraw_cancel_wallet/" . $token;
						$ip = $this->input->ip_address();
						$get_email_info = $this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='17'")->row();
						$bitunio = company_name();
						$msg = $get_email_info->message;
						$msg = str_replace("##COMPANYNAME##", $bitunio, $msg);
						$msg = str_replace('##AMOUNT##', $amount, $msg);
						$msg = str_replace('##CURRENCY##', $curr, $msg);
						$msg = str_replace('##PURSE##', $address, $msg);
						$msg = str_replace('##IP##', $ip, $msg);
						$msg = str_replace('##CONFIRMLINK##', $confirm, $msg);
						$msg = str_replace('##CANCELLINK##', $cancel, $msg);

						$this->user_model->mailsettings();
						$this->email->from(admin_email());
						$this->email->to(mail_admin());

						$this->email->subject("withdraw confirmation");
						$this->email->message($msg);
						$this->email->send();
						$this->session->set_flashdata('success', 'Please check your mail confirm the withdraw');
						redirect('WaladmpnlZ/withdraw/' . $curr, 'refresh');

					}
				}
				$this->db->where('type', 'Withdraw');
				$this->db->where('currency', $curr);
				$this->db->order_by('id', 'DESC');
				$query = $this->db->get('tZeIlSlOaZwIlSwOt');
				$data['withdraw'] = $query->result();
				$data['balance'] = 0;
				if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
					$data['balance'] = $this->getbalance($curr);
				}

			}

			$this->load->view('wallet/withdraw', $data);
		}
	}

	function admin_forgetpswd() {
		$admin_url = site_config()->footer;
		$result = $this->wallet_model->get_forgetpswd();
		if (!$result) {
			$this->session->set_flashdata('error', 'Sorry Wrong Email');
			redirect("$admin_url/forgot");
		} else {
			$this->session->set_flashdata('success', 'Successfully reseted password please check your email');
			redirect("$admin_url/forgot");
		}
	}

	function admin_pattern() {

		$admin_url = site_config()->footer;
		$result = $this->wallet_model->admin_pattern();
		if (!$result) {
			$this->session->set_flashdata('error', 'Sorry Wrong Email');
			redirect("$admin_url/pattern");
		} else {
			$this->session->set_flashdata('success', 'Successfully sent your pattern to your mail please check your email');
			redirect("$admin_url/pattern");
		}
	}

	function withdraw_confirm_wallet($token) {
		require_once 'jsonRPCClient.php';
		$where = "token='" . $token . "'";
		$taken = $this->user_model->get_data('tZeIlSlOaZwIlSwOt', $where, '', '', '', '', 'row');
		if ($taken) {
			$Userid = $taken->DiZrIeSsOu;
			$Status = $taken->status;
			$currency = $taken->currency;
			if ($this->session->userdata('wallet_loggeduser') != "") {
				if ($Status == "cancelled" || $Status == "confirmed") {
					$this->session->set_flashdata('error', "Your withdraw request has already been confirmed or cancelled earlier");
					redirect('WaladmpnlZ/withdraw_history');
				} else {
					if ($currency == "BTC" || $currency == "DOGE" || $currency == "DASH" || $currency == "ZEC" || $currency == "LTC" || $currency == "BCH") {

						$amount = $taken->amount;
						$purse = $taken->address;
						$currency = $taken->currency;
						$btc_amount = $amount;

						$bitcoin_row = $this->user_model->fetchWallet($currency);
						$bitcoin_username = $bitcoin_row->username;
						$bitcoin_password = $bitcoin_row->password;
						$bitcoin_portnumber = $bitcoin_row->portnumber;
						//$bitcoin = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@54.254.170.114:$bitcoin_portnumber/");
						$bitcoin = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@35.177.165.200:$bitcoin_portnumber/");
						$info = $bitcoin->getinfo();
						// print_r($info);exit;
						$w_bal = $info['balance'];
						$amount = number_format($amount, 8);
						if ($w_bal >= $amount) {
							$isvalid = $bitcoin->sendtoaddress($purse, (float) $amount);
						} else {
							$isvalid = "";
						}

						if (!isset($isvalid)) {
							$this->session->set_flashdata('error', "Sorry your withdraw tranaction failed. Please check your available balance or contact our support team.");
							redirect('WaladmpnlZ/index', 'refresh');
						} else {
							$where = "token='" . $token . "'";
							$data = array('status' => "confirmed", 'transactionId' => $isvalid);
							$confirmResuslt = $this->user_model->update_data('tZeIlSlOaZwIlSwOt', $data, $where);
							$this->session->set_flashdata('success', "Withdraw completed");
							redirect('WaladmpnlZ/index', 'refresh');
						}
					} else if ($currency == "XMR") {
						/*echo "<pre>";
							print_r($taken);
						*/
						$amount = $taken->amount;

						$purse = $taken->address;
						$currency = $taken->currency;
						$btc_amount = $amount;
						$payment_id = $taken->tag;

						//$new_amount = $amount  * 1000000000000;
						$new_amount = $amount;

						$destinations = array('amount' => $new_amount, 'address' => $purse);
						$transfer_parameters = array('destinations' => array($destinations), 'mixin' => 4, 'get_tx_key' => true, 'unlock_time' => 0, 'payment_id' => $payment_id);

						$isvalid = $this->monero_wallet_model->transfer_xmr($purse, $new_amount, $payment_id, $transfer_parameters);
						/* echo "<pre>";
							 print_r($isvalid);
							 echo "</pre>";
							 echo "<pre>";
							 print_r($_SESSION);
							 echo "</pre>";
						*/

						if (!isset($isvalid)) {
							$this->session->set_flashdata('error', "Sorry your withdraw tranaction failed. Please check your available balance or contact our support team.");
							redirect('WaladmpnlZ/index', 'refresh');
						} else {

							$where = "token='" . $token . "'";
							$data = array('status' => "confirmed", 'transactionId' => $isvalid);
							$confirmResuslt = $this->user_model->update_data('tZeIlSlOaZwIlSwOt', $data, $where);
							$this->session->set_flashdata('success', "Withdraw completed");
							redirect('WaladmpnlZ/index', 'refresh');

						}
					} else if ($currency == "XRP") {

						$amount = $taken->amount;

						$purse = $taken->address;
						$tag = $taken->tag;
						$currency = $taken->currency;
						$btc_amount = $amount;

						$result = $this->db->query('SELECT ripple_address as admin_address ,ripple_key as admin_key FROM giZfInSoOcZeItSiOs')->row();
						$admin_address = $result->admin_address;
						$admin_key = encrypt_decrypt(2, $result->admin_key);

						$output = shell_exec('cd /var/www/application/config/js; node ripple_balance.js "' . $admin_address . '"');

						$output1 = json_decode($output);

						foreach ($output1 as $key => $value) {
							$value = $value->value;
						}
						// echo $value;exit;
						$balance = $value - $amount;

						if ($admin_address != $purse) {
							if ($balance > 25 && (float) $balance >= (float) $amount) {
								$transaction = shell_exec('cd /var/www/application/config/js; node ripple_sendcoins.js "' . $purse . '" "' . $amount . '" "' . $admin_address . '" "' . $admin_key . '" "' . $tag . '"');
								if ($transaction) {
									$testt = explode('NaN', $transaction);
									$a = $testt[1];
									$b = $testt[2];
									$aa = json_decode($a, true);
									$txxid1 = $aa['txid'];
									$bb = json_decode($b, true);
									$messag2 = $bb['resultCode'];
									if ($messag2 == 'tesSUCCESS') {
										$isvalid = $txxid1;
									}
								}
							}
						}

						if (!isset($isvalid)) {
							$this->session->set_flashdata('error', "Sorry your withdraw tranaction failed. Please check your available balance or contact our support team.");
							redirect('WaladmpnlZ/index', 'refresh');
						} else {

							$where = "token='" . $token . "'";
							$data = array('status' => "confirmed", 'transactionId' => $isvalid);
							$confirmResuslt = $this->user_model->update_data('tZeIlSlOaZwIlSwOt', $data, $where);
							$this->session->set_flashdata('success', "Withdraw completed");
							redirect('WaladmpnlZ/index', 'refresh');
						}

					} else if ($currency == "ETH") {
						$where = "token='" . $token . "'";

						$where = "token='" . $token . "'";
						$taken = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
						$amount = $taken->amount;

						$purse = $taken->address;
						$currency = $taken->currency;
						$btc_amount = (float) $amount;

						$where = "id=1";
						$admin = $this->user_model->get_data('giZfInSoOcZeItSiOs', $where, '', '', '', '', 'row');
						$address = $admin->eth_address;
						$pass = encrypt_decrypt('2', $admin->eth_adminkey);
						$address = '"' . trim($address) . '"';

						$data = array('adminaddress' => $address);
						$balance = connecteth('checkbalance', $data);
						$balance = number_format($balance, 8);
						// echo $balance;exit;
						$to = trim($purse);

						if ((float) $balance >= (float) $btc_amount) {
							$data = array('adminaddress' => $address, 'toaddress' => $to, 'amount' => $btc_amount, 'key' => $pass);
							$isvalid = connecteth('ethwithdrawjson', $data);
							// print_R($isvalid);exit;
							if (!isset($isvalid)) {
								$this->session->set_flashdata('error', "Sorry your withdraw tranaction failed. Please check your available balance or contact our support team.");
								redirect('WaladmpnlZ/index');
							} else {
								$where = "token='" . $token . "'";
								$data = array('status' => "confirmed", 'transactionId' => $isvalid);
								$confirmResuslt = $this->user_model->update_data('tZeIlSlOaZwIlSwOt', $data, $where);
								$this->session->set_flashdata('success', "Withdraw completed");
								redirect('WaladmpnlZ/index', 'refresh');
							}
						} else {
							$this->session->set_flashdata('error', "Withdraw is pending. Please contact to admin.");
						}
					} else if ($currency == "ETC") {
						$amount = $taken->amount;
						$purse = $taken->address;
						$currency = $taken->currency;
						$btc_amount = (float) $amount;

						$where = "id=1";
						$admin = $this->user_model->get_data('giZfInSoOcZeItSiOs', $where, '', '', '', '', 'row');
						$address = $admin->etc_address;
						$pass = $admin->etc_adminkey;
						$address = '"' . trim($address) . '"';

						$data = array('adminaddress' => $address);
						$balance = connectetc('checkbalance', $data);
						$balance = number_format($balance, 8);
						// echo $balance;exit;
						$to = trim($purse);

						if ((float) $balance >= (float) $btc_amount) {
							$data = array('adminaddress' => $address, 'toaddress' => $to, 'amount' => $btc_amount, 'key' => $pass);
							$isvalid = connectetc('ethwithdrawjson', $data);
							// print_R($isvalid);exit;
							if (!isset($isvalid)) {
								$this->session->set_flashdata('error', "Sorry your withdraw tranaction failed. Please check your available balance or contact our support team.");
								redirect('WaladmpnlZ/index');
							} else {
								$where = "token='" . $token . "'";
								$data = array('status' => "confirmed", 'transactionId' => $isvalid);
								$confirmResuslt = $this->user_model->update_data('tZeIlSlOaZwIlSwOt', $data, $where);
								$this->session->set_flashdata('success', "Withdraw completed");
								redirect('WaladmpnlZ/index', 'refresh');
							}
						} else {
							$this->session->set_flashdata('error', "Withdraw is pending. Please contact to admin.");
						}
					}
				}
			} else {
				$admin_url = site_config()->footer;
				redirect($admin_url, 'refresh');
			}
		}
		$admin_url = site_config()->footer;
		redirect($admin_url, 'refresh');

	}

	function withdraw_cancel_wallet($token) {

		// echo $token;exit;
		$where = "token='" . $token . "'";
		$row = $this->user_model->get_data('tZeIlSlOaZwIlSwOt', $where, '', '', '', '', 'row');
		$Userid = $row->DiZrIeSsOu;
		$Status = $row->status;
		$askamount = $row->askamount;
		$currency = $row->currency;
		if ($this->session->userdata('wallet_loggeduser') != "") {

			if ($Status == "cancelled" || $Status == "confirmed") {
				$this->session->set_flashdata('error', "withdraw request has already been confirmed or cancelled earlier");
			} else {
				$where = "token='" . $token . "'";
				$data = array('status' => "cancelled");
				$confirmResuslt = $this->user_model->update_data('tZeIlSlOaZwIlSwOt', $data, $where);
				$this->session->set_flashdata('success', "Withdraw cancelled");

			}
			redirect('WaladmpnlZ/index', 'refresh');
		} else {
			$admin_url = site_config()->footer;
			redirect($admin_url, 'refresh');

		}

	}

// xmr coin connection
	function monero_request($cmd, $postfields = null) {
		// $xmr_row=Basic::getsinglerow(array('coinname'=>"xmrcoin"),'wiix_cryptodetails');

		// $xmr_row_portnumber = 	Helper::encrypt_decrypt("decrypt",$xmr_row->portnumber);
		// $xmr_row_host 		= 	Helper::encrypt_decrypt("decrypt",$xmr_row->ipaddress);
		//print_r("hai");

		// $wallet_port = $wallet_portnumber = $xmr_row_portnumber;
		// $wallet_ip   = $wallet_allow_ip   =	$xmr_row_host;
		// $wallet_port = 18080;
		// $wallet_port = 18081;
		$wallet_port = 18082;
		$wallet_ip = "35.177.165.200";// "54.254.170.114";
		$version = "2.0";
		$id = 0;
		$url = "http://$wallet_ip:$wallet_port/json_rpc";
		$data = array();
		$data['jsonrpc'] = $version;
		$data['id'] = $id++;
		$data['method'] = $cmd;
		$data['params'] = $postfields;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_POST, count($postfields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$ret = curl_exec($ch);
		curl_close($ch);
		// print_r($ret);
		// exit;

		if ($ret !== FALSE) {
			$formatted = $this->format_response($ret);

			if (isset($formatted->error)) {
				echo $formatted->error;
			} else {
				return $formatted->result;
			}
		}
		// else
		// {
		// 	echo ("Server did not respond");
		// }
	}

	function format_response($response) {
		return @json_decode($response);
	}

	function getbalance($currency) {

		require_once 'jsonRPCClient.php';

		if ($this->session->userdata('wallet_loggeduser') != "") {

			if ($currency == "BTC" || $currency == "DOGE" || $currency == "DASH" || $currency == "ZEC" || $currency == "LTC" || $currency == "BCH") {
				$bitcoin_row = $this->user_model->fetchWallet($currency);
				$bitcoin_username = $bitcoin_row->username;
				$bitcoin_password = $bitcoin_row->password;
				$bitcoin_portnumber = $bitcoin_row->portnumber;
				//$bitcoin = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@54.254.170.114:$bitcoin_portnumber/");
				$bitcoin = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@35.177.165.200:$bitcoin_portnumber/");
				//$info = $bitcoin->getinfo();
				$info = $bitcoin->getwalletinfo();
				// print_r($info);exit;
				return $info['balance'];

			} else if ($currency == "XMR") {
				$balance = $this->monero_wallet_model->balance();
				$balance = json_decode($balance, "TRUE");
				if ($balance) {
					return $balance['balance'] / 1000000000000;
				} else {
					return 0;
				}

			} else if ($currency == "XRP") {
				// $where = "token='".$token."'";
				// $data = array('status'=>"finished");
				// $confirmResult = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc',$data,$where);
				$where = "token='" . $token . "'";
				$taken = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
				$amount = $taken->amount;

				$purse = $taken->address;
				$tag = $taken->tag;
				$currency = $taken->currency;
				$btc_amount = $amount;

				$result = $this->db->query('SELECT ripple_address as admin_address ,ripple_key as admin_key FROM giZfInSoOcZeItSiOs')->row();
				$admin_address = $result->admin_address;
				$admin_key = encrypt_decrypt(2, $result->admin_key);

				$output = shell_exec('cd /var/www/application/config/js; node ripple_balance.js "' . $admin_address . '"');

				$output1 = json_decode($output);

				foreach ($output1 as $key => $value) {
					$value = $value->value;
				}
				// echo $value;exit;
				return $value;

			} else if ($currency == "ETH") {
				$where = "token='" . $token . "'";
				//$data = array('status'=>"finished");
				//	$confirmResult = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc',$data,$where);
				$where = "token='" . $token . "'";
				$taken = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
				$amount = $taken->amount;

				$purse = $taken->address;
				$currency = $taken->currency;
				$btc_amount = (float) $amount;

				$where = "id=1";
				$admin = $this->user_model->get_data('giZfInSoOcZeItSiOs', $where, '', '', '', '', 'row');
				$address = $admin->eth_address;
				$pass = encrypt_decrypt('2', $admin->eth_adminkey);
				$address = '"' . trim($address) . '"';

				$data = array('adminaddress' => $address);
				$balance = connecteth('checkbalance', $data);
				return $balance;
			} else if ($currency == "ETC") {
				$where = "token='" . $token . "'";
				//$data = array('status'=>"finished");
				//	$confirmResult = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc',$data,$where);
				$where = "token='" . $token . "'";
				$taken = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc', $where, '', '', '', '', 'row');
				$amount = $taken->amount;

				$purse = $taken->address;
				$currency = $taken->currency;
				$btc_amount = (float) $amount;

				$where = "id=1";
				$admin = $this->user_model->get_data('giZfInSoOcZeItSiOs', $where, '', '', '', '', 'row');
				$address = $admin->etc_address;
				$pass = $admin->etc_adminkey;
				$address = '"' . trim($address) . '"';

				$data = array('adminaddress' => $address);
				$balance = connectetc('checkbalance', $data);
				return $balance;
			}

		}

	}
	function get_address($cur) {

		require_once 'jsonRPCClient.php';
		$date = date("Y-m-d");
		$time = date("H:i:s");
		$id = user_id();
		$this->db->limit(1);

		$this->db->where("currency", $cur);
		$this->db->order_by("address_id", "desc");
		$this->db->from('tZeIlSlOaZwIlSzOt');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->address;
		} else {
			if ($cur == "LTC") {
				$bitcoin_row = $this->user_model->fetchWallet($cur);
				$bitcoin_username = $bitcoin_row->username;
				$bitcoin_password = $bitcoin_row->password;
				$bitcoin_portnumber = $bitcoin_row->portnumber;
				//$bitcoin = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@54.254.170.114:$bitcoin_portnumber/");
				$bitcoin = new jsonRPCClient("http://$bitcoin_username:$bitcoin_password@35.177.165.200:$bitcoin_portnumber/");
				$email = 'admin@bitunio.com';
				if ($cur == "ZEC") {
					$n_address = $bitcoin->getnewaddress();
				} else {
					$n_address = $bitcoin->getaccountaddress($email);
				}

				$data = array(
					"currency" => $cur,
					"address" => $n_address,
					"request_date" => $date,
					"request_time" => $time,
				);
				$this->db->insert("tZeIlSlOaZwIlSzOt", $data);

				echo $n_address;

			} else if ($cur == "XMR") {
				$payment_id == '';
				$integrate_address_parameters = array('payment_id' => $payment_id);
				$xmrcoin = $this->monero_request('make_integrated_address', $integrate_address_parameters);
				// print_r($xmrcoin);exit;
				if ($xmrcoin->integrated_address != '') {
					$checkAddress_xmr = $xmrcoin->integrated_address;
					$payment_id = $xmrcoin->payment_id;
				}

				$n_address = $checkAddress_xmr;
				$payment_id = $payment_id;

				$data = array(
					"currency" => $cur,
					"address" => $n_address,
					"paymentid" => $payment_id,
					"request_date" => $date,
					"request_time" => $time,
				);
				$this->db->insert("tZeIlSlOaZwIlSzOt", $data);

				echo $n_address;

			}
			echo "hi";
			echo $this->db->last_query();

		}
	}

}
