<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PageHandler extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	public function index()
	{
		$url = "http://".$_SERVER['HTTP_HOST'];
		$url .= preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).$_SERVER['PATH_INFO'];
		$access_details = $_SERVER['HTTP_USER_AGENT'];
		 $ip = $this->get_client_ip();
		$currect_date = date("Y-m-d H:i:s");
		
		//check ip already found
		$condition = array(
						"user_agent" => $access_details,
						"ip"  => $ip,
						"url" => $url,
					);
		$block = $this->common_model->getData("gInSiOlZdInSaOhZeIgSaOp",$condition);
		if($block->num_rows()==0){			
			$insert_data = array(
						"user_agent" => $access_details,
						"ip"  => $ip,
						"url" => $url,
						"access_date" =>  date("Y-m-d H:i:s")
					);
			$this->common_model->insertData("gInSiOlZdInSaOhZeIgSaOp",$insert_data);		
		}
		redirect('notfound','refresh');
	}
	
	
	
	function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
}
