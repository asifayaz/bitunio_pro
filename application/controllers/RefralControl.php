<?php
 if (!defined('BASEPATH')) exit('No direct script access allowed');
class RefralControl extends CI_Controller
{
			
			public function __construct() 
			{
				parent::__construct();

				error_reporting(E_ERROR);
				$this->load->database();		
				$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
				$this->output->set_header("Pragma: no-cache");
				header('X-Frame-Options: SAMEORIGIN'); 
				//header('Access-Control-Allow-Origin: true');
				header('X-XSS-Protection: 1; mode=block');
				header('X-Content-Type-Options: nosniff');
				$base=base_url();
				header("ALLOW-FROM: $base");
				header("X-Powered-By: $base");

				ini_set('session.gc_maxlifetime',300);
				ini_set('session.cookie_httponly', 1);	
				ini_set('session.use_only_cookies', 1);
				ini_set('session.cookie_secure', 1);
				require_once 'jsonRPCClient.php';
				
				// if(!log_in())
				// redirect(''); price
				$this->username=username();
				
				$user_newid = log_in();
				if(!$user_newid)
				redirect("");				
			}
			
			function index($id)
			{
				$user_id = user_id();
		
				//$data['transactions'] = $this->user_model->transactions();
				$data['transactions'] = $this->user_model->refral_transactions($user_id);
				$totalusers = $this->user_model->get_total_refral($user_id);
				$data['total_earned'] = ($totalusers*100);
				$data['total_users'] = $totalusers;
				$data['refral_link'] = $id;
				$data['refralid'] = $this->user_model->refral_encryptdecrypt(1, $user_id); //Encrypt ID
				$this->load->view("front/refral", $data);
			}
			
			
			
			

			

}		//End of Class	
				
