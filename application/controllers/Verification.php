<?php
 if (!defined('BASEPATH')) exit('No direct script access allowed');
class Verification extends CI_Controller
		{
			
			public function __construct() 
			{
				parent::__construct();

				error_reporting(E_ERROR);
				$this->load->database();		
				$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
				$this->output->set_header("Pragma: no-cache");
				header('X-Frame-Options: SAMEORIGIN'); 
				//header('Access-Control-Allow-Origin: true');
				header('X-XSS-Protection: 1; mode=block');
				header('X-Content-Type-Options: nosniff');
				$base=base_url();
				header("ALLOW-FROM: $base");
				header("X-Powered-By: $base");

				ini_set('session.gc_maxlifetime',300);
				ini_set('session.cookie_httponly', 1);	
				ini_set('session.use_only_cookies', 1);
				ini_set('session.cookie_secure', 1);
				
				if(!log_in())
				redirect(''); 
			$this->username=username();
			
			$user_newid = log_in();
			if(!$user_newid)
			redirect("");
				
			}
			function index()
			{
				$data['verification']=$this->user_model->get_verification();
				$data['cms']=$this->user_model->get_cms(19);
				$data['refralstatus']  = $this->user_model->get_refral_status_admin('active'); //Get Refral Status of Admin
				$this->load->view("front/verification",$data);
			}
function verification_complete()
{
	
	
	//echo "sdksdsd"; exit; id_number	
	 
	 
	$currentDate = date('Y-m-d');
	$user_id=user_id();
	
	if($user_id == "")
	{   
		die('Your session expired. Please login again.');
	}
	else
	{
		if($_FILES['document4']['name'] != "" && $_FILES['document2']['name'] != "" && $_FILES['document3']['name'] != "")
		{
		$rnumber1 = mt_rand(0,999999); 
		$rnumber2 = mt_rand(0,999999); 
		$rnumber3 = mt_rand(0,999999); 
		
		$this->session->set_userdata('rnumber1',$rnumber1);
		$this->session->set_userdata('rnumber2',$rnumber2);
		$this->session->set_userdata('rnumber3',$rnumber3);
	
	 	$document1 = str_replace(' ', '_', urldecode($_FILES['document4']['name']));   	      
		$document2 = str_replace(' ', '_', urldecode($_FILES['document2']['name']));   	      
		$document3 = str_replace(' ', '_', urldecode($_FILES['document3']['name']));

		if($document1!="")
		{
			//@list($document1,$ext)=explode(".",$document1);						
			$document1_filename		=$rnumber1.$document1;								
			$config['upload_path'] 		=	'assets/uploads/verification/';
			$config['allowed_types'] 	= 	'gif|jpg|jpeg|png';
			$config['file_name']		=	$document1_filename; 
			$config['max_size'] = '200000';   
			$this->load->library('upload', $config);
			$this->upload->initialize($config);	  
			if(!$this->upload->do_upload('document4'))
			{
				$error="Photo ID document".$this->upload->display_errors();  
			}
		} 
		if($document2!="")     
		{          
			//@list($document2,$ext)=explode(".",$document2);						
			$document2_filename		=$rnumber2.$document2;								
			$config['upload_path'] 		=	'assets/uploads/verification/';
			$config['allowed_types'] 	= 	'gif|jpg|jpeg|png';
			$config['file_name']		=	$document2_filename;
			$config['max_size'] = '200000';      
			$this->load->library('upload', $config);
			$this->upload->initialize($config);	  
			if(!$this->upload->do_upload('document2')) 
			{
				$error="Photo ID Back document".$this->upload->display_errors();  
			}
			
		} 
		if($document3!="")     
		{          
			//@list($document3,$ext)=explode(".",$document3);						
			$document3_filename		=$rnumber3.$document3;								
			$config['upload_path'] 		=	'assets/uploads/verification/';
			$config['allowed_types'] 	= 	'gif|jpg|jpeg|png';
			$config['max_size'] = '200000';  
			$config['file_name']		=	$document3_filename;    
			$this->load->library('upload', $config);
			$this->upload->initialize($config);	  
			if(!$this->upload->do_upload('document3')) 
			{
				$error="Proof of residence document".$this->upload->display_errors();  
			}
		} 
		
		
		if(isset($error))
		{
			die($error);		
		}
	$date	=	date('Y-m-d'); 
	$name = $this->input->post('name');
	$id_number = $this->input->post('id_number');
	$verify_comments = $this->input->post('verify_comments');
	
	$document1 = $document1;   	      
	$document2 = $document2;   	      
	$document3 = $document3; 
	
	

	$rnumber1 = $this->session->userdata('rnumber1');	
	$rnumber2 = $this->session->userdata('rnumber2');	
	$rnumber3 = $this->session->userdata('rnumber3');	

	if($document1!="")  
	{  
		@list($document1,$ext)=explode(".",$document1);						
		$images1 = $rnumber1.$document1.".".$ext;	   
	}  
	if($document2!="")   
	{  
		@list($document2,$ext)=explode(".",$document2);
		$images2 = $rnumber2.$document2.".".$ext;	   
	}  
	if($document3!="")  
	{  
		@list($document3,$ext)=explode(".",$document3);						
		$images3 = $rnumber3.$document3.".".$ext;	   
	} 


	$updatedata = array(
						'fname'=>$name,
						'Identity_card_no'=>$id_number,
						'document1'=>$images1,
						'document2'=>$images2,
						'document3'=>$images3,
						'verify_comments'=>$verify_comments,
						'verification_status'=>"pending",
						'created_date'=>$date,
						); 
				//print_r($updatedata); exit;	
						
	$this->db->where("DiZrIeSsOu",$user_id);
	$result = $this->db->update('noitZaIcSiOfZiIrSeOvZrIeSsOu',$updatedata);
	
	if($result)
		{
			die('success');
		}
		else
		{
			die('Oops! Your information could not saved');
		}
		}
		else 
		{
			die('Please upload all doucuments to proceed');
		}
	}
}



  function imageupload()
  {
	  echo $_FILES['photo_proof']['name'];
  }
  




}
