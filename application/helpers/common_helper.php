<?php

if (!function_exists('get_data')) {
	function get_data($table, $where = FALSE, $select = FALSE, $sort = FALSE, $orderBy = FALSE, $limit = FALSE, $offset = FALSE) {
		$ci = &get_instance();
		if ($select) {
			$ci->db->select($select, FALSE);
		}

		if ($where) {
			$ci->db->where($where);
		}

		if ($orderBy) {
			$ci->db->order_by($sort, $orderBy);
		}

		if ($limit != FALSE && $offset != FALSE) {
			$ci->db->limit($limit, $offset);
		}

		if ($limit != FALSE && $offset == FALSE) {
			$ci->db->limit($limit);
		}

		return $ci->db->get($table);
	}
}

if (!function_exists('count_fun')) {
	function count_fun($table, $where = FALSE) {
		$ci = &get_instance();
		return $ci->db->where($where)->get($table)->num_rows();
	}
}
if (!function_exists('update_data')) {
	function update_data($table, $data, $where = FALSE) {
		$ci = &get_instance();
		if ($where) {
			$ci->db->where($where);
		}

		return $ci->db->update($table, $data);
	}
}
if (!function_exists('site_config')) {
	function site_config() {
		$ci = &get_instance();
		$id = 1;
		return get_data("giZfInSoOcZeItSiOs", array("id" => $id))->row();
	}
}
if (!function_exists('user_data')) {
	function user_data($userId = FALSE) {
		$ci = &get_instance();
		if (!$userId) {
			$userId = user_id();
		}

		return get_data("userdetails", array("user_id" => $userId))->row();
	}
}
if (!function_exists('admin_email')) {

	function admin_email() {
		$ci = &get_instance();
		$id = 1;
		$name = get_data("giZfInSoOcZeItSiOs", array("id" => $id))->row()->smtp_username;
		return $name = encrypt_decrypt("2", $name);
	}
}
if (!function_exists('company_name')) {
	function company_name() {
		$ci = &get_instance();
		$id = 1;
		return get_data("giZfInSoOcZeItSiOs", array("id" => $id))->row()->company_name;
	}
}
if (!function_exists('site_config')) {
	function site_config() {
		$ci = &get_instance();
		$id = 1;
		return get_data("giZfInSoOcZeItSiOs", array("id" => $id))->row();
	}
}
if (!function_exists('username')) {
	function username() {
		$ci = &get_instance();
		$id = $ci->session->userdata('user_id');
		if ($id != "") {
			return get_data("SliIaStOeZdIrSeOsu", array("DiZrIeSsOu" => $id))->row()->emanZrIeSsOu;
		} else {
			return false;
		}

	}
}
if (!function_exists('username1')) {
	function username1($id) {
		$ci = &get_instance();
		if ($id != "") {
			return get_data("SliIaStOeZdIrSeOsu", array("DiZrIeSsOu" => $id))->row()->emanZrIeSsOu;
		} else {
			return false;
		}

	}
}
if (!function_exists('user_type')) {
	function user_type() {
		$ci = &get_instance();
		$id = $ci->session->userdata('user_id');
		if ($id != "") {
			return get_data("SliIaStOeZdIrSeOsu", array("DiZrIeSsOu" => $id))->row()->type;
		} else {
			return false;
		}

	}
}
if (!function_exists('uri')) {
	function uri($segement = 1) {
		$ci = &get_instance();
		return $ci->uri->segment($segement);
	}
}
if (!function_exists('offer_amounts')) {
	function offer_amounts($coin = FALSE) {
		if ($coin) {
			return get_data("offer_amounts", array("id" => 1), $coin)->row()->$coin;
		} else {
			return get_data("offer_amounts", array("id" => 1))->row();
		}

	}

	function insep_encode($value) {
		$skey = "SuPerEncKey2010";
		if (!$value) {return false;}
		$text = $value;
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $skey, $text, MCRYPT_MODE_ECB, $iv);
		return trim(safe_b64encode($crypttext));
	}

	function insep_decode($value) {
		$skey = "SuPerEncKey2010";
		if (!$value) {return false;}
		$crypttext = safe_b64decode($value);
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $skey, $crypttext, MCRYPT_MODE_ECB, $iv);
		return trim($decrypttext);
	}
	function safe_b64encode($string) {

		$data = base64_encode($string);
		$data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
		return $data;
	}

	function safe_b64decode($string) {
		$data = str_replace(array('-', '_'), array('+', '/'), $string);
		$mod4 = strlen($data) % 4;
		if ($mod4) {
			$data .= substr('====', $mod4);
		}
		return base64_decode($data);
	}

	function encrypt_decrypt($action, $string) {
		$output = false;
		$encrypt_method = "AES-256-CBC";
		$secret_key = md5('AOdSmIiZn');
		$secret_iv = md5('BTCBOX_SITE_pwd');
		// hash
		$key = hash('sha256', $secret_key);

		$iv = substr(hash('sha256', $secret_iv), 0, 16);
		if ($action == '1') {
			$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
			$output = base64_encode($output);
		} else if ($action == '2') {
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		}
		return $output;
	}

	function client_id($userId = FALSE) {
		$ci = &get_instance();
		return get_data("SliIaStOeZdIrSeOsu", array("DiZrIeSsOu" => $userId))->row()->client_id;
	}

	function verify_status($userId = FALSE) {
		$ci = &get_instance();
		return get_data("noitZaIcSiOfZiIrSeOvZrIeSsOu", array("DiZrIeSsOu" => $userId))->row()->verification_status;
	}

	function get_admin_blnce($cur = false) {
		$ci = &get_instance();
		return get_data("ycneZrIrSuOc", array("currency_symbol" => $cur))->row()->reserve_amount;
	}

	function log_in() {
		$ci = &get_instance();
		$user_id = $ci->session->userdata('user_id');
		if ($user_id == "") {
			return false;
		} else {
			return true;
		}

	}
	if (!function_exists('user_id')) {
		function user_id() {
			$ci = &get_instance();
			return $ci->session->userdata('user_id');
		}
	}
	function to_decimal($value, $places = 7) {
		if (trim($value) == '') {
			return 0;
		} else if ((float) $value == 0) {
			return 0;
		}

		if ((float) $value == (int) $value) {
			return (int) $value;
		} else {
			$value = number_format($value, $places, '.', '');
			$value1 = $value;
			if (substr($value, -1) == '0') {
				$value = substr($value, 0, strlen($value) - 1);
			}

			if (substr($value, -1) == '0') {
				$value = substr($value, 0, strlen($value) - 1);
			}

			if (substr($value, -1) == '0') {
				$value = substr($value, 0, strlen($value) - 1);
			}

			if (substr($value, -1) == '0') {
				$value = substr($value, 0, strlen($value) - 1);
			}

			if (substr($value, -1) == '0') {
				$value = substr($value, 0, strlen($value) - 1);
			}

			if (substr($value, -1) == '0') {
				$value = substr($value, 0, strlen($value) - 1);
			}

			if (substr($value, -1) == '0') {
				$value = substr($value, 0, strlen($value) - 1);
			}

			return $value;
		}
	}

	if (!function_exists('count_fun')) {
		function count_fun($table, $where = FALSE) {
			$ci = &get_instance();
			return $ci->db->where($where)->get($table)->num_rows();
		}
	}

}

if (!function_exists('base64_encode_url')) {
	function base64_encode_url($filename = string, $filetype = string) {
		if ($filename) {
			$imgbinary = fread(fopen($filename, "r"), filesize($filename));
			return 'data:text/' . $filetype . ';base64,' . base64_encode($imgbinary);
		}
	}
}
if (!function_exists('get_extension_from_url')) {
	function get_extension_from_url($url) {
		if ($url) {
			$userfile_extn = substr($url, strrpos($url, '.') + 1);
			return $userfile_extn;
		}
	}
}

function sendmaildepositia($customer_user_id, $amount, $fee, $currency) {

	$ci = &get_instance();

	/*		Get Admin Details Start		 */
	$ci->db->where('id', 1);
	$query = $ci->db->get('giZfInSoOcZeItSiOs');
	if ($query->num_rows() == 1) {
		$row = $query->row();
		$admin_email = $row->email_id;
		$companyname = $row->company_name;
		$siteurl = $row->siteurl;
	}
	$where = "DiZrIeSsOu='" . $customer_user_id . "'";
	$userResult = $ci->user_model->get_data('SliIaStOeZdIrSeOsu', $where, '', '', '', '', 'row');
	$username = $userResult->emanZrIeSsOu;

	$id = user_id();
	$query_count = $ci->db->query("select t1.dilZiIaSmOe,t1.DiZrIeSsOu,t2.txid,t2.DiZrIeSsOu from SliIaStOeZdIrSeOsu as t1 INNER JOIN tneZtInSoOc as t2 where t1.DiZrIeSsOu = t2.DiZrIeSsOu  AND t1.DiZrIeSsOu='$id'")->row();
	$to = encrypt_decrypt("2", $query_count->txid) . $query_count->dilZiIaSmOe;

	$get_email_info = $ci->db->query("select * from setalpZmIeStOlZiIaSmOe where id='16'")->row();

	$msg = $get_email_info->message;

	$bex = "<b>Bitunio</b>";
	$msg = str_replace("##USERNAME##", $username, $msg);
	$msg = str_replace("##COMPANYNAME##", $bex, $msg);
	$msg = str_replace('##AMOUNT##', $amount, $msg);
	$msg = str_replace('##CURRENCY##', $currency, $msg);
	$msg = str_replace('##FEE##', $fee, $msg);

	$ci->user_model->mailsettings();
	$ci->email->from(admin_email(), company_name());
	$ci->email->to($to);
	$ci->email->subject("Deposit Request Pending");
	$ci->email->message($msg);
	$ci->email->send();
	return true;

}

// eth connection start here
function connecteth($method, $data = array()) {
	$url = '35.177.165.200/eth_api.php';//54.254.170.114/eth_api.php';
	$name = $_SERVER['SERVER_NAME'];//'54.254.203.35';// ;
	$data = array("method" => $method, "name" => $name, "keyword" => 'tbeisthtuiantom', 'data' => $data);
	$data_string = json_encode($data);
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$response = curl_exec($ch);
	curl_close($ch);
	// echo $response;
	// exit;
	// header('Content-Type: application/json');
	$result = json_decode($response);
	if ($result->type == 'success') {
		// print_r($result->result);
		return $result->result;
	} else {

	}
}
// eth connection end here

// etc connection start here
function connectetc($method, $data = array()) {
	$url = '35.177.165.200/etc_api.php';//'54.254.170.114/etc_api.php';
	$name = $_SERVER['SERVER_NAME'];//'54.254.203.35';//$_SERVER['SERVER_NAME'];
	//$data = array("method" => $method, "name" => $name, "keyword" => 'tbeistctuiantom', 'data' => $data);
	$data = array("method" => $method, "name" => $name, "keyword" => 'tbeistctuiantom', 'data' => $data);
	$data_string = json_encode($data);
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$response = curl_exec($ch);
	curl_close($ch);
	// echo $response;
	// exit;
	// header('Content-Type: application/json');
	$result = json_decode($response);
	if ($result->type == 'success') {
		// print_r($result->result);
		return $result->result;
	} else {
		
	}
}
// etc connection end here

function admin_mail() {
	$ci = &get_instance();
	$id = 1;
	$name = get_data("giZfInSoOcZeItSiOs", array("id" => $id))->row()->dilZiIaSmOe;
	return encrypt_decrypt(2, $name);
}

function mail_admin() {
	return encrypt_decrypt(2, 'bDJ4SitYOE13a0ZOZzlvWUVGWVBKRE5kZmVSMGFCa0NLS1p1OVhmZ2UzVT0');
}

function coinconnect_curl($connection_parms, $cmd, $postfields = array()) {
	$data = array();
	$data['jsonrpc'] = $connection_parms['version'];
	$data['id'] = $connection_parms['id'];
	$data['method'] = $cmd;
	$data['params'] = $postfields;
	$url = 'http://' . $connection_parms['user'] . ':' . $connection_parms['password'] . '@' . $connection_parms['ip'] . ':' . $connection_parms['port'];
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_POST, count($postfields));
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$ret = curl_exec($ch);
	$info = curl_getinfo($ch);

	curl_close($ch);

	if ($ret !== FALSE) {
		if (isset($formatted->error)) {
			return $formatted->error;
			//throw new Exception($formatted->error->message, $formatted->error->code);
		} else {

			$output = json_decode($ret);
			return $output->result;
		}
	} else {
		throw new Exception("Server did not respond");
	}
}
?>
