$("#withdraw_ia").validate({
			rules: {
				amount:{required:true,min:50,},
						  },
			submitHandler: function(form) {
           var dataform=$('#withdraw_ia').serialize();
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'withdraw_ia_submit',
					 beforeSend:function(){
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					 $('.pageloadingBG, .pageloading').css('display', 'none');
					 var doutput = output.trim();
						
						if(doutput == "true")
                        {
						$('#withdraw_ia')[0].reset();
						$("#tfa").modal("hide");
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your withdraw request accepted. A letter with further instructions sent to your email!");
						$('#success-modal').modal('show');
						refresh_withdraw_ia(w_cur); 
						window.setTimeout(function(){    
                        window.location.href ="";   },3000);
						}
						else if(doutput == "balance")
						{
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#error-modal').modal('show');
						}
						else if(doutput == "status")
						{
						$("#stat").html("Error");
						$("#msg").html("Please verify your bank details");
						$('#error-modal').modal('show');
						}
						else if(doutput == "enable")
						{
						$("#tfa").modal("show");
						}
						else
                        {
						$("#stat").html("Error");
						$("#msg").html("Error occured while doing deposit request. Please try again");
						$('#error-modal').modal('show');
						}
						}
                 });
				
         },
			
			});
			
function withdraw_ia_cal(amnt)
{
var wi_fee=$("#wfee").html();

var wi_fee_type=$("#fee_type").html();

if(wi_fee_type=='percentage')
{
 var fee=parseFloat(amnt) * parseFloat(wi_fee)/100;
 var f_amnt=parseFloat(amnt) - parseFloat(fee);	
}
else
{
	var f_amnt=parseFloat(amnt) - parseFloat(wi_fee);	
}

 $("#final").html(f_amnt);
 $("#final1").val(f_amnt);

}			




function refresh_withdraw_ia(cur)
  {
 
    $.get(base_url+'refreshwithdraw1/'+cur, function(data){
	//$('.pageloadingBG, .pageloading').css('display', '');
	
    var with_history 		 = data.with_history;
       var with_history_place = '';
	
	var sec_cur_dec=2;
	var fir_cur_dec=8;
	var add='';
	if(with_history.length>0)
      {
		  for(var j=0;j<with_history.length;j++){
	  var trns="---";
		  add='---';
		  if(with_history[j].status == "completed")
		  {
		  var statt="<span style='color:green'>completed</span>";
		  }
		  else if(with_history[j].status == "cancelled")
		  {
		  var statt="<span style='color:red'>cancelled</span>";
		  }
		  else if(with_history[j].status == "pending")
		  {
		  var statt="<span style='color:orange'>pending</span>";
		  }
		  else if(with_history[j].status == "rejected")
		  {
		  var statt="<span style='color:red'>rejected</span>";
		  }
		  else if(with_history[j].status == "processing")
		  {
		  var statt="<span style='color:blue'>processing</span>";
		  }		  
		  else 
		  {
		  var statt="<span style='color:orange'>pending</span>";
		  }
		  
          with_history_place += '<tr><td>'+parseInt(j+1)+'</td><td>'+with_history[j].currency+'</td><td>'+with_history[j].askamount+'</td><td>'+(parseFloat(with_history[j].askamount) - parseFloat(with_history[j].amount)).toFixed(4)+'</td><td>'+parseFloat(with_history[j].amount)+'</td><td>'+with_history[j].request_date+' '+with_history[j].request_time+'</td><td>'+statt+'</td></tr>';
        }     
      }
      else
      {
        with_history_place += '<tr><td colspan="6" class="no-records">No Records available</td></tr>';
      }
	  


      $('.with_ia_history tbody').html(with_history_place);
	  //$('.pageloadingBG, .pageloading').css('display', 'none');

	  
    },'json');
	
  }
  
  $("#tfa_form").validate({
			rules: {
				onecode:{required:true,number:true,},	
			
			},
			messages: {
				
			},
			
			submitHandler: function(form) {
           var dataform=$('#tfa_form').serialize();
		   var amount=$("#amount").val();
		   var final1=$("#final1").val();
		   var wcur=$("#wcur").val();
		   
		   $.ajax({
                     type:'POST',
                     data:dataform+"&amount="+amount+"&final1="+final1+"&wcur="+wcur,
                     url:base_url+'bitunio/tfa_confirm2',
					 beforeSend:function(){
					$('.pageloadingBG, .pageloading').css('display', '');
					$('#tfa_code_error').hide();
						},
                     success:function(output) 
                     {
						$('.pageloadingBG, .pageloading').css('display', 'none');
                        var doutput = output.trim();
						
						
						
						if(doutput == "true")
                        {
						$('#tfa_code_error').hide();
						$('#withdraw_ia')[0].reset();
						$("#tfa").modal("hide");
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your withdraw request accepted. A letter with further instructions sent to your email!");
						$('#success-modal').modal('show');
						refresh_withdraw_ia(w_cur); 
						}
						else if(doutput == "balance")
						{
						$('#pwd_btn').attr('disabled', false);
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#new_error-modal').modal('show');
						}
						else if(doutput == "false")
						{
						$('#tfa_error').show();						
						$("#tfa_error_msg").html("Invalid TFA Code");
						$('#tfa_error').fadeOut(4000);
						$('#tfa_btn').attr('disabled', false);
						}
						else
                        {
						$('#pwd_btn').attr('disabled', false);
						$("#stat").html("Error");
						$("#msg").html("Error occured while doing withdraw request. Please try again");
						$('#new_error-modal').modal('show');
						}
						
						 
						 
						
						
                     }
                 });
				
         },
		 });
  
  
  refresh_withdraw_ia(w_cur);
  
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 46 || charCode > 57))
        return false;
    return true;
}