$("#bank_form").validate({
			rules: {				
				bank_name:{required:true},
				account_number:{required:true},
				iban_code:{required:true},
				bank_address:{required:true},
				document1:{required:true},
				account_holder:{required:true},
				account_type:{required:true},
				city:{required:true},
				},
			messages: {
				
				
					},
					
submitHandler: function (form) {

                    //var data = $('#bank_form').serialize();
                      $.ajax({
                        type:'POST',
						data:new FormData($('#bank_form')[0]),
						url:base_url+'bitunio/bank_verification',
						processData: false,
                        contentType: false,
						success:function(output) {
							$('.pageloadingBG, .pageloading').css('display', 'none');
						
							console.log("Dat1 : " + output);
						if(output =="success")
							{
								$("#stat_suc").html("Success");
								$("#msg_suc").html("You Have updated your Bank verification details Successfully please wait until admin verify your details");
								$('#success-modal').modal('show');
								setTimeout(function(){ window.location.href=base_url+"profile/bank_verification"; }, 3000);	
							}
							else
							{
								
								$("#stat").html("Error");
								$("#msg").html("Please upload bank account book to proceed");
								$('#failure-modal').modal('show');
							}
                        },
                         beforeSend:function(){                 
                            	$('.pageloadingBG, .pageloading').css('display', '');
                          }
                      });
                      return false;
                  },
			
			});
