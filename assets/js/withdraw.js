
function refresh_withdraw(cur)
  {
  //alert(cur); return false;
  if(cur == "INR" || cur == "AED")
  {
  window.location.href=base_url+'withdraw_ia/'+cur;
  return false;
  }
  
    $.get(base_url+'refreshwithdraw/'+cur, function(data){
	//$('.pageloadingBG, .pageloading').css('display', '');
	
    var with_history 		 = data.with_history;
    var balc 		 = data.balance;
    var selected_currency 		 = data.selected_currency;
    var with_fee 		 = data.with_fee;
    var with_min 		 = data.with_min;

    if(selected_currency == 'XRP'){
    	var tag = '<div class="form-group clearfix"><label class="form-control-label"> Destination tag ( optional )</label><input class="form-control"  name="tag" placeholder=" TAG" type="text" id="tag"></div>';
    	$("#destination_tag").html(tag);
    }
    else if(selected_currency == 'XMR'){
    	var tag = '<div class="form-group clearfix"><label class="form-control-label"> Payment Id ( optional )</label><input class="form-control"  name="tag" placeholder=" Payment Id" type="text" id="tag"></div>';
    	$("#destination_tag").html(tag);
    }else{
    	var tag = ' ';
    	$("#destination_tag").html(tag);
    }
    if(selected_currency == 'BTC'){
    	$('.notif_blk1').css('display','block');
    }else{
    	$('.notif_blk1').css('display','none');
    }
   	$("#selected_currency").html(selected_currency);
   	$("#balc").html(balc);
   	$("#selected_currency1").html(selected_currency);
   	$(".selected_currency").html(selected_currency);
   	$("#with_fee").html(with_fee);
   	$('.min_wit').val(with_min)
   	$('.min_wit').html(with_min)
   	$('.fee_type').html(selected_currency);
   	$("#amount").val("");
   	$("#final").html("0.00");
    var with_history_place = '';
	
	var sec_cur_dec=2;
	var fir_cur_dec=8;
	var add='';
	if(with_history.length>0)
      {
		  for(var j=0;j<with_history.length;j++){
	  var trns=(with_history[j].trans_id != "")?with_history[j].trans_id : "---";
		  add=(with_history[j].address !="")?with_history[j].address:'---';
          with_history_place += '<tr><td>'+parseInt(j+1)+'</td><td>'+trns+'</td><td>'+with_history[j].askamount+'</td><td>'+(parseFloat(with_history[j].askamount) - parseFloat(with_history[j].amount)).toFixed(4)+'</td><td>'+parseFloat(with_history[j].amount)+'</td><td>'+with_history[j].withdraw_address+'</td><td>'+with_history[j].request_date+' '+with_history[j].request_time+'</td><td>'+with_history[j].status+'</td></tr>';
        }     
      }
      else
      {
        with_history_place += '<tr><td colspan="8" class="no-records">No Records available</td></tr>';
      }
	  


      $('.with_history tbody').html(with_history_place);
	  //$('.pageloadingBG, .pageloading').css('display', 'none');

	  
    },'json');
	
  }
  
  refresh_withdraw(w_cur);  
  
  
  //
  
  $("#withdraw_crypto").validate({
			rules: {
				amount:{required:true,number:true,check_min:true,},				
				address:{required:true,},
			
					  },
		
					
					submitHandler: function(form) {
           var dataform=$('#withdraw_crypto').serialize();
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'coin_tfa',
					 beforeSend:function(){
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                    success:function(output)
                    {
					$('.pageloadingBG, .pageloading').css('display', 'none');
					var doutput = output.trim();
						
						if(doutput == "true")
                        {
						$('#withdraw_crypto')[0].reset();
						$("#tfa").modal("hide");
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your withdraw request accepted. A letter with further instructions sent to your email!");
						$('#success-modal').modal('show');
						refresh_withdraw(w_cur);						
						}
						else if(doutput == "enable")
						{
						$("#tfa").modal("show");
						}
						else if(doutput == "balance")
						{
						$('#pwd_btn').attr('disabled', false);
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#error-modal').modal('show');
						}
						else
                        {
						$('#pwd_btn').attr('disabled', false);
						$("#stat").html("Error");
						$("#msg").html("Error occured while doing withdraw request. Please try again");
						$('#error-modal').modal('show');
						}
						}
                 });
				
         },
			
			});

  jQuery.validator.addMethod("check_min", function(value, element) {

  var min_value = parseFloat($('.min_wit').val());
   
    return (parseFloat(value) > min_value || parseFloat(value) == min_value);
}, "Please enter an amount greater than or equal to minimum amount "+$('.min_wit').val());

			
// function withdraw_cal(amnt)
// {
// var wi_fee=$("#with_fee").html();
// var fee=parseFloat(amnt) * parseFloat(wi_fee)/100;
// var f_amnt=parseFloat(amnt) - parseFloat(fee);
// if(fee >= 0.000000003)
// {
// $("#with_error").html("");
// $('#withdraw_btn').attr('disabled', false);
// $('#final').html(f_amnt.toFixed(8));
// $('#final1').val(f_amnt.toFixed(8));
// }
// else
// {
// $("#with_error").html("Enter valid amount");
// $('#withdraw_btn').attr('disabled', true);
// $("#final").html("0.00");
// }
// }

function withdraw_cal(amnt)
{
	var wi_fee=$("#with_fee").html();
	var min_wit=parseFloat($(".min_wit").html());
	var f_amnt=parseFloat(amnt) - parseFloat(wi_fee);
if(amnt && (amnt > min_wit || amnt == min_wit ) )
{
	$("#with_error").html("");
	$('#withdraw_btn').attr('disabled', false);
	$('#final').html(f_amnt.toFixed(8));
	$('#final1').val(f_amnt.toFixed(8));
}
else
{
// $("#with_error").html("Enter valid amount");
$('#withdraw_btn').attr('disabled', true);
$("#final").html("0.00");
}
}

$("#tfa_form").validate({
			rules: {
				onecode:{required:true,number:true,},	
			
			},
			messages: {
				
			},
			
			submitHandler: function(form) {
           var dataform=$('#tfa_form').serialize();
		   var currency=$("#crypto_withdraw").val();
		   var amount=$("#amount").val();
		   var address=$("#address").val();
		   var final1=$("#final1").val();
		   
		   $.ajax({
                     type:'POST',
                     data:dataform+"&currency="+currency+"&amount="+amount+"&address="+address+"&final1="+final1,
                     url:base_url+'bitunio/tfa_confirm1',
					 beforeSend:function(){
					$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output)
                     {
						$('.pageloadingBG, .pageloading').css('display', 'none');
                        var doutput = output.trim();
						
						
						if(doutput == "true")
                        {
						$('#withdraw_crypto')[0].reset();
						$("#tfa").modal("hide");
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your withdraw request accepted. A letter with further instructions sent to your email!");
						$('#success-modal').modal('show');
						refresh_withdraw(w_cur); 
						// window.setTimeout(function(){    
                        // window.location.href ="";   },5000);
						}
						else if(doutput == "balance")
						{
						$('#pwd_btn').attr('disabled', false);
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#error-modal').modal('show');
						}
						else
                        {
						$('#tfa_error').show();						
						$("#tfa_error_msg").html("Invalid TFA Code");
						$('#tfa_error').fadeOut(4000);
						$('#tfa_btn').attr('disabled', false);
						}
                     }
                 });
				
         },
		 });

		 
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 46 || charCode > 57))
        return false;
    return true;
}