jQuery.validator.addMethod("Passwordconditions", function(value, element) {
    return (/^(?=.*[A-Z])(?=.*\d)[A-Za-z\d$@+$!%*#?&]{8,}$/.test(value));
}, "Password must have minimum 8 characters at least 1 Alphabet in Caps, 1 Number 1 Special character");

jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z,A-Z," "]+$/i.test(value);
}, "Letters only please");

$("#personal_inform").validate({
			rules: {				
				
				
				phone:{required:true,number:true,minlength:6,maxlength:20,},
				strt1:{required:true},
				strt2:{required:true},
				city:{required:true,lettersonly:true,},
				country:{required:true},
				state:{required:true},
				postal_code:{required:true,number:true,minlength:6,maxlength:6,},
				
					},
			messages: {
				
				
					},
					
					submitHandler: function(form) {
           var dataform=$('#personal_inform').serialize();
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'profile_settings/update_personal_inform',
					 beforeSend:function(){
						$('.pageloadingBG, .pageloading').css('display', '');
						//$('#per_btn').attr('disabled', true);
						},
                     success:function(output) 
                     {
						$('.pageloadingBG, .pageloading').css('display', 'none');
                        var doutput = output.trim();
						console.log("Dat : " + output);
						
						if(doutput == "true")
                        {
							$("#stat_suc").html("Success");
							$("#msg_suc").html("Successfully Updated Your Profile");
							$('#success-modal').modal('show');
							window.setTimeout(function(){    
	                        window.location.href =base_url+"profile";   },3000);
						}
						else
                        {
							
							$("#stat").html("Error");
							$("#msg").html("Some error Occured please try again");
							$('#error-modal').modal('show');
						}
					}
                 });
				
         },
			
			});

function change_state(country_id)
			{
			  $.ajax({
                    type:'GET',
					url:base_url+'profile_settings/change_state/'+country_id,
					success:function(output)
                    {
						var doutput = output.trim();
						//alert(doutput);
						$("#state_id12").html(doutput);
					}
                 });
			
			}		

$("#change_pass").validate({
			rules: {
				
				password:{required:true},
				npassword:{required:true,Passwordconditions:true},
				cnpassword:{required:true,equalTo:"#npwd",},
				
					},
			messages: {
				
				
					},
					
					submitHandler: function(form) {
           var dataform=$('#change_pass').serialize();
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'profile_settings/change_pass',
					 beforeSend:function(){
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					 $('.pageloadingBG, .pageloading').css('display', 'none');
						$('#pwd_loader').hide();
                        var doutput = output.trim();
						
						if(doutput == "true")
                        {
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Successfully changed Your password ,Your password will Expiry in "+pass_ex+" days");
						$('#success-modal').modal('show');
						window.setTimeout(function(){    
                        window.location.href =base_url+"profile/change";   },3000);
						}
						else
                        {
						$('#pwd_btn').attr('disabled', false);
						$("#stat").html("Error");
						$("#msg").html("Old password is wrong please try again");
						$('#error-modal').modal('show');
						}
						}
                 });
				
         },
			
			});			


$("#tfa_change").validate({
			rules: {				
				one_code:{required:true},
					},
			messages: {
				npassword:{required:'Required'},
				rpassword:{required:'Required',equalTo:'Do not Match'},
				
					},					
					submitHandler: function(form) {
           var dataform=$('#tfa_change').serialize();
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'profile_settings/verify_code1',
					 beforeSend:function(){
						$('#tfa_loader').show();
						$('#tfa_btn').attr('disabled', true);
						},
                     success:function(output) 
                     {
						$('#tfa_loader').hide();
                        var doutput = output.trim();
						
						if(doutput == "enable")
                        {						
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Successfully Enabled your TFA");
						$('#success-modal').modal('show');
						window.setTimeout(function(){    
                        window.location.href =base_url+"profile/security";   },3000);
						  }
						else if(doutput == "disable")
                        {						
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Successfully Disabled your TFA");
						$('#success-modal').modal('show');
						window.setTimeout(function(){    
                        window.location.href ="";   },4000);
						}
						else
                        {
						$('#tfa_btn').attr('disabled', false);
						$("#stat").html("Error");
						$("#msg").html("Wrong Code Try Again");
						$('#error-modal').modal('show');                         
						}
						}
                 });
				
         },
			
});


//$("#api_create").validate({
	
$("#inputCreate-button-create1").click(function(){	
	
   var dataform=$('#api_create').serialize();
   
   $.ajax({
             type:'POST',
             data:dataform,
             url:base_url+'profile_settings/createapikey',
			 beforeSend:function(){
					//$('#tfa_loader').show();
					//$('#tfa_btn').attr('disabled', true);
			 },
             success:function(output) 
             {
				//$('#tfa_loader').hide();
                var doutput = output.trim();
				
                //var string = JSON.stringify(rec);
                var objects = JSON.parse(doutput);
                var data = "";
             //   console.log("M : " + objects.length);
                //Reverse of records
                for (var i = objects.length-1; i>=0; i--) {
                	data = data + "<tr>";
	            	    data = data + "<td>" + objects[i].Api_name + "</td>";
	            	    data = data + "<td>" + objects[i].Api_key + "</td>";
	            	    data = data + "<td>" + objects[i].Api_secret + "</td>";
	            	    data = data + "<td>" + objects[i].Api_status + "</td>";                	  
	        	    data = data + "</tr>";
                }
               
                
                $("#tdata").html(data);
                
                $("#apiname").val('');
                
				/*if(doutput == "enable")
                {						
					$("#stat_suc").html("Success");
					$("#msg_suc").html("Successfully Enabled your TFA");
					$('#success-modal').modal('show');
					window.setTimeout(function(){    
	                window.location.href =base_url+"profile/security";   },3000);
				}
				else if(doutput == "disable")
                {						
					$("#stat_suc").html("Success");
					$("#msg_suc").html("Successfully Disabled your TFA");
					$('#success-modal').modal('show');
					window.setTimeout(function(){    
	                window.location.href ="";   },4000);
				}
				else
                {
					$('#tfa_btn').attr('disabled', false);
					$("#stat").html("Error");
					$("#msg").html("Wrong Code Try Again");
					$('#error-modal').modal('show');                         
				}*/
			 }
         });	
});
 //});